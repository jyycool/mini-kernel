package com.gcloud.common.crypto;

import java.util.zip.Adler32;

import com.google.common.primitives.Longs;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DefaultCryptoProvider implements CryptoProvider
{
	
	public static String  KEY_ALGORITHM         = "RSA";
	public static String  KEY_SIGNING_ALGORITHM = "SHA512WithRSA";
	public static int     KEY_SIZE              = 2048;
	public static String  PROVIDER              = "BC";
	
	public DefaultCryptoProvider( )
	{
		
	}
	
	
	
	private String generateRandomAlphanumeric()
	{
		// length from generateRandomAlphanumeric is not constant due to
		// removal of punctuation characters
		return Hashes.getRandom( 64 ).replaceAll("\\p{Punct}", "");
	}
	
	private String generateRandomAlphanumeric(int length)
	{
		final StringBuilder randomBuilder = new StringBuilder( length + 90 );
		while( randomBuilder.length() < length ) {
			randomBuilder.append(generateRandomAlphanumeric() );
		}
		return randomBuilder.toString().substring( 0, length );
	}
	
	@Override
	public String generateQueryId()
	{
		return generateRandomAlphanumeric(21).toUpperCase();//NOTE: this MUST be 21-alnums upper case.
	}
	
	@Override
	public String generateSecretKey() {
		
		return generateRandomAlphanumeric(40);//NOTE: this MUST be 40-chars from base64.
	}
	
	@Override
	public String generateId( final String seed, final String prefix )
	{
		Adler32 hash = new Adler32( );
		String key = seed;
		hash.update( key.getBytes( ) );
		
	    /**
	     * @see http://tools.ietf.org/html/rfc3309
	     */
		for ( int i = key.length( ); i < 128; i += 8 ) {
			hash.update( Longs.toByteArray( Double.doubleToRawLongBits( Math.random( ) ) ) );
		}
		String id = String.format( "%s-%08X", prefix, hash.getValue( ) );
		return id;
	}

}