package com.gcloud.common.crypto;

import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum Hmac {
	HmacSHA1,
	HmacSHA256;

    private static Logger LOG = LogManager.getLogger( Hmac.class );
    public Mac getInstance() {
	    try {
	      return Mac.getInstance( this.toString( ) );
	    } catch ( NoSuchAlgorithmException e ) {
        LOG.fatal( e, e );
        throw new RuntimeException( e );
    }
  }
}