package com.gcloud.common.util;

import com.gcloud.common.model.IpRange;
import com.gcloud.common.model.IpSubnet;
import com.gcloud.common.model.PortRangeInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class NetworkUtil {
	public static String calculateNetmask(int number) {
		int divisor = 8;
		int remainder = number % divisor;
		int multiple = number / divisor;
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < multiple; i++) {
			if (i > 0) {
				sb.append(".");
			}
			sb.append("255");
		}
		if (multiple < 4) {
			int tmp = 0;
			while (remainder-- > 0) {
				tmp += 1 << --divisor;
			}
			sb.append(".").append(tmp);
			multiple++;
		}
		if (multiple < 4) {
			for (int i = multiple; i < 4; i++) {
				sb.append(".0");
			}
		}
		return sb.toString();
	}
	
	public static boolean isIpv4(String ipAddress) {

		String ip = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
			    +"(00?\\d|1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
			    +"(00?\\d|1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
			    +"(00?\\d|1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";

		Pattern pattern = Pattern.compile(ip);
		Matcher matcher = pattern.matcher(ipAddress);
		return matcher.matches();

	}

	public static boolean checkCidrIp(String cidr, String value) {
		if (value != null && !"".equals(value) && cidr != null && !"".equals(cidr)) {
			String subnetMask = cidr.substring(cidr.lastIndexOf('/') + 1);
			int subnetRemain = 32 - Integer.parseInt(subnetMask);

			String subnetIp = cidr.substring(0, cidr.lastIndexOf('/'));
			String[] subnetIpArr = subnetIp.split("\\.");
			String subnetIpBin = "";
			for (int i = 0; i < subnetIpArr.length; i++) {
				subnetIpBin = subnetIpBin + String.format("%08d", Integer.parseInt(Integer.toBinaryString(Integer.parseInt(subnetIpArr[i]))));
			}
			String subnetRange = subnetIpBin.substring(0, subnetIpBin.length() - subnetRemain);

			String[] curIpArr = value.split("\\.");
			String curIpBin = "";
			for (int i = 0; i < curIpArr.length; i++) {
				curIpBin = curIpBin + String.format("%08d", Integer.parseInt(Integer.toBinaryString(Integer.parseInt(curIpArr[i]))));
			}

			if (curIpBin.indexOf(subnetRange) != 0) {
				return false;
			}
		}
		return true;
	}

	//  80/100 转成 min = 80 max = 100
	public static PortRangeInfo portRangeInfo(String portRange){

		if(StringUtils.isBlank(portRange)){
			return new PortRangeInfo(null);
		}

		String[] ports = portRange.split("/");
		if(ports.length == 1){

			if("-1".equals(ports[0])){
				return new PortRangeInfo(null);
			}else if(RegexUtil.isValidPort(ports[0])){
				return new PortRangeInfo(Integer.valueOf(ports[0]));
			}

		}else if(ports.length == 2){

			if ("-1".equals(ports[0]) && "-1".equals(ports[1])) {
				return new PortRangeInfo(null);
			}else if(RegexUtil.isValidPort(ports[0]) && RegexUtil.isValidPort(ports[1])){
				Integer min = Integer.valueOf(ports[0]);
				Integer max = Integer.valueOf(ports[1]);

				if(max >= min){
					return new PortRangeInfo(min, max);
				}
			}

		}

		return null;
	}

	public static String porRange(Integer min, Integer max){
		String result = null;
		if(min == null && max == null){
			result = null;
		}else if(min == null){
			result = String.valueOf(max);
		}else if(max == null){
			return String.valueOf(min);
		}else{
			result = String.format("%s/%s", min, max);
		}
		return result;
	}

	public static long ipToDecm(String ip){
		String[] ipArr = ip.split("\\.");
		return Long.valueOf(ipArr[0]) * 256 * 256 * 256 + Long.valueOf(ipArr[1]) * 256 * 256 + Long.valueOf(ipArr[2]) * 256 + Long.valueOf(ipArr[3]);
	}

	public static boolean ipRangeConflict(List<String> ipRanges){
		if(ipRanges == null || ipRanges.size() <= 1){
			return false;
		}

		List<IpRange> ipRangeList = new ArrayList<>();
		for(String ipRange : ipRanges){
			String[] ipRangeArr = ipRange.split("-");
			long start = ipToDecm(ipRangeArr[0]);
			long end = ipToDecm(ipRangeArr[1]);
			ipRangeList.add(new IpRange(start, end));
		}

		Collections.sort(ipRangeList, new IpRange.IpRangeComparator());

		//对比相邻两个
		for(int i = 0; i < ipRangeList.size() - 1; i++){
			if(ipRangeList.get(i).getEnd() >= ipRangeList.get(i + 1).getStart()){
				return true;
			}
		}

		return false;
	}

	public static boolean cidrConflict(List<String> cidrs){

		List<String> ipRanges = new ArrayList<>();
		for(String cidr : cidrs){
			IpSubnet subnet = ipSubnet(cidr);
			ipRanges.add(String.format("%s-%s", subnet.getFirstIp(), subnet.getLastIp()));
		}
		return ipRangeConflict(ipRanges);
	}

	public static IpSubnet ipSubnet(String cidr){

		String[] cidrInfo = cidr.split("/");

		String ip = cidrInfo[0];
		Integer mask = Integer.valueOf(cidrInfo[1]);

		String netmask = toNetmask(mask);

		//网段
		String subnet = subnet(ip, netmask);
		//第一个ip
		String firstIp = firstIp(subnet);
		//�?后一个ip
		String lastIp = lastIp(subnet, mask);

		IpSubnet ipSubnet = new IpSubnet();
		//31特殊，只有两个可用ip，一个是�?始（subnet），�?个是结束(firstIp)
		if(mask == 31){
			ipSubnet.setFirstIp(subnet);
			ipSubnet.setLastIp(firstIp);
		}else{
			ipSubnet.setNetmask(netmask);
			ipSubnet.setSubnet(subnet);
			ipSubnet.setFirstIp(firstIp);
			ipSubnet.setLastIp(lastIp);
		}

		return ipSubnet;
	}

	public static String toNetmask(int mask){

		int num = mask / 8;
		int left = mask % 8;

		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < left; i++){
			sb.append("1");
		}

		for(int i = 0; i < (8 - left); i++){
			sb.append("0");
		}

		StringBuffer netmask = new StringBuffer();
		for(int i = 0; i < num; i++){
			netmask.append("255").append(".");
		}
		netmask.append(Integer.valueOf(sb.toString(), 2)).append(".");
		for(int i = 0; i < 4 - num - 1; i++){
			netmask.append("0").append(".");
		}
		netmask.deleteCharAt(netmask.length() - 1);


		return netmask.toString();
	}

	public static String toBinaryString(String ip){

		String[] ipArr = ip.split("\\.");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < ipArr.length; i++){
			String str = StringUtils.leftPad(Integer.toBinaryString(Integer.valueOf(ipArr[i])), 8, "0");
			sb.append(str).append(".");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	public static String toDecimalString(String ip){

		String[] ipArr = ip.split("\\.");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < ipArr.length; i++){
			String str = String.valueOf(Integer.valueOf(ipArr[i], 2));
			sb.append(str).append(".");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	//入参二进�?
	public static String subnet(String net, String netmask){

		String[] netArr = net.split("\\.");
		String[] netmaskArr = netmask.split("\\.");

		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < 4; i++){
			Integer sem = Integer.valueOf(netArr[i]) & Integer.valueOf(netmaskArr[i]);
			sb.append(sem).append(".");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	public static String firstIp(String subnet){

		String[] ipArr = subnet.split("\\.");
		ipArr[3] = String.valueOf(Integer.valueOf(ipArr[3]) + 1);
		return StringUtils.join(ipArr, ".");

	}

	public static String lastIp(String subnet, Integer netmask){

		String subnetBinary = toBinaryString(subnet);
		//主机位全部转�?1 再减�?
//		String subnetStr = subnetBinary.replace("\\.", "");
		StringBuffer sb = new StringBuffer();
		sb.append(subnetBinary);

		//�?后一位不替换
		int replace = 0;
		int length = sb.length();
		for(int i = 1; i < sb.length(); i++){
			if(sb.charAt(length - 1 - i) != '.'){
				sb.setCharAt(length - 1 - i, '1');
				replace++;
			}
			//�?后一位不替换
			if(replace == 32 - netmask - 1){
				break;
			}
		}
		return toDecimalString(sb.toString());
	}

	public static String decmToIp(long ipValue){

		long left = ipValue;

		long first = ipValue / 256 / 256 / 256;
		left = left - first * 256 * 256 * 256;
		long second = left / 256 / 256;
		left = left - second * 256 * 256;
		long third = left / 256;
		long last = left - third * 256;

		return String.format("%s.%s.%s.%s", first, second, third, last);


	}

	public static List<String> rangeIp(String first, String last){

		List<String> ips = new ArrayList<>();
		long firstValue = ipToDecm(first);
		long lastValue = ipToDecm(last);

		for(long i = firstValue; i <= lastValue; i++){
			String ip = decmToIp(i);
			if(ip.endsWith(".0") || ip.endsWith(".255")){
				continue;
			}
			ips.add(ip);
		}

		return ips;
	}

	public static void main(String[] args) {

//		System.out.println(String.format("%08d", "9"));

//		System.out.println(ipToDecm("192.168.198.1"));
//
//		long dec = 3232286209L;
//
//		System.out.println(decmToIp(dec));

		List<String> ips = rangeIp("192.168.3.1", "192.168.5.1");
		ips.stream().forEach(System.out::println);

	}
}