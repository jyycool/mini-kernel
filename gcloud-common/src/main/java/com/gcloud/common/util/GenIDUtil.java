/*
 * @Date 2015-4-22
 * 
 * @Author dengyf@g-cloud.com.cn
 * 
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved. 
 * 
 * @Description 自动 生成 ID工具�? ，镜像ID、isoID、云服务�? ID�?
 */
package com.gcloud.common.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.zip.Adler32;

import com.google.common.primitives.Longs;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public class GenIDUtil {
	public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };
	
	public static String genernateId( final String prefix, final String location ) {
		Adler32 hash = new Adler32();
		String key = location;
		hash.update(key.getBytes());
		
	    /**
	     * @see http://tools.ietf.org/html/rfc3309
	     */
		for ( int i = key.length(); i < 128; i += 8 ) {
			hash.update( Longs.toByteArray( Double.doubleToRawLongBits( Math.random() ) ) );
		}
		String id = String.format( "%s-%08X", prefix, hash.getValue() );
		return id;
	}

	/**千万次内没有重复
	 * @param uuid
	 * @return
	 */
	public static String generateShortUuid(String uuid) {
	    StringBuffer shortBuffer = new StringBuffer();
	    if(StringUtils.isBlank(uuid)) {
	    	uuid = UUID.randomUUID().toString().replace("-", "");
	    }
	    for (int i = 0; i < 8; i++) {
	        String str = uuid.substring(i * 4, i * 4 + 4);
	        int x = Integer.parseInt(str, 16);
	        shortBuffer.append(chars[x % 0x3E]);
	    }
	    return shortBuffer.toString();
	}
	
	/**亲测千万次内没有重复
	 * @param uuid
	 * @return
	 */
	public static String generateShortUuidStrengthen(String uuid) {
	    StringBuffer shortBuffer = new StringBuffer();
	    if(StringUtils.isBlank(uuid)) {
	    	uuid = UUID.randomUUID().toString();
	    }
	    //System.out.println(uuid);
	    List<Integer> indexs = searchAllIndex(uuid, "-");
	    int j = 0;
	    int start = 0;
	    for (int i = 0; i <= 11; i++) {
	    	String str="";
	    	if(start >= uuid.length()) {
	    		break;
	    	}
	    	if(j<4) {
	    		if(start == indexs.get(j)) {
	    			start = start +1;
	    			j++;
	    			continue;
	    		}else if(start + 4 > indexs.get(j)) {
		    		str = uuid.substring(start, indexs.get(j++));
		    		start = indexs.get(j-1)+1;
	    		}else {
		    		str = uuid.substring(start, (start + 4)>uuid.length()?(uuid.length()-1):(start + 4));
		    		start = start + 4;
		    	}
	    		
	    	}else {
	    		str = uuid.substring(start, (start + 4)>uuid.length()?uuid.length():(start + 4));
	    		start = start + 4;
	    	}
	        int x = Integer.parseInt(str, 16);
	        shortBuffer.append(chars[x % 0x3E]);
	    }
	    //System.out.println(shortBuffer.toString());
	    return shortBuffer.toString();
	}
	
	private static List<Integer> searchAllIndex(String sourceStr, String searchStr) {
		List<Integer> allIndex = new ArrayList<Integer>();
        int index = sourceStr.indexOf(searchStr);
        allIndex.add(index);
        while (index != -1) {
            index = sourceStr.indexOf(searchStr, index + 1);
            allIndex.add(index);
        }
        return allIndex;
    }
	
	/*public static void main(String[] args) {
		List<String> ids = new ArrayList<String>();
		for(int i=0;i<10000000;i++) {
			ids.add(generateShortUuidTest(""));
		}
		
        if (cheakIsRepeat(ids)) {
            System.out.println("没有重复值！");
        } else {
            System.out.println("有重复�?�！");
        }
	}
	
	public static boolean cheakIsRepeat(List<String> list) {
        HashSet<String> hashSet = new HashSet<String>();
        for (int i = 0; i < list.size(); i++) {
            hashSet.add(list.get(i));
        }
        if (hashSet.size() == list.size()) {
            return true;
        } else {
            return false;
        }
    }*/

}