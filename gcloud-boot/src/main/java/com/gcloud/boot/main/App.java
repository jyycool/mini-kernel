package com.gcloud.boot.main;


import org.springframework.boot.SpringApplication;

import com.gcloud.boot.config.ActiveProfiles;
import com.gcloud.boot.config.Application;
import com.gcloud.boot.config.ApplicationWithoutDB;
import com.gcloud.boot.config.ComponentPackages;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */




public class App 
{
	public static void main(String[] args) {
		ActiveProfiles activeProfiles=ActiveProfiles.getInstance();
		activeProfiles.init(args);
		ComponentPackages.getInstance().init(activeProfiles.getIncludes());//初始数据用于fiterCustom过滤
		args=activeProfiles.process(args);
		if(activeProfiles.isWithDb()){
			SpringApplication.run(Application.class, args);
		}else{
			SpringApplication.run(ApplicationWithoutDB.class, args);
		}
	}
	
}