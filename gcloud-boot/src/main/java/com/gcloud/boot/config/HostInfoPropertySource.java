package com.gcloud.boot.config;


import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.core.env.PropertySource;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class HostInfoPropertySource extends PropertySource<Object>{
	public static final String HostInfo_PROPERTY_SOURCE_NAME = "hostInfo";
	private static final String PREFIX = "hostInfo";
	public HostInfoPropertySource(String name) {
		super(name, new Object());
		// TODO Auto-generated constructor stub
	}
	public HostInfoPropertySource() {
		this(HostInfo_PROPERTY_SOURCE_NAME);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getProperty(String name) {
		// TODO Auto-generated method stub
		if (!name.startsWith(PREFIX)) {
			return null;
		}
		try {
			InetAddress addr = InetAddress.getLocalHost();
			//String ip=addr.getHostAddress().toString(); //获取本机ip  
	        String hostName=addr.getHostName().toString(); //获取本机计算机名�?  
	        //return hostName+"-"+ip;
	        return hostName;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}

}