
USE gcloud_controller;

-- gcloud多实现    2019-05-21

ALTER TABLE `gc_routers` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_routers` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_routers` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_subnets` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_subnets` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_subnets` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_security_groups` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_security_groups` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_security_groups` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_floating_ips` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_floating_ips` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_floating_ips` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_networks` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_networks` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_networks` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_ports` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_ports` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_ports` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_slb` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_slb` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_slb` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_images` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_images` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_images` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_snapshots` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_snapshots` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_snapshots` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_volumes` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_volumes` ADD `provider_ref_id` varchar(40) DEFAULT null;
ALTER TABLE `gc_volumes` ADD `updated_at` datetime DEFAULT null;

ALTER TABLE `gc_storage_pools` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_storage_pools` ADD `provider_ref_id` varchar(40) DEFAULT null;

ALTER TABLE `gc_ovs_bridges` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_ovs_bridges` ADD `provider_ref_id` varchar(40) DEFAULT null;


-- gcloud磁盘类型    2019-06-05

DROP TABLE IF EXISTS `gc_disk_categories`;
CREATE TABLE `gc_disk_categories` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `min_size` int(10) DEFAULT null,
  `max_size` int(10) DEFAULT null,
  `zone_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_storage_pools` ADD `category_id` varchar(36) DEFAULT NULL;

ALTER TABLE `gc_volumes` ADD `pool_id` varchar(36) DEFAULT NULL;

-- gcloud可用区    2019-06-05

DROP TABLE IF EXISTS `gc_zones`;
CREATE TABLE `gc_zones` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_subnets` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_compute_nodes` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_instances` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_instance_types` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_volumes` ADD `zone_id` varchar(36) DEFAULT NULL;
ALTER TABLE `gc_storage_pools` ADD `zone_id` varchar(36) DEFAULT NULL;

-- ovs_bridge_id 2019-06-10
alter table gc_ports change custom_ovs_br ovs_bridge_id varchar(200) null;
alter table gc_ovs_bridges_usage modify id bigint(20) auto_increment;

-- 虚拟机表租户ID    2019-06-17
ALTER TABLE `gc_instances` ADD `tenant_id` varchar(64) DEFAULT null;
ALTER TABLE `gc_images` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_volumes` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_snapshots` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_floating_ips` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_networks` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_routers` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_subnets` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_security_groups` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_slb` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';
ALTER TABLE `gc_slb` ADD `user_id` varchar(64) DEFAULT NULL;

-- 网卡表租户ID    2019-06-18
ALTER TABLE `gc_ports` ADD `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID';


-- 负载均衡服务器组库表   2019-06-19
DROP TABLE IF EXISTS `gc_slb_vservergroup`;
CREATE TABLE `gc_slb_vservergroup` (
  `id` varchar(64) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `load_balancer_id` varchar(64) DEFAULT NULL,
  `protocol` varchar(64) DEFAULT NULL,
  `provider` tinyint(1) DEFAULT NULL,
  `provider_ref_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 浮动IP表qos、带宽字段   2019-06-19
ALTER TABLE `gc_floating_ips` ADD `bw_qos_policy_id` varchar(64) DEFAULT NULL COMMENT '带宽qos策略';
ALTER TABLE `gc_floating_ips` ADD `bandwidth` bigint(10) DEFAULT NULL COMMENT '带宽';
ALTER TABLE `gc_floating_ips` DROP column `routerId`;
ALTER TABLE `gc_floating_ips` ADD `router_id` varchar(64) DEFAULT NULL COMMENT '路由id';

-- 安全组规则表   2019-06-21
-- ----------------------------
--  Table structure for `gc_security_group_rules`
-- ----------------------------
DROP TABLE IF EXISTS `gc_security_group_rules`;
CREATE TABLE `gc_security_group_rules` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `security_group_id` varchar(64) NOT NULL COMMENT '安全组ID',
  `protocol` varchar(64) DEFAULT NULL COMMENT '协议类型,取值：tcp ，udp ，icmp，all，为all类型时，PortRange范围-1/-1',
  `port_range` varchar(64) DEFAULT NULL COMMENT '端口范围,IP协议相关的端口号范围如1/80，IpProtocol不是all则不为空',
  `remote_ip_prefix` varchar(64) DEFAULT NULL COMMENT '目标网段',
  `remote_group_id` varchar(64) DEFAULT NULL COMMENT '目标安全组',
  `direction` varchar(32) DEFAULT NULL COMMENT '方向',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `ethertype` varchar(64) DEFAULT NULL COMMENT '以太网类型，IPV4，IPV6',
  `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户ID',
  `user_id` varchar(32) DEFAULT NULL COMMENT '所有者用户ID',
  `provider` int(10) DEFAULT null,
  `provider_ref_id` varchar(40) DEFAULT null,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 默认安全组
ALTER TABLE gc_security_groups ADD is_default tinyint(1) null;

-- 任务流实例表、任务流实例步骤表添加最顶层任务id  2019-06-25
ALTER TABLE `gc_work_flow_instance` ADD `topest_flow_task_id` char(64) DEFAULT NULL COMMENT '最顶层任务id，根据这个字段判断是否在同一个任务流程';
ALTER TABLE `gc_work_flow_instance_step` ADD `topest_flow_task_id` char(64) DEFAULT NULL COMMENT '最顶层任务id，根据这个字段判断是否在同一个任务流程';

--
ALTER TABLE `gc_volume_attachments` ADD `provider` int(10) DEFAULT null;
ALTER TABLE `gc_volume_attachments` ADD `provider_ref_id` varchar(40) DEFAULT null;

-- 获取任务流步骤优先级
delimiter ~
DROP FUNCTION IF EXISTS getPriority~

CREATE FUNCTION getPriority (inID INT, flowId INT) RETURNS VARCHAR(255) DETERMINISTIC
begin
  DECLARE gParentID VARCHAR(255) DEFAULT '';
  DECLARE gPriority VARCHAR(255) DEFAULT '';
  SET gPriority = inID;
  SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = inID and flow_id=flowId;
  WHILE gParentID is not null DO  /*NULL为根*/
    SET gPriority = CONCAT(gParentID, '.', gPriority);
    SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = gParentID and flow_id=flowId;
  END WHILE;
  RETURN gPriority;
end~

delimiter ;

-- 存储池加上hostname    2019-06-28
ALTER TABLE `gc_storage_pools` ADD `hostname` varchar(36) DEFAULT NULL;

-- 添加回滚失败后是否必回滚
ALTER TABLE `gc_work_flow_template` ADD `rollback_fail_continue` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 流程回滚失败后不回滚，1 流程回滚失败后回滚；';
ALTER TABLE `gc_work_flow_instance_step` ADD `rollback_fail_continue` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 流程回滚失败后不回滚，1 流程回滚失败后回滚；';

-- 磁盘类型加上code    2019-07-03
ALTER TABLE `gc_disk_categories` ADD `code` varchar(50) NOT NULL;

-- 完成获取步骤层级顺序函数，因为mysql存储过程不支持递归，故用sub的形式调用。暂时不支持单步骤并行形式，只支持单步和分支。
delimiter ~
DROP FUNCTION IF EXISTS getPrioritySub~

CREATE FUNCTION getPrioritySub (inID INT, flowId INT) RETURNS VARCHAR(255) DETERMINISTIC
begin
  DECLARE gParentID VARCHAR(255) DEFAULT '';
  DECLARE gPriority VARCHAR(255) DEFAULT '';
  SET gPriority = inID;
  SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = inID and flow_id=flowId;
  WHILE gParentID is not null DO  /*NULL为根*/
    SET gPriority = CONCAT(gParentID, '.', gPriority);
    SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = gParentID and flow_id=flowId;
  END WHILE;
  RETURN gPriority;
end~

delimiter ;

delimiter ~
DROP FUNCTION IF EXISTS getPriority~

CREATE FUNCTION getPriority (inID INT, flowId INT) RETURNS VARCHAR(255) DETERMINISTIC
begin
  DECLARE gParentID VARCHAR(255) DEFAULT '';
  DECLARE gPriority VARCHAR(255) DEFAULT '';
  DECLARE commaLocate tinyint;
  DECLARE firstId VARCHAR(255) DEFAULT '';
  DECLARE secondId VARCHAR(255) DEFAULT '';
  DECLARE firstPriority VARCHAR(255) DEFAULT '';
  DECLARE secondPriority VARCHAR(255) DEFAULT '';
  DECLARE firstPriorityLevel tinyint;
  DECLARE secondPriorityLevel tinyint;
  DECLARE gNewParentID VARCHAR(255) DEFAULT '';
  SET commaLocate = 0;
  SET gPriority = inID;
  SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = inID and flow_id=flowId;
  WHILE gParentID is not null DO  /*NULL为根*/
    SET commaLocate = locate(',', gParentID);
    IF commaLocate >0 THEN
      SET firstId = SUBSTRING(gParentID,1,locate(',', gParentID)-1);
      SET secondId = SUBSTRING(gParentID,locate(',', gParentID)+1);
	    SET firstPriority = getPrioritySub(firstId, flowId);
      SET secondPriority = getPrioritySub(secondId, flowId);
      SET firstPriorityLevel = LENGTH(firstPriority)- LENGTH(REPLACE(firstPriority,'.',''));
      SET secondPriorityLevel = LENGTH(secondPriority)- LENGTH(REPLACE(secondPriority,'.',''));
      IF firstPriorityLevel = secondPriorityLevel THEN
        SET gPriority = CONCAT(gParentID, '.', gPriority);
      ELSE
        IF firstPriorityLevel > secondPriorityLevel THEN
          SET gNewParentID = CONCAT(secondId, ".",firstId);
				ELSE
				  SET gNewParentID = CONCAT(firstId, ".", secondId);
				end IF;
        SET gPriority = CONCAT(gNewParentID, '.', gPriority);
      END IF;
      SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = firstId and flow_id=flowId;
    ELSE
      SET gPriority = CONCAT(gParentID, '.', gPriority);
      SELECT from_ids INTO gParentID FROM gc_work_flow_instance_step WHERE template_step_id = gParentID and flow_id=flowId;
    END IF;
  END WHILE;
  RETURN gPriority;
end~

delimiter ;

-- 添加步骤描述字段  2019-7-8
 ALTER TABLE `gc_work_flow_template` ADD `step_desc` varchar(128) DEFAULT NULL COMMENT '步骤描述';
 ALTER TABLE `gc_work_flow_instance_step` ADD `step_desc` varchar(128) DEFAULT NULL COMMENT '步骤描述';
 ALTER TABLE `gc_work_flow_instance_step` ADD `rollback_start_time` datetime DEFAULT NULL COMMENT '回滚开始时间';
 ALTER TABLE `gc_work_flow_instance_step` ADD `rollback_update_time` datetime DEFAULT NULL COMMENT '回滚修改时间';

-- 实例类型添加zone_id 2019-7-11
ALTER TABLE `gc_instance_types` ADD `zone_id` varchar(36) DEFAULT NULL COMMENT '关联的可用区ID';

-- 存储池加上driver    2019-07-16
ALTER TABLE `gc_storage_pools` ADD `driver` varchar(36) DEFAULT NULL;


-- 存储类型 分布式改为 distributed 2019-07-17
alter table gc_storage_pools modify storage_type varchar(20) not null;
update gc_storage_pools set storage_type = 'distributed' where storage_type = 'rbd';
alter table gc_volumes modify storage_type varchar(20) not null;
alter table gc_snapshots modify storage_type varchar(20) not null;

-- 存储池增加connect_protocol 2019-07-18
alter table gc_storage_pools add connect_protocol varchar(20) null;

-- 修复slb问题
alter table gc_slb add vip_port_id varchar(36) null;
alter table gc_slb change vswitch_id vip_subnet_id varchar(64) not null;

DROP TABLE IF EXISTS `gc_image_stores`;
CREATE TABLE `gc_image_stores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID，自增',
  `image_id` char(64) NOT NULL COMMENT '镜像ID',
  `store_target` char(64) NOT NULL COMMENT '存储目标',
  `store_type` char(64) NOT NULL COMMENT '存储类型，node、vg、rbd',
  `status` char(32) NOT NULL COMMENT '镜像分发状态，downloading\deleting\active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_images` ADD `disable` tinyint(1) NOT NULL COMMENT '是否禁用，1禁用0可用';

-- 网络修改  2019-08-01
alter table gc_subnets drop column router_id;
alter table gc_routers change external_network_id external_gateway_network_id varchar(50) null comment '外网网关';
CREATE TABLE `gc_router_ports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `router_id` varchar(36) DEFAULT NULL,
  `port_id` varchar(36) DEFAULT NULL,
  `port_type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- 添加可用区节点关联表 2019-08-05
--删除掉可用区节点关联表 节点表中包含了可用区信息
--DROP TABLE IF EXISTS `gc_zone_node`;
--CREATE TABLE `gc_zone_node` (
--`id` VARCHAR(36) NOT NULL COMMENT 'id' ,
--`hostname` varchar(128) NOT NULL COMMENT '节点名称' ,
--`zone_id` varchar(36) NOT NULL COMMENT '可用区ID' ,
--PRIMARY KEY (`id`)
--) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='可用区节点';

-- 添加节点组表 2019-08-05
DROP TABLE IF EXISTS `gc_group_node`;
CREATE TABLE `gc_group_node` (
`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id' ,
`hostname` varchar(128) NOT NULL COMMENT '节点名称' ,
`group_id` varchar(36) NOT NULL COMMENT '分组ID' ,
`cluster_group_id` varchar(36) NULL DEFAULT NULL COMMENT '集群组ID' ,
`create_time` datetime NULL DEFAULT NULL COMMENT '入组时间' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `IDX_NODEGROUP_01` (`hostname`, `group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点组';

-- 打包流程修改 2019-08-06
INSERT INTO gc_work_flow_command_value_template (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`)
VALUES ('BundleInstanceWorkflow', 4, 'imageId', 1, 'res', 'imageId');

delete from gc_work_flow_template where flow_type_code = 'BundleInstanceWorkflow' and step_id in(2, 5, 6);
delete from gc_work_flow_command_value_template where flow_type_code = 'BundleInstanceWorkflow' and step_id in(2, 5, 6);

UPDATE gc_work_flow_template SET y_to_ids = '3' WHERE flow_type_code = 'BundleInstanceWorkflow' step_id = 1;
UPDATE gc_work_flow_template SET from_ids = '1' WHERE flow_type_code = 'BundleInstanceWorkflow' step_id = 3;
UPDATE gc_work_flow_template SET y_to_ids = '7' WHERE flow_type_code = 'BundleInstanceWorkflow' step_id = 8;
UPDATE gc_work_flow_template SET from_ids = '8' WHERE flow_type_code = 'BundleInstanceWorkflow' step_id = 7;

-- instance type 、 disk categories 和 storage_pool 关联可用区处理。
rename table gc_zones_instance_type to gc_zone_instance_types;


CREATE TABLE `gc_storage_pool_zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_pool_id` varchar(36) DEFAULT NULL,
  `zone_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `gc_disk_category_pools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disk_category_id` varchar(36) DEFAULT NULL,
  `zone_id` varchar(36) DEFAULT NULL,
  `storage_pool_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into gc_zone_instance_types (instance_type_id, zone_id) select t.id, t.zone_id from gc_instance_types t;
insert into gc_storage_pool_zones(storage_pool_id, zone_id) select t.id, t.zone_id from gc_storage_pools t;
insert into gc_disk_category_pools(disk_category_id, zone_id, storage_pool_id) SELECT t.category_id, t.zone_id, t.id from gc_storage_pools t;

ALTER TABLE gc_storage_pools ADD UNIQUE INDEX `UN_01` (`storage_type` ASC, `pool_name` ASC, `provider` ASC, `hostname` ASC);
alter table gc_disk_categories drop column zone_id;
alter table gc_storage_pools drop column zone_id;
alter table gc_storage_pools drop column category_id;
alter table gc_disk_categories drop column code;
alter table gc_instance_types drop column zone_id;

-- 添加可用区和磁盘类型的关联表  2019-08-15
CREATE TABLE `gc_zone_disk_categories` (
	`id` int(16) AUTO_INCREMENT NOT NULL,
	`zone_id` VARCHAR(36) NOT NULL,
	`disk_category_id` VARCHAR(36) NOT NULL,
	PRIMARY KEY(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 删除Gcloud image work flow
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DeleteGcloudImageWorkflow', 1, 'deleteGcloudImageFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudImageWorkflow', 2, 'deleteImageCacheFlowCommand', 'command', '1', '3', null, 0, 1, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1),
(null, 'DeleteGcloudImageWorkflow', 3, 'deleteImageCacheDoneFlowCommand', 'command', '2', null, null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DeleteGcloudImageWorkflow', 1, '', 0, '', ''),
(null, 'DeleteGcloudImageWorkflow', 2, 'repeatParams', 1, 'res', 'stores'),
(null, 'DeleteGcloudImageWorkflow', 2, 'imageId', 0, '', 'imageId'),
(null, 'DeleteGcloudImageWorkflow', 3, 'imageId', 0, '', 'imageId');

ALTER TABLE `gc_work_flow_task` ADD `need_feedback_log` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否需要feedback日志，0 | 1';

-- 2019.8.23 给可用区，实例类型，磁盘类型添加是否可用的字段
ALTER TABLE `gc_instance_types` ADD `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可用';
ALTER TABLE `gc_zones` ADD `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可用';
ALTER TABLE `gc_disk_categories` ADD `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可用';

-- 子网添加dns字段
ALTER TABLE `gc_subnets` ADD `dns_servers` varchar(255) DEFAULT NULL COMMENT 'dns';
ALTER TABLE `gc_networks` ADD `network_type` varchar(64) DEFAULT NULL COMMENT '网络模式，VLAN、FLAT';
ALTER TABLE `gc_networks` ADD `physical_network` varchar(64) DEFAULT NULL COMMENT '物理网络';
ALTER TABLE `gc_networks` ADD `segment_id` int(10) DEFAULT NULL COMMENT '段id';

-- 2019-08-26 qos多实现
alter table gc_qos_policies	add provider int(10) null;
alter table gc_qos_policies	add provider_ref_id varchar(40) null;

alter table gc_qos_bandwidth_limit_rules add provider int(10) null;
alter table gc_qos_bandwidth_limit_rules add provider_ref_id varchar(40) null;

-- 2019-8-27 子网添加网关字段
ALTER TABLE `gc_subnets` ADD `gateway_ip` varchar(64) DEFAULT NULL COMMENT '网关';

-- 2019-08-28 浮动ip qos
CREATE TABLE `gc_qos_fip_policy_bindings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `policy_id` varchar(36) DEFAULT NULL,
  `fip_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2019-08-28 在线快照 work flow
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'OnLineSnapshotWorkflow', 1, 'onLineSnapshotInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'OnLineSnapshotWorkflow', 2, 'instanceFsFreezeFlowCommand', 'command', 1, '3', null, 1, 1, 1, 1, 1, 'FROM_ONE_DONE', null, 1),
(null, 'OnLineSnapshotWorkflow', 3, 'createSnapshotFlowCommand', 'command', '2', '4', null, 1, 1, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'OnLineSnapshotWorkflow', 4, 'instanceFsThawFlowCommand', 'command', '3', null, null, 1, 1, 1, 1, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'OnLineSnapshotWorkflow', 1, '', 0, '', ''),
(null, 'OnLineSnapshotWorkflow', 2, 'resParam', 0, '', 'preRes'),
(null, 'OnLineSnapshotWorkflow', 3, 'diskId', 0, '', 'diskId'),
(null, 'OnLineSnapshotWorkflow', 3, 'name', 0, '', 'name'),
(null, 'OnLineSnapshotWorkflow', 3, 'description', 0, '', 'description'),
(null, 'OnLineSnapshotWorkflow', 3, 'currentUser', 0, '', 'currentUser'),
(null, 'OnLineSnapshotWorkflow', 3, 'snapshotId', 1, 'res', 'snapshotId'),
(null, 'OnLineSnapshotWorkflow', 4, 'resParam', 0, '', 'preRes');

-- 2019-08-28 安全组规则
alter table gc_security_group_rules drop column port_range;

alter table gc_security_group_rules	add port_range_max int null;

alter table gc_security_group_rules	add port_range_min int null;

-- 2019-08-29 更新在线快照 work flow
delete from gcloud_controller.gc_work_flow_template where flow_type_code='OnLineSnapshotWorkflow';
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'OnLineSnapshotWorkflow', 1, 'onLineSnapshotInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'OnLineSnapshotWorkflow', 2, 'instanceFsFreezeFlowCommand', 'command', 1, '3', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'OnLineSnapshotWorkflow', 3, 'createSnapshotFlowCommand', 'command', '2', '4', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'OnLineSnapshotWorkflow', 4, 'instanceFsThawFlowCommand', 'command', '3', null, null, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', null, 1);

-- 2019-08-30 修复创建虚拟机，网卡的安全组参数
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('CreateInstanceWorkflow', 10, 'securityGroupId', 0, '', 'securityGroupId');

-- 2019-08-30 操作日志归档表
create table gc_file_log like gc_log;

-- 2019-08-30 创建日志归档存储过程
DROP PROCEDURE IF EXISTS file_log;

delimiter $
create procedure file_log(IN logKeptDays int, IN fileLogKeptDays int)
begin
	IF fileLogKeptDays != -1 THEN
		INSERT into gc_file_log SELECT * from gc_log where DATE(start_time) <=  DATE(DATE_SUB(NOW(), INTERVAL logKeptDays DAY));
		DELETE FROM gc_file_log where DATE(start_time) <=  DATE(DATE_SUB(NOW(), INTERVAL fileLogKeptDays DAY));
	END IF;
  DELETE FROM gc_log where DATE(start_time) <=  DATE(DATE_SUB(NOW(), INTERVAL logKeptDays DAY));
end $
delimiter ;

-- 2019-9-6 资源租户ID不能为空
ALTER TABLE `gc_routers` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_subnets` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_security_groups` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_security_group_rules` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_floating_ips` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_networks` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_instances` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_ports` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_slb` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_snapshots` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_volumes` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';
ALTER TABLE `gc_images` MODIFY `tenant_id` varchar(64) NOT NULL COMMENT '租户ID';


-- 2019-9-27 存储节点汇报、lv汇报、池与节点关联表、删除磁盘流程添加失活lv步骤
DROP TABLE IF EXISTS `gc_storage_nodes`;
CREATE TABLE `gc_storage_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `hostname` varchar(128) NOT NULL COMMENT '节点全名',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `state` int(11) DEFAULT NULL COMMENT '状态\n0:没有注册,1已经注册',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储节点表';

DROP TABLE IF EXISTS `gc_storage_node_connect_logs`;
CREATE TABLE `gc_storage_node_connect_logs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(50) DEFAULT NULL,
  `log_code` varchar(50) DEFAULT NULL,
  `connect_type` varchar(20) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `gc_storage_pool_nodes`;
CREATE TABLE `gc_storage_pool_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_type` varchar(20) NOT NULL,
  `pool_name` varchar(200) NOT NULL,
  `provider` int(10) DEFAULT NULL,
  `hostname` varchar(128) NOT NULL COMMENT '节点全名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储池节点关联表';

DROP TABLE IF EXISTS `gc_storage_lv_nodes`;
CREATE TABLE `gc_storage_lv_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lv_path` varchar(128) NOT NULL COMMENT 'lv全路径',
  `hostname` varchar(128) NOT NULL COMMENT '节点全名',
  `state` char(16) NOT NULL COMMENT 'lv状态，active\unactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集中存储lv卷状态节点关联表';

delete from gcloud_controller.gc_work_flow_template where flow_type_code='DeleteDiskWorkflow';
delete from gcloud_controller.gc_work_flow_command_value_template where flow_type_code='DeleteDiskWorkflow';

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DeleteDiskWorkflow', 1, 'deleteDiskInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteDiskWorkflow', 2, 'deleteSnapshotFlowCommand', 'command', '1', '3', null, 1, 1, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1),
(null, 'DeleteDiskWorkflow', 3, 'unactiveLvDiskWorkFlow', 'task_flow', '2', '4', null, 1, 1, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteDiskWorkflow', 4, 'deleteDiskFlowCommand', 'command', '3', null, null, 1, 1, 0, 1, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DeleteDiskWorkflow', 1, '', 0, '', ''),
(null, 'DeleteDiskWorkflow', 2, 'repeatParams', 1, 'res', 'snapshotIds'),
(null, 'DeleteDiskWorkflow', 3, 'volumeId', 0, '', 'volumeId'),
(null, 'DeleteDiskWorkflow', 4, 'volumeId', 0, '', 'volumeId');

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'UnactiveLvDiskWorkFlow', 1, 'unactiveLvDiskInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'UnactiveLvDiskWorkFlow', 2, 'unactiveLvDiskFlowCommand', 'command', '1', null, null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'UnactiveLvDiskWorkFlow', 1, '', 0, '', ''),
(null, 'UnactiveLvDiskWorkFlow', 2, 'repeatParams', 1, 'res', 'lvNodes');

-- 2019-9-29 删除镜像接口改为任务流方式，并添加失活image cache lv任务流嵌套步骤
delete from gcloud_controller.gc_work_flow_template where flow_type_code='DeleteGcloudImageWorkflow';
delete from gcloud_controller.gc_work_flow_command_value_template where flow_type_code='DeleteGcloudImageWorkflow';

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DeleteGcloudImageWorkflow', 1, 'deleteGcloudImageFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudImageWorkflow', 2, 'unactiveLvImageWorkFlow', 'task_flow', '1', '3', null, 0, 1, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudImageWorkflow', 3, 'deleteImageCacheFlowCommand', 'command', '2', '4', null, 0, 1, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1),
(null, 'DeleteGcloudImageWorkflow', 4, 'deleteImageCacheDoneFlowCommand', 'command', '3', null, null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DeleteGcloudImageWorkflow', 1, '', 0, '', ''),
(null, 'DeleteGcloudImageWorkflow', 2, 'imageId', 0, '', 'imageId'),
(null, 'DeleteGcloudImageWorkflow', 3, 'repeatParams', 1, 'res', 'stores'),
(null, 'DeleteGcloudImageWorkflow', 3, 'imageId', 0, '', 'imageId'),
(null, 'DeleteGcloudImageWorkflow', 4, 'imageId', 0, '', 'imageId');

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'UnactiveLvImageWorkFlow', 1, 'unactiveLvImageInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'UnactiveLvImageWorkFlow', 2, 'unactiveLvDiskFlowCommand', 'command', '1', null, null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'UnactiveLvImageWorkFlow', 1, '', 0, '', ''),
(null, 'UnactiveLvImageWorkFlow', 2, 'repeatParams', 1, 'res', 'lvNodes');

-- 2019-10-14
alter table gc_images add parent_id varchar(36) null;

-- 2019-10-16 iso表结构
DROP TABLE IF EXISTS `gc_isos`;
CREATE TABLE `gc_isos` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `owner_type` varchar(50) DEFAULT null COMMENT 'public\private',
  `iso_type` varchar(50) NOT null COMMENT 'system\other',
  `os_type` varchar(50) DEFAULT null COMMENT 'linux\windows或空',
  `os_version` varchar(128) DEFAULT null COMMENT '如CentOS 7.0 Minimal-1503等',
  `architecture` varchar(50) DEFAULT null COMMENT 'x86_64等',
  `provider` int(10) DEFAULT null,
  `provider_ref_id` varchar(40) DEFAULT null,
  `tenant_id` varchar(64) NOT NULL COMMENT '租户ID',
  `disable` tinyint(1) NOT NULL COMMENT '是否禁用，1禁用0可用',
  `format` varchar(64) NOT NULL COMMENT 'iso',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `gc_vm_iso_attachments`;
CREATE TABLE `gc_vm_iso_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso_id` varchar(36) NOT NULL,
  `instance_uuid` varchar(36) DEFAULT NULL,
  `mountpoint` varchar(255) DEFAULT NULL,
  `status` varchar(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `iso_pool` varchar(255) NOT NULL,
  `iso_pool_id` varchar(255) NOT NULL,
  `iso_storage_type` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `gc_iso_map_infos`;
CREATE TABLE `gc_iso_map_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso_id` varchar(36) NOT NULL,
  `hostname` varchar(36) DEFAULT NULL,
  `map_path` varchar(255) DEFAULT NULL,
  `ln_path` varchar(255) DEFAULT NULL,
  `status` varchar(36) NOT NULL COMMENT 'mapping、active',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_image_stores` ADD `resource_type` char(64) NOT NULL COMMENT '资源类型，image、iso';

-- 2019-10-16 挂载光盘
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'AttachIsoWorkflow', 1, 'attachIsoInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'AttachIsoWorkflow', 2, 'checkAndDistributeIsoCacheFlowCommand', 'command', '1', '3', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'AttachIsoWorkflow', 3, 'rbdmapAndLnIsoCacheFlowCommand', 'command', '2', '4', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'AttachIsoWorkflow', 4, 'configIsoFileFlowCommand', 'command', '3', '5', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'AttachIsoWorkflow', 5, 'attachIsoDoneFlowCommand', 'command', '4', null, null, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'AttachIsoWorkflow', 1, 'instanceId', 0, '', 'instanceId'),
(null, 'AttachIsoWorkflow', 1, 'isoId', 0, '', 'isoId'),
(null, 'AttachIsoWorkflow', 2, 'isoId', 0, '', 'isoId'),
(null, 'AttachIsoWorkflow', 2, 'storagePoolId', 1, 'res', 'storagePoolId'),
(null, 'AttachIsoWorkflow', 2, 'hostname', 1, 'res', 'hostname'),
(null, 'AttachIsoWorkflow', 3, 'isoId', 0, '', 'isoId'),
(null, 'AttachIsoWorkflow', 3, 'storagePoolId', 1, 'res', 'storagePoolId'),
(null, 'AttachIsoWorkflow', 3, 'hostname', 1, 'res', 'hostname'),
(null, 'AttachIsoWorkflow', 4, 'instanceId', 0, '', 'instanceId'),
(null, 'AttachIsoWorkflow', 4, 'isoId', 0, '', 'isoId'),
(null, 'AttachIsoWorkflow', 4, 'storagePoolId', 1, 'res', 'storagePoolId'),
(null, 'AttachIsoWorkflow', 4, 'hostname', 1, 'res', 'hostname'),
(null, 'AttachIsoWorkflow', 5, 'instanceId', 0, '', 'instanceId');

-- 2019-10-16卸载光盘
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DetachIsoWorkflow', 1, 'detachIsoInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'DetachIsoWorkflow', 2, 'cleanIsoFileFlowCommand', 'command', '1', '3', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DetachIsoWorkflow', 3, 'detachIsoDoneFlowCommand', 'command', '2', null, null, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DetachIsoWorkflow', 1, 'instanceId', 0, '', 'instanceId'),
(null, 'DetachIsoWorkflow', 1, 'isoId', 0, '', 'isoId'),
(null, 'DetachIsoWorkflow', 1, 'inTask', 0, '', 'inTask'),
(null, 'DetachIsoWorkflow', 2, 'instanceId', 0, '', 'instanceId'),
(null, 'DetachIsoWorkflow', 2, 'hostname', 1, 'res', 'hostname'),
(null, 'DetachIsoWorkflow', 2, 'vmCdromDetail', 1, 'res', 'vmCdromDetail'),
(null, 'DetachIsoWorkflow', 3, 'instanceId', 0, '', 'instanceId');

-- 2019-10-16删除光盘
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DeleteGcloudIsoWorkflow', 1, 'deleteGcloudIsoInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudIsoWorkflow', 2, 'unmapAndDelLnGcloudIsoWorkFlow', 'task_flow', '1', '3', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudIsoWorkflow', 3, 'deleteGcloudIsoCacheWorkFlow', 'task_flow', '2', '4', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudIsoWorkflow', 4, 'deleteGcloudIsoFlowCommand', 'command', '3', null, null, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', null, 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DeleteGcloudIsoWorkflow', 1, 'isoId', 0, '', 'isoId'),
(null, 'DeleteGcloudIsoWorkflow', 2, 'isoId', 0, '', 'isoId'),
(null, 'DeleteGcloudIsoWorkflow', 3, 'isoId', 0, '', 'isoId'),
(null, 'DeleteGcloudIsoWorkflow', 4, 'isoId', 0, '', 'isoId');

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'UnmapAndDelLnGcloudIsoWorkFlow', 1, 'unmapAndDelLnGcloudIsoInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'UnmapAndDelLnGcloudIsoWorkFlow', 2, 'unmapAndDelLnGcloudIsoFlowCommand', 'command', '1', null, null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'UnmapAndDelLnGcloudIsoWorkFlow', 1, 'isoId', 0, '', 'isoId'),
(null, 'UnmapAndDelLnGcloudIsoWorkFlow', 2, 'repeatParams', 1, 'res', 'isoMaps');

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DeleteGcloudIsoCacheWorkFlow', 1, 'deleteGcloudIsoCacheInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteGcloudIsoCacheWorkFlow', 2, 'deleteGcloudIsoCacheFlowCommand', 'command', '1', null, null, 1, 0, 0, 1, 0, 'FROM_ONE_DONE', 'serial', 1);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DeleteGcloudIsoCacheWorkFlow', 1, 'isoId', 0, '', 'isoId'),
(null, 'DeleteGcloudIsoCacheWorkFlow', 2, 'repeatParams', 1, 'res', 'stores');

-- 2019-10-16修改用户ID字段长度为64
ALTER TABLE `gc_routers` MODIFY `user_id` varchar(64) DEFAULT NULL COMMENT '所有者用户ID';
ALTER TABLE `gc_subnets` MODIFY `user_id` varchar(64) DEFAULT NULL COMMENT '所有者用户ID';
ALTER TABLE `gc_security_groups` MODIFY `user_id` varchar(64) DEFAULT NULL COMMENT '所有者用户ID';
ALTER TABLE `gc_security_group_rules` MODIFY `user_id` varchar(64) DEFAULT NULL COMMENT '所有者用户ID';
ALTER TABLE `gc_log` MODIFY `user_id` varchar(64) DEFAULT NULL COMMENT '用户ID，关联用户表';
ALTER TABLE `gc_ports` MODIFY `user_id` varchar(64) DEFAULT NULL;
ALTER TABLE `gc_volumes` MODIFY `user_id` varchar(64) DEFAULT NULL;

-- 2019-10-16创建虚拟机流程兼容iso创建修改
delete from gcloud_controller.gc_work_flow_template where flow_type_code='CreateInstanceWorkflow';
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'CreateInstanceWorkflow', 1, 'createInstanceFlowInitCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 2, 'checkCreateVmNodeEnvFlowCommand', 'command', '1', '3', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 3, 'checkAndDistributeImageFlowCommand', 'command', '2', '4', null, 1, 1, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 4, 'createSystemDiskFlowCommand', 'command', '3', '5', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 5, 'connectDiskFlowCommand', 'command', '4', '6', null, 1, 0, 1, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 6, 'attachSystemDiskFlowCommand', 'command', '5', '7', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 7, 'buildVmConfigFlowCommand', 'command', '6', '9', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 8, 'createDomainFlowCommand', 'command', '14', '12', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 9, 'createAttachDataVolumeWorkflow', 'task_flow', '7', '10', null, 1, 1, 0, 0, 1, 'FROM_ONE_DONE', 'serial', 1),
(null, 'CreateInstanceWorkflow', 10, 'createAttachNetcardWorkflow', 'task_flow', '9', '14', null, 1, 1, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'CreateInstanceWorkflow', 14, 'attachIsoWorkflow', 'task_flow', '10', '8', null, 1, 1, 0, 0, 1, 'FROM_ONE_DONE', null, 0),
(null, 'CreateInstanceWorkflow', 11, 'createInstanceFlowDoneCommand', 'command', '13', null, null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 0),
(null, 'CreateInstanceWorkflow', '12', 'modifyInstancePasswordFlowCommand', 'command', '8', '13', NULL, '1', '1', '0', '1', '0', 'FROM_ONE_DONE', NULL, '1'),
(null, 'CreateInstanceWorkflow', '13', 'renameInstanceFlowCommand', 'command', '12', '11', NULL, '1', '1', '0', '1', '0', 'FROM_ONE_DONE', NULL, '1');

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'CreateInstanceWorkflow', 7, 'isoId', 0, '', 'isoId'),
(null,'CreateInstanceWorkflow',14,'instanceId',1,'res','instanceId'),
(null,'CreateInstanceWorkflow',14,'isoId',0,'','isoId');

-- 删除掉节点可用区表 2019.10.16
DROP TABLE IF EXISTS `gc_zone_node`;

-- 2019-10-16 租户ID不能为空
alter table gc_routers modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_subnets modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_security_groups modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_security_group_rules modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_floating_ips modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_networks modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_instances modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_ports modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_slb modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_snapshots modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_volumes modify tenant_id varchar(64) not null comment '租户ID';
alter table gc_images modify tenant_id varchar(64) not null comment '租户ID';

-- 2019-10-16 挂载光盘添加平台参数
INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'AttachIsoWorkflow', 4, 'platform', 1, 'res', 'platform');

-- 2019.10.17 迁移虚拟机流程
-- INSERT INTO `gc_work_flow_template` VALUES ('MigrateVmWorkflow', 1, 'migrateVmInitFlowCommand', 'command', NULL, '2', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
-- INSERT INTO `gc_work_flow_template` VALUES ('MigrateVmWorkflow', 2, 'checkTargetHostFlowCommand', 'command', '1', '3', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
-- INSERT INTO `gc_work_flow_template` VALUES ('MigrateVmWorkflow', 3, 'migrateAttachNetworkFlowCommand', 'command', '2', '4', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
-- INSERT INTO `gc_work_flow_template` VALUES ('MigrateVmWorkflow', 5, 'migrateAttachVolumeFlowCommand', 'command', '4', '6', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
-- INSERT INTO `gc_work_flow_template` VALUES ('MigrateVmWorkflow', 4, 'migrateFromSourceFlowCommand', 'command', '3', '5', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
-- INSERT INTO `gc_work_flow_template` VALUES ('MigrateVmWorkflow', 6, 'cleanSourceEnvFlowCommand', 'command', '5', NULL, NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
--
-- 2019.10.17 迁移虚拟机流程中的数据流
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 1, '', 0, '', '');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 2, '', 0, '', '');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 2, 'sourceHostName', 1, 'res', 'sourceHostName');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 3, '', 0, '', '');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 4, 'beginStatus', 1, 'res', 'beginStatus');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 4, '', 0, '', '');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 4, 'sourceHostName', 1, 'res', 'sourceHostName');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 5, 'sourceHostName', 1, 'res', 'sourceHostName');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 5, '', 0, '', '');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 6, 'networkDetailList', 3, 'res', 'vmNetworkDetails');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 6, 'sourceHostName', 1, 'res', 'sourceHostName');
-- INSERT INTO `gc_work_flow_command_value_template` VALUES ('MigrateVmWorkflow', 6, '', 0, '', '');

-- 2019-10-18
alter table gc_networks add `user_id` varchar(64) DEFAULT NULL COMMENT '拥有者';

-- 2019.10.21 初始化迁移虚拟机流程的相关内容
INSERT INTO `gc_work_flow_template` (`flow_type_code`, `step_id`, `exec_command`, `step_type`, `from_ids`, `y_to_ids`, `n_to_ids`, `necessary`, `async`, `rollback_async`, `rollback_skip`, `rollback_fail_continue`, `from_relation`, `repeat_type`, `visible`, `step_desc`) VALUES ('MigrateVmWorkflow', 1, 'migrateVmInitFlowCommand', 'command', NULL, '2', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
INSERT INTO `gc_work_flow_template` (`flow_type_code`, `step_id`, `exec_command`, `step_type`, `from_ids`, `y_to_ids`, `n_to_ids`, `necessary`, `async`, `rollback_async`, `rollback_skip`, `rollback_fail_continue`, `from_relation`, `repeat_type`, `visible`, `step_desc`) VALUES ('MigrateVmWorkflow', 2, 'checkTargetHostFlowCommand', 'command', '1', '3', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
INSERT INTO `gc_work_flow_template` (`flow_type_code`, `step_id`, `exec_command`, `step_type`, `from_ids`, `y_to_ids`, `n_to_ids`, `necessary`, `async`, `rollback_async`, `rollback_skip`, `rollback_fail_continue`, `from_relation`, `repeat_type`, `visible`, `step_desc`) VALUES ('MigrateVmWorkflow', 3, 'migrateAttachNetworkFlowCommand', 'command', '2', '4', NULL, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', NULL, 1, NULL);
INSERT INTO `gc_work_flow_template` (`flow_type_code`, `step_id`, `exec_command`, `step_type`, `from_ids`, `y_to_ids`, `n_to_ids`, `necessary`, `async`, `rollback_async`, `rollback_skip`, `rollback_fail_continue`, `from_relation`, `repeat_type`, `visible`, `step_desc`) VALUES ('MigrateVmWorkflow', 4, 'migrateFromSourceFlowCommand', 'command', '3', '5', NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);
INSERT INTO `gc_work_flow_template` (`flow_type_code`, `step_id`, `exec_command`, `step_type`, `from_ids`, `y_to_ids`, `n_to_ids`, `necessary`, `async`, `rollback_async`, `rollback_skip`, `rollback_fail_continue`, `from_relation`, `repeat_type`, `visible`, `step_desc`) VALUES ('MigrateVmWorkflow', 5, 'migrateAttachVolumeFlowCommand', 'command', '4', '6', NULL, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', NULL, 1, NULL);
INSERT INTO `gc_work_flow_template` (`flow_type_code`, `step_id`, `exec_command`, `step_type`, `from_ids`, `y_to_ids`, `n_to_ids`, `necessary`, `async`, `rollback_async`, `rollback_skip`, `rollback_fail_continue`, `from_relation`, `repeat_type`, `visible`, `step_desc`) VALUES ('MigrateVmWorkflow', 6, 'cleanSourceEnvFlowCommand', 'command', '5', NULL, NULL, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', NULL, 1, NULL);

INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 1, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 2, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 2, 'sourceHostName', 1, 'res', 'sourceHostName');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 3, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 4, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 4, 'sourceHostName', 1, 'res', 'sourceHostName');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 4, 'beginStatus', 1, 'res', 'beginStatus');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 5, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 6, 'networkDetailList', 3, 'res', 'vmNetworkDetails');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 6, 'sourceHostName', 1, 'res', 'sourceHostName');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 6, '', 0, '', '');
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('MigrateVmWorkflow', 6, 'volumeAttachment', 5, 'res', 'volumeAttachment');


-- 2019-10-22  dhcp 分配池
CREATE TABLE `gc_ipallocation_pools` (
  `id` varchar(36) NOT NULL,
  `subnet_id` varchar(36) DEFAULT NULL,
  `first_ip` varchar(50) DEFAULT NULL,
  `last_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gc_subnets`
ADD COLUMN `enable_dhcp` TINYINT(1) NULL;

UPDATE `gc_subnets` SET enable_dhcp = true where enable_dhcp is null;

-- 2019-10-23
ALTER TABLE `gc_vm_iso_attachments` drop `instance_uuid`;
ALTER TABLE `gc_vm_iso_attachments` ADD `instance_id` varchar(36) DEFAULT NULL;

-- 2019-10-23
INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'AttachIsoWorkflow', 1, 'createVmTask', 0, '', 'createVmTask');

-- 2019-10-23
CREATE TABLE `gc_message_tasks` (
  `task_id` varchar(36) NOT NULL,
  `message` varchar(200) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `timeout_time` datetime DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `mdata` text,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2019.10.24 给存储池添加创建时间
ALTER TABLE `gc_storage_pools` ADD `create_time` datetime COMMENT '创建时间';

-- 2019.10.24 给磁盘类型添加创建时间
ALTER TABLE `gc_disk_categories` ADD `create_time` datetime COMMENT '创建时间';

-- 2019-10-25 日志表记录功能路径
ALTER TABLE `gc_log` ADD `fun_path` varchar(64) DEFAULT NULL COMMENT '功能路径';
ALTER TABLE `gc_file_log` ADD `fun_path` varchar(64) DEFAULT NULL COMMENT '功能路径';

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'CreateInstanceWorkflow', 10, 'portName', 1, 'res', 'portName');

-- 2019.10.31 负载均衡的监听器表的后端服务器ID可以为空
ALTER TABLE `gc_slb_listener` MODIFY `vserver_group_id` varchar(64) NULL;

-- 2019-11-1
ALTER TABLE `gc_iso_map_infos` DROP column `map_path`;
ALTER TABLE `gc_iso_map_infos` ADD `pool_name` varchar(200) NOT NULL;
update `gc_iso_map_infos` set `pool_name`='isos';

-- 2019-11-4
ALTER TABLE `gc_floating_ips` DROP column `instance_id`;
ALTER TABLE `gc_floating_ips` DROP column `instance_type`;

-- 2019-11-6
-- 更新前需要导出之前的表数据
DROP TABLE `gc_image_stores`;
DROP TABLE IF EXISTS `gc_image_stores`;
CREATE TABLE `gc_image_stores` (
  `id` char(255) NOT NULL unique COMMENT 'ID',
  `image_id` char(64) NOT NULL COMMENT '镜像ID',
  `store_target` char(64) NOT NULL COMMENT '存储目标',
  `store_type` char(64) NOT NULL COMMENT '存储类型，node、vg、rbd',
  `status` char(32) NOT NULL COMMENT '镜像分发状态，downloading\deleting\active',
  `resource_type` char(64) NOT NULL COMMENT '资源类型，image、iso',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 更新后把之前导出的表数据导入

-- 2019-11-7
-- 更新前需要导出之前的表数据
DROP TABLE IF EXISTS `gc_iso_map_infos`;
CREATE TABLE `gc_iso_map_infos` (
  `id` char(255) NOT NULL unique COMMENT 'ID',
  `iso_id` varchar(36) NOT NULL,
  `hostname` varchar(36) DEFAULT NULL,
  `pool_name` varchar(200) NOT NULL,
  `ln_path` varchar(255) DEFAULT NULL,
  `status` varchar(36) NOT NULL COMMENT 'mapping、active',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 更新后把之前导出的表数据导入

-- 2019-11-8
INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null,'CreateInstanceWorkflow',12,'isoCreate',1,'res','isoCreate'),
(null,'CreateInstanceWorkflow',13,'isoCreate',1,'res','isoCreate');


-- 迁移优化  2019-11-8
delete from gcloud_controller.gc_work_flow_template where flow_type_code = 'MigrateVmWorkflow';
delete from gcloud_controller.gc_work_flow_command_value_template where flow_type_code = 'MigrateVmWorkflow';

INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible, step_desc) VALUES
(null, 'MigrateVmWorkflow', 1, 'migrateVmInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 2, 'migrateActiveImageFlowCommand', 'command', '1', '3', null, 1, 1, 1, 1, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 3, 'migrateActiveIsoFlowCommand', 'command', '2', '4', null, 1, 1, 1, 1, 1, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 4, 'migrateAttachNetworkFlowCommand', 'command', '3', '5', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 5, 'migrateAttachVolumeFlowCommand', 'command', '4', '6', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 6, 'migrateFromSourceFlowCommand', 'command', '5', '7', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 7, 'migrateDetachSourceVolumeFlowCommand', 'command', '6', '8', null, 1, 0, 1, 0, 0, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 8, 'migrateDetachSourceNetcardFlowCommand', 'command', '7', '9', null, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 9, 'cleanSourceEnvFlowCommand', 'command', '8', '10', null, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 10, 'migrateVmDoneFlowCommand', 'command', '9', null, null, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', null, 0, null);


INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name) VALUES
(null, 'MigrateVmWorkflow', 1, '', 0, '', ''),
(null, 'MigrateVmWorkflow', 2, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 2, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 3, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 3, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 3, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 3, 'repeatParams', 1, 'res', 'isos'),
(null, 'MigrateVmWorkflow', 4, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 4, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 4, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 4, 'repeatParams', 1, 'res', 'netcards'),
(null, 'MigrateVmWorkflow', 5, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 5, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 5, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 5, 'repeatParams', 1, 'res', 'disks'),
(null, 'MigrateVmWorkflow', 6, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 6, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 6, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 6, 'beginStatus', 1, 'res', 'beginStatus'),
(null, 'MigrateVmWorkflow', 7, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 7, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 7, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 7, 'repeatParams', 1, 'res', 'disks'),
(null, 'MigrateVmWorkflow', 8, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 8, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 8, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 8, 'repeatParams', 1, 'res', 'netcards'),
(null, 'MigrateVmWorkflow', 9, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 9, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 10, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 10, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 10, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 10, 'beginStatus', 1, 'res', 'beginStatus');


ALTER TABLE `gcloud_controller`.`gc_volume_attachments`
ADD UNIQUE INDEX `unq_volume_attachment_01` (`volume_id` ASC, `attached_host` ASC, `instance_uuid` ASC);

ALTER TABLE `gcloud_controller`.`gc_instances`
ADD COLUMN `migrate_to` VARCHAR(128) NULL AFTER `hostname`;

-- 迁移网络修复  2019-11-13
delete from gcloud_controller.gc_work_flow_template where flow_type_code = 'MigrateVmWorkflow';
delete from gcloud_controller.gc_work_flow_command_value_template where flow_type_code = 'MigrateVmWorkflow';

delete from gcloud_controller.gc_work_flow_template where flow_type_code = 'MigrateAttachNetworkWorkflow';
delete from gcloud_controller.gc_work_flow_command_value_template where flow_type_code = 'MigrateAttachNetworkWorkflow';


INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible, step_desc) VALUES
(null, 'MigrateVmWorkflow', 1, 'migrateVmInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 2, 'migrateActiveImageFlowCommand', 'command', '1', '3', null, 1, 1, 1, 1, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 3, 'migrateActiveIsoFlowCommand', 'command', '2', '4', null, 1, 1, 1, 1, 1, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 4, 'migrateAttachNetworkWorkflow', 'task_flow', '3', '5', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 5, 'migrateAttachVolumeFlowCommand', 'command', '4', '6', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 6, 'migrateFromSourceFlowCommand', 'command', '5', '7', null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 7, 'migrateDetachSourceVolumeFlowCommand', 'command', '6', '8', null, 1, 0, 1, 0, 0, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 8, 'migrateDetachSourceNetcardFlowCommand', 'command', '7', '9', null, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', 'serial', 0, null),
(null, 'MigrateVmWorkflow', 9, 'cleanSourceEnvFlowCommand', 'command', '8', '10', null, 1, 1, 1, 0, 0, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateVmWorkflow', 10, 'migrateVmDoneFlowCommand', 'command', '9', null, null, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateAttachNetworkWorkflow', 1, 'migrateAttachPortFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 0, null),
(null, 'MigrateAttachNetworkWorkflow', 2, 'migrateAttachNetworkFlowCommand', 'command', '1', null, null, 1, 1, 1, 0, 1, 'FROM_ONE_DONE', null, 0, null);

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name) VALUES
(null, 'MigrateVmWorkflow', 1, '', 0, '', ''),
(null, 'MigrateVmWorkflow', 2, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 2, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 3, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 3, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 3, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 3, 'repeatParams', 1, 'res', 'isos'),
(null, 'MigrateVmWorkflow', 4, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 4, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 4, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 4, 'repeatParams', 1, 'res', 'netcards'),
(null, 'MigrateVmWorkflow', 5, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 5, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 5, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 5, 'repeatParams', 1, 'res', 'disks'),
(null, 'MigrateVmWorkflow', 6, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 6, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 6, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 6, 'beginStatus', 1, 'res', 'beginStatus'),
(null, 'MigrateVmWorkflow', 7, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 7, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 7, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 7, 'repeatParams', 1, 'res', 'disks'),
(null, 'MigrateVmWorkflow', 8, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 8, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 8, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 8, 'repeatParams', 1, 'res', 'netcards'),
(null, 'MigrateVmWorkflow', 9, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 9, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 10, 'instanceId', 1, 'res', 'instanceId'),
(null, 'MigrateVmWorkflow', 10, 'targetHostName', 1, 'res', 'targetHostName'),
(null, 'MigrateVmWorkflow', 10, 'sourceHostName', 1, 'res', 'sourceHostName'),
(null, 'MigrateVmWorkflow', 10, 'beginStatus', 1, 'res', 'beginStatus'),
(null, 'MigrateAttachNetworkWorkflow', 1, '', 0, '', ''),
(null, 'MigrateAttachNetworkWorkflow', 1, 'netcardInfo', 0, '', 'repeatParams'),
(null, 'MigrateAttachNetworkWorkflow', 2, '', 0, '', ''),
(null, 'MigrateAttachNetworkWorkflow', 2, 'netcardInfo', 0, '', 'repeatParams');




-- dhcp 端口维护
ALTER TABLE `gc_ports`
ADD UNIQUE INDEX `unique_ports_01` (`provider` ASC, `provider_ref_id` ASC);

CREATE TABLE `gc_subnet_dhcp_handles` (
  `subnet_id` varchar(50) DEFAULT NULL,
  `handle_id` varchar(50) DEFAULT NULL,
  `delete_subnet` tinyint(1) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `retry` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2019-11-25 挂载光盘清除taskState
INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'AttachIsoWorkflow', 5, 'createVmTask', 0, '', 'createVmTask');

-- 2019-11-26 iso创建虚拟机 
INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null,'CreateInstanceWorkflow',14,'createVmTask',1,'res','isoCreate');

-- 2019.12.9 磁盘类型添加存储类型
ALTER TABLE `gcloud_controller`.`gc_disk_categories` 
ADD COLUMN `storage_type` VARCHAR(20) NOT NULL DEFAULT 'distributed' AFTER `max_size`;

-- 2019.12.11创建虚拟机流程参数修改
INSERT INTO `gc_work_flow_command_value_template` (`flow_type_code`, `step_id`, `field_name`, `from_step_id`, `from_param_type`, `from_field_name`) VALUES ('CreateInstanceWorkflow', 3, 'systemDisk', 1, 'res', 'systemDisk');

-- 2019-12-13 节点表中的节点名添加索引
create unique index UK_gc_compute_nodes_hostname on gc_compute_nodes (`hostname`);

-- 2019-12-14 磁盘类型关联可用区
ALTER TABLE `gcloud_controller`.`gc_disk_category_pools`
ADD COLUMN `storage_type` VARCHAR(20) NULL,
ADD COLUMN `hostname` VARCHAR(200) NOT NULL;

UPDATE gc_disk_category_pools set hostname = (select ifnull(p.hostname, '') from gc_storage_pools p where p.id = storage_pool_id), storage_type = (select p.storage_type from gc_storage_pools p where p.id = storage_pool_id) where storage_type is null;

ALTER TABLE `gcloud_controller`.`gc_disk_category_pools`
ADD UNIQUE INDEX `UNI_IDX_01` (`disk_category_id` ASC, `zone_id` ASC, `storage_type` ASC, `hostname` ASC);

-- 添加存储池信息收集任务表 2019-12-17
DROP TABLE IF EXISTS `gc_storage_pool_collect_cron_tasks`;
CREATE TABLE `gc_storage_pool_collect_cron_tasks` (
`batch_id` varchar(64) NOT NULL COMMENT '收集批次ID' ,
`state` varchar(36) NOT NULL COMMENT 'collecting、collected' ,
`create_time` datetime NULL DEFAULT NULL COMMENT '开始时间' ,
PRIMARY KEY (`batch_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储池信息收集任务表';

DROP TABLE IF EXISTS `gc_storage_pool_collect_details`;
CREATE TABLE `gc_storage_pool_collect_details` (
`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id' ,
`batch_id` varchar(64) NOT NULL COMMENT '批次ID' ,
`pool_id` varchar(64) NOT NULL COMMENT '池ID' ,
`avail` int(11) default 0 COMMENT '可用大小',
`used` int(11) default 0 COMMENT '已用大小',
`total` int(11) default 0 COMMENT '总大小',
`state` varchar(36) NOT NULL COMMENT 'collecting、collected' ,
`create_time` datetime NULL DEFAULT NULL COMMENT '创建时间' ,
PRIMARY KEY (`id`) ,
UNIQUE INDEX `IDX_BATCH_POOL_01` (`batch_id`, `pool_id`) USING BTREE 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储池信息收集详细表';

-- 2020-1-6 删除虚拟机时卸载光盘
delete from gcloud_controller.gc_work_flow_template where flow_type_code='DeleteInstanceWorkflow';
INSERT INTO gcloud_controller.gc_work_flow_template (id, flow_type_code, step_id, exec_command, step_type, from_ids, y_to_ids, n_to_ids, necessary, async, rollback_async, rollback_skip, rollback_fail_continue, from_relation, repeat_type, visible)
VALUES
(null, 'DeleteInstanceWorkflow', 1, 'deleteInstanceInitFlowCommand', 'command', null, '2', null, 1, 0, 0, 0, 1, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteInstanceWorkflow', 2, 'destroyInstanceIfExistFlowCommand', 'command', '1', '3', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteInstanceWorkflow', 3, 'forceDetachAndDeleteDiskWorkflow', 'task_flow', '2', '4', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', 'serial', 1),
(null, 'DeleteInstanceWorkflow', 4, 'forceDetachAndDeleteNetcardWorkflow', 'task_flow', '3', '5', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', 'serial', 1),
(null, 'DeleteInstanceWorkflow', 5, 'detachIsoWorkflow', 'task_flow', '4', '6', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteInstanceWorkflow', 6, 'cleanInstanceFileFlowCommand', 'command', '5', '7', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteInstanceWorkflow', 7, 'cleanInstanceInfoFlowCommand', 'command', '6', '8', null, 1, 1, 0, 0, 0, 'FROM_ONE_DONE', null, 1),
(null, 'DeleteInstanceWorkflow', 8, 'deleteInstanceDoneFlowCommand', 'command', '7', null, null, 1, 0, 0, 0, 0, 'FROM_ONE_DONE', null, 1);


delete from gcloud_controller.gc_work_flow_command_value_template where flow_type_code='DeleteInstanceWorkflow';
INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DeleteInstanceWorkflow', 1, '', 0, '', ''),
(null, 'DeleteInstanceWorkflow', 2, 'instanceId', 0, '', 'instanceId'),
(null, 'DeleteInstanceWorkflow', 3, 'instanceId', 0, '', 'instanceId'),
(null, 'DeleteInstanceWorkflow', 3, 'repeatParams', 1, 'res', 'disks'),
(null, 'DeleteInstanceWorkflow', 4, 'instanceId', 0, '', 'instanceId'),
(null, 'DeleteInstanceWorkflow', 4, 'repeatParams', 1, 'res', 'netcards'),
(null, 'DeleteInstanceWorkflow', 5, 'instanceId', 0, '', 'instanceId'),
(null, 'DeleteInstanceWorkflow', 5, 'isoId', 1, 'res', 'isoId'),
(null, 'DeleteInstanceWorkflow', 5, 'inTask', 1, 'res', 'inTask'),
(null, 'DeleteInstanceWorkflow', 6, 'instanceId', 0, '', 'instanceId'),
(null, 'DeleteInstanceWorkflow', 7, 'instanceId', 0, '', 'instanceId'),
(null, 'DeleteInstanceWorkflow', 8, 'instanceId', 0, '', 'instanceId');

INSERT INTO gcloud_controller.gc_work_flow_command_value_template (id, flow_type_code, step_id, field_name, from_step_id, from_param_type, from_field_name)
VALUES
(null, 'DetachIsoWorkflow', 1, 'inTask', 0, '', 'inTask');