package com.gcloud.storage;

import com.gcloud.storage.service.impl.ReportStoragePoolModel;
import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Data
@ConfigurationProperties(prefix = "gcloud.storage-node")
public class StorageNodeProp {

    private Boolean reportPools;

    private List<ReportStoragePoolModel> pools;

    private String imageCachedPath = "/opt/gcloud/storage/imageCaches/";
    
    private String isoCachedPath = "/opt/gcloud/storage/isoCaches/";
    
	private int reportFrequency;

}