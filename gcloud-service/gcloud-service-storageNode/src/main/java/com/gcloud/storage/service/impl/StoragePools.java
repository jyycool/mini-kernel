package com.gcloud.storage.service.impl;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.SnapshotType;
import com.gcloud.storage.NodeStoragePoolVo;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class StoragePools {

    private static final Map<String, NodeStoragePoolVo> POOLS = new HashMap<>();

    public void add(Integer provider, String storageType, String poolName, String categoryCode, String snapshotType) {
        NodeStoragePoolVo poolVo = new NodeStoragePoolVo();
        poolVo.setProvider(provider);
        poolVo.setStorageType(storageType);
        poolVo.setPoolName(poolName);
        poolVo.setCategoryCode(categoryCode);
        poolVo.setSnapshotType(SnapshotType.get(snapshotType));
        POOLS.put(poolVo.getPoolName(), poolVo);
    }

    public NodeStoragePoolVo get(String poolName) {
        return POOLS.get(poolName);
    }

    public NodeStoragePoolVo checkAndGet(String poolName) throws GCloudException {
        NodeStoragePoolVo pool = POOLS.get(poolName);
        if (pool == null) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_POOL);
        }
        return pool;
    }

}