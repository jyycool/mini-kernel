package com.gcloud.controller.monitor.service;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.monitor.msg.api.ApiDescribeDiskMonitorDataHandlerMsg;
import com.gcloud.header.monitor.msg.api.ApiDescribeDiskMonitorDataHandlerReplyMsg;
import com.gcloud.header.monitor.msg.api.ApiDescribeInstanceMonitorDataHandlerMsg;
import com.gcloud.header.monitor.msg.api.ApiDescribeInstanceMonitorDataHandlerReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IMonitorService {

	ApiDescribeInstanceMonitorDataHandlerReplyMsg describeInstanceMonitorData(ApiDescribeInstanceMonitorDataHandlerMsg msg) throws GCloudException, Exception;
	
	ApiDescribeDiskMonitorDataHandlerReplyMsg describeDiskMonitorData(ApiDescribeDiskMonitorDataHandlerMsg msg) throws GCloudException, Exception;
}