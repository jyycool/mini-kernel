package com.gcloud.controller.storage.model;

import com.gcloud.common.model.PageParams;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeStoragePoolsParams extends PageParams{
    private String poolId;
    private String storageType;
    private String zoneId;
    private Boolean associated;
    private String associateDiskCategoryId;
    private String associateZoneId;
	private String poolName;
	private String displayName;
	private String hostname;

	public String getPoolId() {
		return poolId;
	}
	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public Boolean getAssociated() {
		return associated;
	}
	public void setAssociated(Boolean associated) {
		this.associated = associated;
	}
	public String getAssociateDiskCategoryId() {
		return associateDiskCategoryId;
	}
	public void setAssociateDiskCategoryId(String associateDiskCategoryId) {
		this.associateDiskCategoryId = associateDiskCategoryId;
	}
	public String getAssociateZoneId() {
		return associateZoneId;
	}
	public void setAssociateZoneId(String associateZoneId) {
		this.associateZoneId = associateZoneId;
	}
	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}