package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.CreateDiskCategoryParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiCreateDiskCategoryMsg;
import com.gcloud.header.storage.msg.api.pool.ApiCreateDiskCategoryReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@GcLog(taskExpect="创建磁盘类型")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.CREATE_DISK_CATEGORY, name = "创建磁盘类型")
public class ApiCreateDiskCategoryHandler extends MessageHandler<ApiCreateDiskCategoryMsg, ApiCreateDiskCategoryReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiCreateDiskCategoryReplyMsg handle(ApiCreateDiskCategoryMsg msg) throws GCloudException {
    	CreateDiskCategoryParams params = BeanUtil.copyProperties(msg, CreateDiskCategoryParams.class);
    	String typeId = poolService.createDiskCategory(params);
    	
        ApiCreateDiskCategoryReplyMsg reply = new ApiCreateDiskCategoryReplyMsg();
        reply.setTypeId(typeId);
        //添加操作日志
        msg.setObjectId(typeId);
        msg.setObjectName(msg.getName());
        return reply;
    }

}