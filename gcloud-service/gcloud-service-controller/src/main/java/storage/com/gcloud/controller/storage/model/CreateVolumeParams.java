package com.gcloud.controller.storage.model;

import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.header.storage.enums.DiskType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class CreateVolumeParams {

    private String diskName;
    private String description;
    private Integer size;
    private String snapshotId;
    private String diskCategory;
    private DiskType diskType;
    private boolean bootable;
    private String imageRef;
    private String zoneId;
    private String poolId;

    private String imageProviderRefId;
    private Integer imageProvider;

    private String createHost;

    private StoragePool pool;
    
    private String taskId;


    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getDiskCategory() {
        return diskCategory;
    }

    public void setDiskCategory(String diskCategory) {
        this.diskCategory = diskCategory;
    }


    public DiskType getDiskType() {
        return diskType;
    }

    public void setDiskType(DiskType diskType) {
        this.diskType = diskType;
    }

	public boolean isBootable() {
		return bootable;
	}

	public void setBootable(boolean bootable) {
		this.bootable = bootable;
	}

	public String getImageRef() {
		return imageRef;
	}

	public void setImageRef(String imageRef) {
		this.imageRef = imageRef;
	}

	public StoragePool getPool() {
        return pool;
    }

    public void setPool(StoragePool pool) {
        this.pool = pool;
    }

    public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getImageProviderRefId() {
        return imageProviderRefId;
    }

    public void setImageProviderRefId(String imageProviderRefId) {
        this.imageProviderRefId = imageProviderRefId;
    }

    public Integer getImageProvider() {
        return imageProvider;
    }

    public void setImageProvider(Integer imageProvider) {
        this.imageProvider = imageProvider;
    }

    public String getCreateHost() {
        return createHost;
    }

    public void setCreateHost(String createHost) {
        this.createHost = createHost;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }
}