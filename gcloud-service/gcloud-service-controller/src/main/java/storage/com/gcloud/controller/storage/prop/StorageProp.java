package com.gcloud.controller.storage.prop;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@ConfigurationProperties(prefix = "gcloud.controller.storage")
public class StorageProp {
	 @Value("${gcloud.controller.storage.cephOperator:gcloud}")
	 private String cephOperator;//ceph操作�?
	 
	 @Value("${gcloud.controller.storage.cephImagePool:images}")
	 private String cephImagePool;//ceph镜像�?
	 
	 @Value("${gcloud.controller.storage.cephIsoPool:isos}")
	 private String cephIsoPool;//ceph映像�?
	 
	 @Value("${gcloud.controller.storage.pool.cron:0 */1 * * * ?}")
	 private String poolCollectCron;

	public String getPoolCollectCron() {
		return poolCollectCron;
	}

	public void setPoolCollectCron(String poolCollectCron) {
		this.poolCollectCron = poolCollectCron;
	}

	public String getCephOperator() {
		return cephOperator;
	}

	public void setCephOperator(String cephOperator) {
		this.cephOperator = cephOperator;
	}

	public String getCephImagePool() {
		return cephImagePool;
	}

	public void setCephImagePool(String cephImagePool) {
		this.cephImagePool = cephImagePool;
	}

	public String getCephIsoPool() {
		return cephIsoPool;
	}

	public void setCephIsoPool(String cephIsoPool) {
		this.cephIsoPool = cephIsoPool;
	}
}