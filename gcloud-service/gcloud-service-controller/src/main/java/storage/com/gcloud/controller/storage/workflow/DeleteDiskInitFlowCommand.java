package com.gcloud.controller.storage.workflow;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.controller.storage.workflow.model.volume.DeleteDiskInitFlowCommandReq;
import com.gcloud.controller.storage.workflow.model.volume.DeleteDiskInitFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class DeleteDiskInitFlowCommand  extends BaseWorkFlowCommand {
	@Autowired
	ISnapshotService snapshotService;
	
	@Override
	protected Object process() throws Exception {
		DeleteDiskInitFlowCommandReq req = (DeleteDiskInitFlowCommandReq)getReqParams();
		List<Snapshot> snaps = snapshotService.findSnapshotByVolume(req.getVolumeId());
		DeleteDiskInitFlowCommandRes res = new DeleteDiskInitFlowCommandRes();
		res.setSnapshotIds(snaps.stream().map(Snapshot::getId).collect(Collectors.toList()));
		log.debug(res.getSnapshotIds().toString());
		res.setTaskId(this.getTaskId());
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return DeleteDiskInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return DeleteDiskInitFlowCommandRes.class;
	}

}