package com.gcloud.controller.storage.handler.node.pool;

import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.storage.msg.node.pool.NodeCreateStoragePoolReplyMsg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class NodeCreateStoragePoolReplyHandler extends AsyncMessageHandler<NodeCreateStoragePoolReplyMsg> {

    @Autowired
    private StoragePoolDao poolDao;

    @Override
    public void handle(NodeCreateStoragePoolReplyMsg msg) {
        if (msg.getSuccess()) {
            log.info("创建存储池成功：{}", msg.getPoolId());
        }
        else {
            log.info("创建存储池失败：{} -> {}", msg.getPoolId(), msg.getErrorCode());
            poolDao.deleteById(msg.getPoolId());
        }
    }

}