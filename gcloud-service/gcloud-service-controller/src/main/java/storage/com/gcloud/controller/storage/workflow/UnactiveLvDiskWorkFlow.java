package com.gcloud.controller.storage.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.storage.workflow.model.volume.UnactiveLvDiskWorkFlowReq;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import com.gcloud.header.compute.enums.StorageType;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class UnactiveLvDiskWorkFlow extends BaseWorkFlows {
	@Autowired
    private IVolumeService volumeService;
	
	@Override
	public String getFlowTypeCode() {
		return "UnactiveLvDiskWorkFlow";
	}
	@Override
	public boolean judgeExecute() {
		UnactiveLvDiskWorkFlowReq req = (UnactiveLvDiskWorkFlowReq)getReqParams();
		Volume vol = volumeService.getVolume(req.getVolumeId());
		if(vol == null) {
			throw new GCloudException(":: 该磁盘不存在");
		}
		if(vol.getStorageType().equals(StorageType.CENTRAL.getValue())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object preProcess() {
		return null;
	}

	@Override
	public void process() {
		
	}

	@Override
	protected Class<?> getReqParamClass() {
		return UnactiveLvDiskWorkFlowReq.class;
	}

}