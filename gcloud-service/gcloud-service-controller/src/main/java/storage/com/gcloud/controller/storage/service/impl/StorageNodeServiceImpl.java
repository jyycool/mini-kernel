package com.gcloud.controller.storage.service.impl;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.storage.service.IStorageNodeService;
import com.gcloud.controller.storage.util.RedisNodesUtil;
import com.gcloud.controller.storage.util.StorageControllerUtil;
import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
@Slf4j
public class StorageNodeServiceImpl implements IStorageNodeService {
	@Autowired
    private GCloudRedisTemplate redisTemplate;
	
	@Override
	public void storageNodeConnect(String hostname, int nodeTimeout) {
		String aliveKey = StorageControllerUtil.getNodesAliveKey(hostname);
        String aliveNode = ObjectUtils.toString(redisTemplate.opsForValue().get(aliveKey));
        if (StringUtils.isBlank(aliveNode)) {
            log.debug("node has gone away or just register, node=" + hostname);
            //redis register node
            RedisNodesUtil.registerStorageNode(hostname, nodeTimeout);
            RedisNodesUtil.updateNodeAlive(hostname, nodeTimeout);
        } else {
        	RedisNodesUtil.updateNodeAlive(hostname, nodeTimeout);
        }
	}

}