package com.gcloud.controller.storage.driver;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gcloud.common.util.StringUtils;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.controller.compute.utils.VmControllerUtil;
import com.gcloud.controller.log.util.LongTaskUtil;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.model.CreateDiskResponse;
import com.gcloud.controller.storage.prop.StorageProp;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.storage.util.StorageControllerUtil;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.log.enums.LogType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.model.StoragePoolInfo;
import com.gcloud.service.common.Consts;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.util.RbdUtil;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Slf4j
public class CephStorageDriver implements IStorageDriver {

    @Autowired
    private VolumeDao volumeDao;
    
    @Autowired
    private SnapshotDao snapshotDao;

    @Autowired
    private IVolumeService volumeService;
    
    @Autowired
    private  StorageProp prop;

    @PostConstruct
    private void init() {

    }

    @Override
    public StorageType storageType() {
        return StorageType.DISTRIBUTED;
    }

    @Override
    public void createStoragePool(String poolId, String poolName, String hostname, String taskId) throws GCloudException {
        String[] cmd = new String[] {"ceph", "--user", prop.getCephOperator(), "osd", "pool", "create", poolName, "64", "64"};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_CREATE_POOL);
        //ceph osd pool application enable images rbd
        String[] enablecmd = new String[] {"ceph", "--user", prop.getCephOperator(), "osd", "pool", "application", "enable",poolName, "rbd"};
        this.exec(enablecmd, StorageErrorCodes.FAILED_TO_ENABLE_POOL_RBD);
    }

    @Override
    public void deleteStoragePool(String poolName) throws GCloudException {
        String[] cmd = new String[] {"ceph", "--user", prop.getCephOperator(), "osd", "pool", "rm", poolName, poolName, "--yes-i-really-really-mean-it"};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_DELETE_POOL);
    }

    @Override
    public CreateDiskResponse createVolume(String taskId, StoragePool pool, Volume volume) throws GCloudException {
        String[] cmd;
        if (volume.getImageRef() == null && StringUtils.isBlank(volume.getSnapshotId())) {
            cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "create", getVolumeName(volume.getProviderRefId()), "--pool", volume.getPoolName(), "--size", volume.getSize()*1024 + ""};
            this.exec(cmd, StorageErrorCodes.FAILED_TO_CREATE_VOLUME);
        } else if(StringUtils.isNotBlank(volume.getSnapshotId())) {
        	//基于快照创建�?
        	/*
        	 * 克隆�?
        	rbd --user gcloud clone volumes/volume-fa7e3750-fb58-4bca-a70c-714fd3fd113a@snap volumes/volume-dengyf
        	独立�?
        	rbd flatten volumes/volume-dengyf*/
        	Snapshot snap = snapshotDao.getById(volume.getSnapshotId());
        	//添加分布式锁start
        	String lockName = StorageControllerUtil.getSnapLock(snap.getVolumeId(), snap.getId());
            String lockid = "";
            
            lockid = LockUtil.spinLock(lockName, Consts.Time.STORAGE_REDIS_SNAP_LOCK_TIMEOUT, Consts.Time.STORAGE_REDIS_SNAP_LOCK_GET_LOCK_TIMEOUT, String.format("::获取快照锁失�?,volumeId:%s,snapshotId:%s", snap.getVolumeId(), snap.getId()));
            try {
	        	if(!RbdUtil.snapIsProtected(prop.getCephOperator(), snap.getPoolName(), getVolumeName(snap.getVolumeId()), snap.getId())) {
		        	String[] snapProtectCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "protect", snap.getPoolName() + "/" + getVolumeName(snap.getVolumeId()) + "@" + snap.getId()};
		        	this.exec(snapProtectCmd, "::保护镜像快照失败");
	        	}
            }catch (GCloudException ex) {
                throw ex;
            } finally {
                LockUtil.releaseSpinLock(lockName, lockid);
            }
        	//添加分布式锁end
        	cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "clone", snap.getPoolName() + "/" + getVolumeName(snap.getVolumeId()) + "@" + snap.getId(), volume.getPoolName() + "/" + getVolumeName(volume.getProviderRefId())};
        	this.exec(cmd, StorageErrorCodes.FAILED_TO_CREATE_VOLUME);
        	
        	String[] flattenCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "flatten", volume.getPoolName() + "/" + getVolumeName(volume.getProviderRefId())};
        	this.exec(flattenCmd, "::独立卷失�?");
        	
        }else {
        	//添加分布式锁start
        	String lockName = StorageControllerUtil.getSnapLock(volume.getImageRef(), "snap");
            String lockid = "";
            
            lockid = LockUtil.spinLock(lockName, Consts.Time.STORAGE_REDIS_SNAP_LOCK_TIMEOUT, Consts.Time.STORAGE_REDIS_SNAP_LOCK_GET_LOCK_TIMEOUT, String.format("::获取快照锁失�?,ImageRef:%s,snapshotId:%s", volume.getImageRef(), "snap"));
            try {
	        	String[] snapLsCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "ls", prop.getCephImagePool() + "/" + volume.getImageRef()};
	        	if(StringUtils.isBlank(SystemUtil.run(snapLsCmd))) {
		        	String[] snapCreateCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "create", prop.getCephImagePool() + "/" + volume.getImageRef() +"@snap"};
		        	this.exec(snapCreateCmd, "::创建镜像快照失败");
		        	if(!RbdUtil.snapIsProtected(prop.getCephOperator(), prop.getCephImagePool(), volume.getImageRef(), "snap")) {
			        	String[] snapProtectCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "protect", prop.getCephImagePool() + "/" + volume.getImageRef() +"@snap"};
			        	this.exec(snapProtectCmd, "::保护镜像快照失败");
		        	}
	        	}
            }catch (GCloudException ex) {
                throw ex;
            } finally {
                LockUtil.releaseSpinLock(lockName, lockid);
            }
        	//添加分布式锁end
        	
        	cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "clone", prop.getCephImagePool() + "/" + volume.getImageRef() + "@snap", volume.getPoolName() + "/" + getVolumeName(volume.getProviderRefId())};
        	this.exec(cmd, StorageErrorCodes.FAILED_TO_CREATE_VOLUME);
        }
        
        if (volume.getImageRef() != null) {
	        String[] snapCreateCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "resize", volume.getPoolName() + "/" + getVolumeName(volume.getProviderRefId()), "--size", volume.getSize()*1024 + ""};
	    	this.exec(snapCreateCmd, "::卷resize失败");
	    	
        }
        
        this.volumeDao.updateVolumeStatus(volume.getId(), VolumeStatus.AVAILABLE);

        CreateDiskResponse response = new CreateDiskResponse();
        response.setLogType(LogType.SYNC);
        response.setDiskId(volume.getId());
        return response;
    }

    @Override
    public void deleteVolume(String taskId, StoragePool pool, Volume volume) throws GCloudException {
        String[] cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "rm", "--no-progress", "--pool", volume.getPoolName(), "--image", getVolumeName(volume.getProviderRefId())};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_DELETE_VOLUME);
        this.volumeService.handleDeleteVolumeSuccess(volume.getId());
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, volume.getId());
    }

    @Override
    public void resizeVolume(String taskId, StoragePool pool, Volume volume, int newSize) throws GCloudException {
        String[] cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "resize", "--no-progress", "--pool", volume.getPoolName(), "--image", getVolumeName(volume.getProviderRefId()), "--size",
                newSize*1024 + ""};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_RESIZE_VOLUME);
        this.volumeService.handleResizeVolumeSuccess(volume.getId(), newSize);
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, volume.getId());
    }

    @Override
    public void createSnapshot(StoragePool pool, String volumeRefId, Snapshot snapshot, String taskId) throws GCloudException {
        String[] cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "create", "--pool", pool.getPoolName(), "--image", getVolumeName(volumeRefId), "--snap", snapshot.getProviderRefId()};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_CREATE_SNAP);
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, null);
    }

    @Override
    public void deleteSnapshot(StoragePool pool, String volumeRefId, Snapshot snapshot, String taskId) throws GCloudException {
    	//查看是否�?要去保护�?
    	if(RbdUtil.snapIsProtected(prop.getCephOperator(), snapshot.getPoolName(), getVolumeName(volumeRefId), snapshot.getProviderRefId())) {
    		String[] snapUnProtectCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "unprotect", snapshot.getPoolName() + "/" + getVolumeName(snapshot.getVolumeId()) + "@" + snapshot.getProviderRefId()};
        	this.exec(snapUnProtectCmd, "::去保护镜像快照失�?");
    	}
    	
        String[] cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "rm", "--no-progress", "--pool", snapshot.getPoolName(), "--image", getVolumeName(volumeRefId), "--snap",
                snapshot.getProviderRefId()};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_DELETE_SNAP);
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, snapshot.getId());
    }

    @Override
    public void resetSnapshot(StoragePool pool, String volumeRefId, Snapshot snapshot, Integer size, String taskId) throws GCloudException {
        String[] cmd = new String[] {"rbd", "--user", prop.getCephOperator(), "snap", "rollback", "--no-progress", "--pool", snapshot.getPoolName(), "--image", getVolumeName(volumeRefId), "--snap",
                snapshot.getProviderRefId()};
        this.exec(cmd, StorageErrorCodes.FAILED_TO_RESET_SNAP);
        
        if(size > snapshot.getVolumeSize()) {
        	String[] resizeVolumeCmd = new String[] {"rbd", "--user", prop.getCephOperator(), "resize", pool.getPoolName() + "/" + getVolumeName(volumeRefId), "--size", size*1024 + ""};
	    	this.exec(resizeVolumeCmd, "::卷resize失败");
        }
        
        LongTaskUtil.syncSucc(LogType.SYNC, taskId, snapshot.getId());
    }

    private void exec(String[] cmd, String errorCode) throws GCloudException {
        int res;
        try {
            res = SystemUtil.runAndGetCode(cmd);
        }
        catch (Exception e) {
            throw new GCloudException(errorCode);
        }
        if (res != 0) {
            throw new GCloudException(errorCode);
        }
    }
    
    private String getVolumeName(String refId) {
    	return "volume-" + refId;
    }

	@Override
	public StoragePoolInfo getStoragePool(StoragePool pool) throws GCloudException {
		String[] cmd1 = new String[] {"ceph", "--user", prop.getCephOperator(), "-f", "json", "df"};
		String result = SystemUtil.run(cmd1);
		////////////////////
		if(StringUtils.isBlank(result)){
            throw new GCloudException("::获取ceph信息失败");
        }
		StoragePoolInfo info = new StoragePoolInfo();
        try {
            JSONObject jsonObject = JSONObject.parseObject(result);
            JSONArray pools = jsonObject.getJSONArray("pools");
            for(int i=0;i<pools.size();i++){
            	JSONObject item = pools.getJSONObject(i); 
            	if(item.getString("name").equals(pool.getPoolName())) {
            		JSONObject itemDetail = item.getJSONObject("stats");
            		
            		info.setTotalSize(itemDetail.getLongValue("max_avail")/1024/1024);
            		info.setUsedSize(itemDetail.getLongValue("bytes_used")/1024/1024);
            		info.setAvailSize(info.getTotalSize() - info.getUsedSize());
            		break;
            	}
            }
            
        } catch (Exception ex) {
            log.error("解析ceph json 失败", ex);
            throw new GCloudException("::获取ceph info 信息失败");
        }
		////////////////////
		/*String[] records = result.split("\\n");
		for(String record:records) {
			if(record.contains(pool.getPoolName() + " ")) {
				result = record;
				break;
			}
		}
		
		String[] results =  result.trim().split("\\s+");
		StoragePoolInfo info = new StoragePoolInfo();
		info.setTotalSize(sizeConvertToMB(results[4]));
		info.setUsedSize(sizeConvertToMB(results[2]));
		info.setAvailSize(info.getTotalSize() - info.getUsedSize());*/
		return info;
	}
	
	private long sizeConvertToMB(String size) {
		long result = 0;//M
		String unit = size.substring(size.length() - 1);
		long sizeL = 0;
		char   c   =   unit.charAt(0);
		if(((c>='a' && c<='z')   ||   (c>='A' && c<='Z'))) {
			sizeL = Long.parseLong(size.substring(0, size.length() - 1));
		} else {
			sizeL = Long.parseLong(size);
			unit = "B";
		}
		
		switch(unit){
			case "T":
				result = sizeL * 1024 * 1024;
				break;
			case "G":
				result = sizeL * 1024;
				break;
			case "M":
				result = sizeL;
				break;
			case "K":
				result = sizeL/1024;
				break;
			case "B":
				result = sizeL/1024/1024;
				break;
			default:
		}
		return result;
	}

}