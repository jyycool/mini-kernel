package com.gcloud.controller.storage.entity;

import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name = "gc_storage_nodes", jdbc = "controllerJdbcTemplate")
public class StorageNode {
	@ID
    private Integer id;
    private String hostname;
    private Integer state;
    private Date createTime;
    
    public static final String ID = "id";
    public static final String HOSTNAME = "hostname";
    public static final String STATE = "state";
    public static final String CREATE_TIME = "createTime";

    public String updateId(Integer id) {
        this.setId(id);
        return ID;
    }

    public String updateState(Integer state) {
        this.setState(state);
        return STATE;
    }

    public String updateHostname(String hostname) {
        this.setHostname(hostname);
        return HOSTNAME;
    }
    
    public String updateCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return CREATE_TIME;
    }
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
}