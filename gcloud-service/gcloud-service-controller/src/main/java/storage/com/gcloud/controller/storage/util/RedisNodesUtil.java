package com.gcloud.controller.storage.util;


import java.text.MessageFormat;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gcloud.controller.storage.model.node.StorageNodeModel;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.service.common.Consts;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class RedisNodesUtil {
	public static void registerStorageNode(String hostname, int nodeTimeout) {
        String lockName = StorageControllerUtil.getNodesHostNameLock(hostname);
        String nodeKey = StorageControllerUtil.getNodesHostNameKey(hostname);
        String lockid = "";
        try {
            lockid = LockUtil.spinLock(lockName, Consts.Time.NODES_REDIS_LOCK_TIMEOUT, Consts.Time.NODES_REDIS_LOCK_GET_LOCK_TIMEOUT);
        } catch (Exception ex) {
            log.error("存储节点注册失败:获取锁失�?,hostName=" + hostname, ex);
            return;
        }

        try {
        	GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);
        	StorageNodeModel node = new StorageNodeModel();
        	node.setHostName(hostname);
            redisTemplate.setObject(nodeKey, node);
            log.debug(String.format("【存储节�?-情况】连接成功，node�?%s", hostname));
        } catch (Exception ex) {
            log.error("::registerStorageNode error", ex);
        } finally {
            LockUtil.releaseSpinLock(lockName, lockid);
        }

    }
	
	public static void updateNodeAlive(String hostName, int timeout) {

        try {

            GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);
            String aliveKey = StorageControllerUtil.getNodesAliveKey(hostName);
            redisTemplate.opsForValue().set(aliveKey, hostName, Duration.ofSeconds(timeout));

        } catch (Exception ex) {
            log.error("storage updateNodeAlive error", ex);
        }
    }
	
	/**
     * 移除解注册的计算节点
     */
    public static void cleanStorageNodes(List<String> hostNames) {

        GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);

        Set<String> hostSet = getAllHostName();
        if (hostSet == null) {
            return;
        }

        //剩下的就是失去连接，�?要移除的
        hostSet.removeAll(hostNames);

        for (String rm : hostSet) {

            String lockName = StorageControllerUtil.getNodesHostNameLock(rm);
            String nodeKey = StorageControllerUtil.getNodesHostNameKey(rm);
            String lockid = "";

            log.debug(String.format("获取 Lock �?�?:%s", lockName));
            try {
                lockid = LockUtil.spinLock(lockName, Consts.Time.NODES_REDIS_LOCK_TIMEOUT, Consts.Time.NODES_REDIS_LOCK_GET_LOCK_TIMEOUT);
            } catch (Exception ex) {
                log.error("清除存储节点失败:获取锁失�?,hostName=" + rm, ex);
                continue;
            }
            log.debug(String.format("获取 Lock结束:%s", lockName));
            try {
                redisTemplate.delete(nodeKey);
                log.error(String.format("【存储节�?-情况】失去连接，node�?%s", rm));
            } catch (Exception ex) {
                log.error("getStorageNodeByHostName error", ex);
            } finally {
                LockUtil.releaseSpinLock(lockName, lockid);
            }
        }

    }
    
    public static Set<String> getAllHostName() {

        Set<String> hostKeySet = null;
        Set<String> hostNameSet = null;

        try {
            String key = MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODES_STORAGE_NODE_HOSTNAME, "*");
            GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);
            hostKeySet = redisTemplate.keys(key);

        } catch (Exception ex) {
            log.error("storage getAllNodes error", ex);
        }

        if (hostKeySet != null && hostKeySet.size() > 0) {
            hostNameSet = new HashSet<String>();
            for (String hostKey : hostKeySet) {
                String hostname = StorageControllerUtil.getHostNameByHostKey(hostKey);
                hostNameSet.add(hostname);
            }
        }
        return hostNameSet;
    }
    
    public static Map<String, StorageNodeModel> getStorageNodes() {

        Map<String, StorageNodeModel> nodeMap = new HashMap<String, StorageNodeModel>();
        GCloudRedisTemplate redisTemplate = SpringUtil.getBean(GCloudRedisTemplate.class);

        Set<String> hostSet = getAllHostName();
        if (hostSet != null && hostSet.size() > 0) {

            for (String hostname : hostSet) {
                try {
                    String nodeKey = StorageControllerUtil.getNodesHostNameKey(hostname);
                    StorageNodeModel n = (StorageNodeModel)redisTemplate.getObject(nodeKey);
                    if (n != null) {
                        nodeMap.put(hostname, n);
                    }
                } catch (Exception ex) {
                    log.error("get storage node error", ex);
                }
            }
        }

        return nodeMap;
    }
}