package com.gcloud.controller.storage.service;

import java.util.List;

import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.model.DescribeSnapshotsParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.storage.model.SnapshotType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface ISnapshotService {

    String createSnapshot(String diskId, String name, String description, CurrentUser currentUser, String taskId, String snapshotId) throws GCloudException;

    void handleCreateSnapshotSuccess(String snapshotId);

    void handleCreateSnapshotFailed(String errorCode, String snapshotId);

    void updateSnapshot(String id, String name, String description) throws GCloudException;

    void deleteSnapshot(String id, String taskId) throws GCloudException;

    void handleDeleteSnapshotSuccess(String snapshotId);

    void handleDeleteSnapshotFailed(String errorCode, String snapshotId);

    void resetSnapshot(String snapshotId, String diskId, String taskId) throws GCloudException;

    void handleResetSnapshotSuccess(String snapshotId, List<String> snapshotsToDelete);

    void handleResetSnapshotFailed(String errorCode, String snapshotId);

    PageResult<SnapshotType> describeSnapshots(DescribeSnapshotsParams params, CurrentUser currentUser);
    
    List<Snapshot> findSnapshotByVolume(String volumeId);

}