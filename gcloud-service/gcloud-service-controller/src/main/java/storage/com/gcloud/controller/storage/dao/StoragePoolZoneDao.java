package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.StoragePoolZone;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;

import lombok.val;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class StoragePoolZoneDao extends JdbcBaseDaoImpl<StoragePoolZone, Integer> {
	
	public void deleteByPoolIdAndZoneId(String poolId, List<String> zoneIds) {
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete from gc_storage_pool_zones where storage_pool_id = ? and zone_id in (")
			.append(SqlUtil.inPreStr(zoneIds.size())).append(")");
		values.add(poolId);
		values.addAll(zoneIds);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
	
	public void updateZoneByPoolHostname(String hostname, String zoneId) {
		if(StringUtils.isBlank(hostname)) {
			return;
		}
		
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("update gc_storage_pool_zones set zone_id = ? where storage_pool_id in ");
		sql.append(" (");
		sql.append(" select id from gc_storage_pools where hostname = ?");
		sql.append(" )");
		
		values.add(zoneId);
		values.add(hostname);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
	
	public void deleteByHostname(String hostname) {
		if(StringUtils.isBlank(hostname)) {
			return;
		}
		
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete from gc_storage_pool_zones where storage_pool_id in ");
		sql.append(" (");
		sql.append(" select id from gc_storage_pools where hostname = ?");
		sql.append(" )");
		
		values.add(hostname);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
	
	public void deleteByPoolId(String poolId) {
		if(StringUtils.isBlank(poolId)) {
			return;
		}
		
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete from gc_storage_pool_zones where storage_pool_id = ?");
		values.add(poolId);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
}