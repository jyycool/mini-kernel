package com.gcloud.controller.storage.handler.api.volume;

import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.VolumeTypeListResponse;
import com.gcloud.header.storage.msg.api.volume.ApiVolumeTypeListMsg;
import com.gcloud.header.storage.msg.api.volume.ApiVolumeTypeListReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="VolumeTypeList", name = "磁盘类型列表(无实�?)")
public class ApiVolumeTypeListHandler extends MessageHandler<ApiVolumeTypeListMsg, ApiVolumeTypeListReplyMsg> {

    @Autowired
    private IVolumeService volumeService;

    @Override
    public ApiVolumeTypeListReplyMsg handle(ApiVolumeTypeListMsg msg) throws GCloudException {

        List<VolumeTypeListResponse> list = new ArrayList<>();
        ApiVolumeTypeListReplyMsg reply = new ApiVolumeTypeListReplyMsg();
        reply.setVolumeTypes(list);
        return reply;
    }
}