package com.gcloud.controller.storage.timer;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.dao.StorageNodeConnectLogDao;
import com.gcloud.controller.storage.dao.StorageNodeDao;
import com.gcloud.controller.storage.entity.StorageNode;
import com.gcloud.controller.storage.entity.StorageNodeConnectLog;
import com.gcloud.controller.storage.model.node.StorageNodeModel;
import com.gcloud.controller.storage.util.RedisNodesUtil;
import com.gcloud.controller.storage.util.StorageControllerUtil;
import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.enums.NodeConnectType;
import com.gcloud.header.storage.enums.StorageNodeState;
import com.gcloud.service.common.Consts;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@QuartzTimer(fixedDelay = 10 * 1000L)
@Slf4j
public class CheckConnectStorageNodeTimer extends GcloudJobBean{
	@Autowired
    private GCloudRedisTemplate redisTemplate;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.debug(String.format("【存储控制器节点�?测�?�开�?"));
        long startTime = System.currentTimeMillis();
        try {
            StorageNodeDao storageNodeDao = SpringUtil.getBean(StorageNodeDao.class);

            String searchKey = MessageFormat.format(Consts.RedisKey.GCLOUD_CONTROLLER_STORAGE_NODE_ALIVE, "*");
            Set<String> aliveKeySet = redisTemplate.keys(searchKey);

            // 从key获取hostName，不用再查redis
            if (aliveKeySet == null) {
                aliveKeySet = new HashSet<String>();
            }
            List<String> onLineNodes = new ArrayList<String>();
            for (String aliveKey : aliveKeySet) {
                String aliveNode = StorageControllerUtil.getHostNameByAliveKey(aliveKey);
                onLineNodes.add(aliveNode);
            }


            RedisNodesUtil.cleanStorageNodes(onLineNodes);


            Map<String, StorageNodeModel> nodesInCache = RedisNodesUtil.getStorageNodes();
            List<StorageNode> nodesInDB = storageNodeDao.findAll();

            syncStorageNode(nodesInCache, nodesInDB);

        } catch (Exception e) {
            log.error("监听存储节点汇报出错:", e);
        }

        log.debug(String.format("【存储控制器节点�?测�?�结�?: 用时(ms)�?%s�?", (System.currentTimeMillis() - startTime)));
		
	}
	/**
    *
    * @Description: 同步数据库节点和内存节点
    * @param nodesInCache
    *            内存节点列表
    * @param nodesInDB
    *            数据库节点列�?
    */
   private void syncStorageNode(Map<String, StorageNodeModel> nodesInCache, List<StorageNode> nodesInDB) {
       Hashtable<String, StorageNode> nodeMapsInDB = new Hashtable<String, StorageNode>();
       if (nodesInDB != null && nodesInDB.size() > 0) {
           for (StorageNode nodeDB : nodesInDB) {
               nodeMapsInDB.put(nodeDB.getHostname(), nodeDB);
           }
       }

       StorageNodeDao storageNodeDao = SpringUtil.getBean(StorageNodeDao.class);
       StorageNodeConnectLogDao connectLogDao = SpringUtil.getBean( StorageNodeConnectLogDao.class);

       // 内存列表有，数据库没有的，需要新增和修改为连接状�?
       for (Map.Entry<String, StorageNodeModel> map : nodesInCache.entrySet()) {
           String hostName = map.getKey();
           StorageNodeModel node = map.getValue();
           if (node != null) {
               StorageNode nodeDB = nodeMapsInDB.get(hostName);
               if (nodeDB == null) {
            	   StorageNode newStorageNode = new StorageNode();
            	   newStorageNode.setHostname(hostName);
            	   newStorageNode.setState(StorageNodeState.CONNECTED.getValue());
            	   newStorageNode.setCreateTime(new Date());
                   storageNodeDao.save(newStorageNode);

                   StorageNodeConnectLog connect = new StorageNodeConnectLog();
                   connect.setHostname(hostName);
                   connect.setLogTime(new Date());
                   connect.setConnectType(NodeConnectType.REGISTER.getValue());

                   connectLogDao.save(connect);

               } else {
            	   // 标识内存有，数据库也有但是数据库的状态为0
                   updateDbByCache(node, nodeDB);
               }
           }
       }

       // 数据库有，内存没有的，需要修改为失去连接状�??
       for (StorageNode nodeInDB : nodesInDB) {
           if (nodesInCache.get(nodeInDB.getHostname()) == null) {
               if (StorageNodeState.CONNECTED.getValue() == nodeInDB.getState()) {
                   List<String> ids = new ArrayList<String>();
                   ids.add(nodeInDB.getHostname());
                   storageNodeDao.updateBatch("hostname", ids, "state", StorageNodeState.DISCONNECTED.getValue());

                   StorageNodeConnectLog connect = new StorageNodeConnectLog();
                   connect.setHostname(nodeInDB.getHostname());
                   connect.setLogTime(new Date());
                   connect.setConnectType(NodeConnectType.DISCONNECT.getValue());

                   connectLogDao.save(connect);

               }
           }
       }
   }

   private void updateDbByCache(StorageNodeModel node, StorageNode nodeDB) {
	   StorageNodeDao storageNodeDao = SpringUtil.getBean(StorageNodeDao.class);
	   StorageNodeConnectLogDao connectLogDao = SpringUtil.getBean(StorageNodeConnectLogDao.class);

       List<String> fields = new ArrayList<String>();

       if (StorageNodeState.DISCONNECTED.getValue() == nodeDB.getState()) {
           fields.add("state");
           nodeDB.setState(StorageNodeState.CONNECTED.getValue());
           // 把重新连接写上数据库

           StorageNodeConnectLog connect = new StorageNodeConnectLog();
           connect.setHostname(node.getHostName());
           connect.setLogTime(new Date());
           connect.setConnectType(NodeConnectType.CONNECT.getValue());

           connectLogDao.save(connect);
       }

       if (fields.size() > 0) {
           for (String field : fields) {
               log.debug(String.format("【修改存储节点属性�?�节点：%s, 修改项：%s", node.getHostName(), field));
           }

           storageNodeDao.update(nodeDB, fields);
       }
   }
}