package com.gcloud.controller.storage.model;

import com.gcloud.common.model.PageParams;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeDiskCategoriesParams extends PageParams{
	private String zoneId;
	private String diskCategoryName;
	private String storageType;
	private String hostname;
	private String availableForHost;
	private String availableForHostZone;

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getDiskCategoryName() {
		return diskCategoryName;
	}

	public void setDiskCategoryName(String diskCategoryName) {
		this.diskCategoryName = diskCategoryName;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getAvailableForHost() {
		return availableForHost;
	}

	public void setAvailableForHost(String availableForHost) {
		this.availableForHost = availableForHost;
	}

	public String getAvailableForHostZone() {
		return availableForHostZone;
	}

	public void setAvailableForHostZone(String availableForHostZone) {
		this.availableForHostZone = availableForHostZone;
	}
}