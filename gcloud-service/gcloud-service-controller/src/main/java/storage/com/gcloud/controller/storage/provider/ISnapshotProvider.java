package com.gcloud.controller.storage.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface ISnapshotProvider extends IResourceProvider {

    void createSnapshot(StoragePool pool, Volume volume, String snapshotId, String name, String description, CurrentUser currentUser, String taskId);

    void updateSnapshot(String snapshotRefId, String name, String description);

    void deleteSnapshot(StoragePool pool, Volume volume, Snapshot snapshot, String taskId);

    void resetSnapshot(StoragePool pool, Volume volume, Snapshot snapshot, String diskId, String taskId) throws GCloudException;

}