package com.gcloud.controller.storage.handler.api.pool;

import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.StorageTypeVo;
import com.gcloud.header.storage.msg.api.pool.ApiDiskCategoryStorageTypeMsg;
import com.gcloud.header.storage.msg.api.pool.ApiDiskCategoryStorageTypeReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.DISK_CATEGORY_STORAGE_TYPE, name = "磁盘类型的存储类型列�?")
public class ApiDiskCategoryStorageTypeHandler extends MessageHandler<ApiDiskCategoryStorageTypeMsg, ApiDiskCategoryStorageTypeReplyMsg> {

    @Autowired
    private IStoragePoolService poolService;

    @Override
    public ApiDiskCategoryStorageTypeReplyMsg handle(ApiDiskCategoryStorageTypeMsg msg) throws GCloudException {
        List<StorageTypeVo> storageTypeVos = poolService.diskCategoryStorageType();
        ApiDiskCategoryStorageTypeReplyMsg reply = new ApiDiskCategoryStorageTypeReplyMsg();
        reply.init(storageTypeVos);
        return reply;
    }
}