package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.DetailDiskCategoryParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.DiskCategoryModel;
import com.gcloud.header.storage.msg.api.pool.ApiDetailDiskCategoryMsg;
import com.gcloud.header.storage.msg.api.pool.ApiDetailDiskCategoryReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.DETAIL_DISK_CATEGORY, name = "磁盘类型详情")
public class ApiDetailDiskCategoryHandler extends MessageHandler<ApiDetailDiskCategoryMsg, ApiDetailDiskCategoryReplyMsg>{

	@Autowired
	private IStoragePoolService poolService;
	
	@Override
	public ApiDetailDiskCategoryReplyMsg handle(ApiDetailDiskCategoryMsg msg) throws GCloudException {
		DetailDiskCategoryParams params = BeanUtil.copyProperties(msg, DetailDiskCategoryParams.class);
		DiskCategoryModel response = poolService.detailDiskCategory(params, msg.getCurrentUser());
		
		ApiDetailDiskCategoryReplyMsg reply = new ApiDetailDiskCategoryReplyMsg();
		reply.setDiskCategory(response);
		return reply;
	}

}