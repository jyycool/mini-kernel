package com.gcloud.controller.storage.entity;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public class VolumeExInfo extends Volume{
	private String attachmentId;
	private String mountPoint;
    private String instanceUuid;
    private String attachedHost;
    private String attachProvider;
    private String attachProviderRefId;
	
	public String getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getMountPoint() {
		return mountPoint;
	}
	public void setMountPoint(String mountPoint) {
		this.mountPoint = mountPoint;
	}
	public String getInstanceUuid() {
		return instanceUuid;
	}
	public void setInstanceUuid(String instanceUuid) {
		this.instanceUuid = instanceUuid;
	}
	public String getAttachedHost() {
		return attachedHost;
	}
	public void setAttachedHost(String attachedHost) {
		this.attachedHost = attachedHost;
	}
	public String getAttachProvider() {
		return attachProvider;
	}
	public void setAttachProvider(String attachProvider) {
		this.attachProvider = attachProvider;
	}
	public String getAttachProviderRefId() {
		return attachProviderRefId;
	}
	public void setAttachProviderRefId(String attachProviderRefId) {
		this.attachProviderRefId = attachProviderRefId;
	}
}