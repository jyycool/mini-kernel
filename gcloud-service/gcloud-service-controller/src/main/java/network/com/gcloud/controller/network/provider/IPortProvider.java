package com.gcloud.controller.network.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.model.CreatePortParams;
import com.gcloud.header.api.model.CurrentUser;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IPortProvider extends IResourceProvider {

    String createPort(CreatePortParams params, CurrentUser currentUser);

    void deletePort(Port port);

    void updatePort(com.gcloud.controller.network.entity.Port port, List<String> securityGroupIds, String portName);

    void attachPort(VmInstance instance, Port port, String customOvsBr, Boolean noArpLimit);

    void updatePort(Port port);

    void deleteQosPolicy(String policyId);

    List<Port> list(Map<String, String> filter);

    void detachDone(Port port);

    Port createPortData(String neutronPortId, String deviceId, CurrentUser currentUser);

    Port createPortData(String neutronPortId, String deviceId, String userId, String tenantId);

    void migratePort(String portRefId, String sourceHostName, String targetHostName);

    void migratePortIgnoreBinding(String portRefId, String sourceHostName, String targetHostName);


}