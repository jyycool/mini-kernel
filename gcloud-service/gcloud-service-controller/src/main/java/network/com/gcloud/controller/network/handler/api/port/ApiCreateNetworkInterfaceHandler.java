package com.gcloud.controller.network.handler.api.port;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.network.model.CreatePortParams;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateNetworkInterfaceMsg;
import com.gcloud.header.network.msg.api.CreateNetworkInterfaceReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

//import com.gcloud.controller.enums.ResourceIsolationCheckType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="新增网卡")
@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="CreateNetworkInterface")

@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SECURITYGROUP, resourceIdField = "securityGroupId")
public class ApiCreateNetworkInterfaceHandler extends MessageHandler<CreateNetworkInterfaceMsg, CreateNetworkInterfaceReplyMsg> {
	@Autowired
	private IPortService portService;
	
	@Autowired
	private ISubnetService subnetService;

	@Override
	public CreateNetworkInterfaceReplyMsg handle(CreateNetworkInterfaceMsg msg) throws GCloudException {
		//因为有可能是外网子网创建网卡，所以需要独立隔离校�?
		subnetService.subnetIsolationCheck(msg.getvSwitchId(), msg.getCurrentUser());
		
		CreateNetworkInterfaceReplyMsg reply = new CreateNetworkInterfaceReplyMsg();
        CreatePortParams params = new CreatePortParams();
        params.setSubnetId(msg.getvSwitchId());
        params.setName(msg.getNetworkInterfaceName());
        params.setIpAddress(msg.getPrimaryIpAddress());
        params.setSecurityGroupId(msg.getSecurityGroupId());
        params.setDescription(msg.getDescription());
        params.setDeviceId(msg.getDeviceId());
        params.setDeviceOwner(msg.getDeviceOwner());
		params.setBindingHost(msg.getBindingHost());
		params.setPortSecurityEnabled(msg.getPortSecurityEnabled());
		reply.setNetworkInterfaceId(portService.create(params, msg.getCurrentUser()));
		
		msg.setObjectId(reply.getNetworkInterfaceId());
		msg.setObjectName(msg.getNetworkInterfaceName());
		return reply;
	}

}