package com.gcloud.controller.network.handler.api.router;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IRouterService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.CleanVRouterGatewayMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@GcLog(taskExpect="清除路由网关成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.VROUTER,action="CleanVRouterGateway",name="清除路由网关")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vRouterId")
public class ApiCleanVRouterGatewayHandler extends MessageHandler<CleanVRouterGatewayMsg, ApiReplyMessage> {
	@Autowired
	IRouterService vRouterService;
	
	@Override
	public ApiReplyMessage handle(CleanVRouterGatewayMsg msg) throws GCloudException {
		vRouterService.cleanVRouterGateway(msg.getvRouterId());
		
		msg.setObjectId(msg.getvRouterId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.ROUTER_NAME, msg.getvRouterId()));
		return new ApiReplyMessage();
	}
	
}