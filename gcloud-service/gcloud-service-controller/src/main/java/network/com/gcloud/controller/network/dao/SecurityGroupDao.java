package com.gcloud.controller.network.dao;

import com.gcloud.common.model.PageParams;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.SecurityGroup;
import com.gcloud.controller.network.model.DescribeSecurityGroupsParams;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationType;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
public class SecurityGroupDao extends JdbcBaseDaoImpl<SecurityGroup, String> {
	
	public <E> PageResult<E> describeSecurityGroups(DescribeSecurityGroupsParams params, Class<E> clazz, CurrentUser currentUser){
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "s.", ResourceIsolationCheckType.SECURITYGROUP);
		List<Object> listParams = new ArrayList<Object>();
		
		StringBuffer sql = new StringBuffer();

        sql.append("select s.id as securityGroupId,s.name as securityGroupName,s.description, s.create_time as createTime, s.user_id as userId from gc_security_groups s where 1 = 1 ");
        if(StringUtils.isNotBlank(params.getTenantId())) {
        	sql.append(" and s.tenant_id=?");
        	listParams.add(params.getTenantId());
        }
        
        //添加关键字搜�?
        if(StringUtils.isNotBlank(params.getKey())) {
        	sql.append(" and s.name like concat('%', ?, '%')");
        	listParams.add(params.getKey());
        }
        
        sql.append(sqlModel.getWhereSql());
        listParams.addAll(sqlModel.getParams());
        sql.append(" order by s.create_time desc");

        return findBySql(sql.toString(), listParams, params.getPageNumber(), params.getPageSize(), clazz);
	}
}