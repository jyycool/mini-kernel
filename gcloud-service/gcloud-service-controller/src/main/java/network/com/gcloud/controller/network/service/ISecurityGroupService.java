package com.gcloud.controller.network.service;

import com.gcloud.common.model.PageParams;
import com.gcloud.controller.network.entity.SecurityGroup;
import com.gcloud.controller.network.model.AuthorizeSecurityGroupParams;
import com.gcloud.controller.network.model.CreateSecurityGroupParams;
import com.gcloud.controller.network.model.DescribeSecurityGroupAttributeResponse;
import com.gcloud.controller.network.model.DescribeSecurityGroupsParams;
import com.gcloud.controller.network.model.ModifySecurityGroupAttributeParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.network.model.DetailSecurityGroupResponse;
import com.gcloud.header.network.model.SecurityGroupItemType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface ISecurityGroupService {
	String createSecurityGroup(CreateSecurityGroupParams params, CurrentUser currentUser);
    void deleteSecurityGroup(String id, Boolean force);
    void modifySecurityGroupAttribute(ModifySecurityGroupAttributeParams params);
    PageResult<SecurityGroupItemType> describeSecurityGroups(DescribeSecurityGroupsParams params, CurrentUser currentUser);
    void authorizeSecurityGroup(AuthorizeSecurityGroupParams params, CurrentUser currentUser);
    void revokeSecurityGroup(String securityGroupRuleId);
    DescribeSecurityGroupAttributeResponse describeSecurityGroupAttribute(String securityGroupId, String direction, String etherType, String regionId);

    void cleanSecurityGroupData(String id);
    SecurityGroup createSecurityGroupData(CreateSecurityGroupParams params, ProviderType provider, String refSecurityGroupId, CurrentUser currentUser);
    void modifySecurityGroupData(ModifySecurityGroupAttributeParams params);
    DetailSecurityGroupResponse detail(String securityGroupId);
}