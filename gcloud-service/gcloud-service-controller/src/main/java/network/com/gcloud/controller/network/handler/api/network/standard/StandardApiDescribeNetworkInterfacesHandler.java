package com.gcloud.controller.network.handler.api.network.standard;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.model.DescribeNetworkInterfacesParams;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.network.enums.standard.StandardNetworkInterfaceStatus;
import com.gcloud.header.network.model.NetworkInterfaceSet;
import com.gcloud.header.network.model.standard.StandardNetworkInterfaceSet;
import com.gcloud.header.network.msg.api.standard.StandardDescribeNetworkInterfacesMsg;
import com.gcloud.header.network.msg.api.standard.StandardDescribeNetworkInterfacesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="DescribeNetworkInterfaces", versions = {ApiVersion.Standard}, name = "网卡列表")
public class StandardApiDescribeNetworkInterfacesHandler extends MessageHandler<StandardDescribeNetworkInterfacesMsg, StandardDescribeNetworkInterfacesReplyMsg> {

	@Autowired
	private IPortService portService;

	@Override
	public StandardDescribeNetworkInterfacesReplyMsg handle(StandardDescribeNetworkInterfacesMsg msg) throws GCloudException {

		DescribeNetworkInterfacesParams params = toParams(msg);
		PageResult<NetworkInterfaceSet> response = portService.describe(params, msg.getCurrentUser());
		PageResult<StandardNetworkInterfaceSet> standardResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));

		StandardDescribeNetworkInterfacesReplyMsg replyMsg = new StandardDescribeNetworkInterfacesReplyMsg();
		replyMsg.init(standardResponse);
		return replyMsg;
	}

	public List<StandardNetworkInterfaceSet> toStandardReply(List<NetworkInterfaceSet> datas){
		if(datas == null){
			return null;
		}

		List<StandardNetworkInterfaceSet> standardDatas = new ArrayList<>();
		for(NetworkInterfaceSet data : datas){
			StandardNetworkInterfaceSet standardData = BeanUtil.copyProperties(data, StandardNetworkInterfaceSet.class);
			standardData.setInstanceId(data.getDeviceId());
			standardData.setInstanceType(data.getDeviceType());

			//转换状�??
			if(StringUtils.isNotBlank(data.getDeviceId())) {
				standardData.setStatus(StandardNetworkInterfaceStatus.IN_USE.getValue());
			} else {
				standardData.setStatus(StandardNetworkInterfaceStatus.standardStatus(data.getStatus()));
			}
			standardDatas.add(standardData);
		}

		return standardDatas;

	}

	public DescribeNetworkInterfacesParams toParams(StandardDescribeNetworkInterfacesMsg msg){
		DescribeNetworkInterfacesParams params = BeanUtil.copyProperties(msg, DescribeNetworkInterfacesParams.class);
		params.setDeviceOwners(DeviceOwner.eniDeviceOwnerValues());
		params.setIncludeOwnerless(true);

		return params;
	}
}