package com.gcloud.controller.network.handler.api.subnet;

import com.gcloud.controller.network.service.ISwitchService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateVSwitchMsg;
import com.gcloud.header.network.msg.api.CreateVSwitchReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="创建交换机成�?")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="CreateVSwitch",name="创建交换�?")
public class ApiCreateVSwitchHandler extends MessageHandler<CreateVSwitchMsg, CreateVSwitchReplyMsg> {
	
	@Autowired
	private ISwitchService switchService;
	
	@Override
	public CreateVSwitchReplyMsg handle(CreateVSwitchMsg msg) throws GCloudException {
		CreateVSwitchReplyMsg reply = new CreateVSwitchReplyMsg();
		reply.setvSwitchId(switchService.createVSwitch(msg, msg.getCurrentUser()));
		msg.setObjectId(reply.getvSwitchId());
		msg.setObjectName(msg.getvSwitchName());
		return reply;
	}

}