package com.gcloud.controller.network.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum NetworkType {

	EXTERNAL("外部网络", 1),
	INTERNAL("内部网络", 0);
	
	private String name;
	private int value;
	
	NetworkType(String name, int value) {
		this.name = name;
		this.value = value;
	}
	
	public static NetworkType getNetworkTypeByValue(int value) {
		return Arrays.stream(NetworkType.values()).filter(t -> t.getValue() == value).findFirst().orElse(null);
	}
	
	public static NetworkType getNetworkTypeByEnName(String enName) {
		return Arrays.stream(NetworkType.values()).filter(t -> t.name().equalsIgnoreCase(enName)).findFirst().orElse(null);
	}
	
	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}
	
	
}