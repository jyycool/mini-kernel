package com.gcloud.controller.network.handler.api.router;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DetailVRouterParams;
import com.gcloud.controller.network.service.IRouterService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailVRouter;
import com.gcloud.header.network.msg.api.ApiDetailVRouterMsg;
import com.gcloud.header.network.msg.api.ApiDetailVRouterReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.VROUTER,action="DetailVRouter",name="路由器详�?")
public class ApiDetailVRouterHandler extends MessageHandler<ApiDetailVRouterMsg, ApiDetailVRouterReplyMsg>{

	@Autowired
	IRouterService vRouterService;
	
	@Override
	public ApiDetailVRouterReplyMsg handle(ApiDetailVRouterMsg msg) throws GCloudException {
		DetailVRouterParams params = BeanUtil.copyProperties(msg, DetailVRouterParams.class);
		DetailVRouter response = vRouterService.detailVRouter(params, msg.getCurrentUser());
		
		ApiDetailVRouterReplyMsg reply = new ApiDetailVRouterReplyMsg();
		reply.setDetailVRouter(response);
		return reply;
	}

}