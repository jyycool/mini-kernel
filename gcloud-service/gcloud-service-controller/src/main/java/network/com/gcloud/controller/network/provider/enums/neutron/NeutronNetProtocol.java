package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.NetProtocol;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum NeutronNetProtocol {
    TCP("TCP", NetProtocol.TCP),
    UDP("UDP", NetProtocol.UDP),
    ICMP("ICMP", NetProtocol.ICMP);

    NeutronNetProtocol(String neutronNetProtocol, NetProtocol netProtocol) {
        this.neutronNetProtocol = neutronNetProtocol;
        this.netProtocol = netProtocol;
    }

    private String neutronNetProtocol;
    private NetProtocol netProtocol;

    public String getNeutronNetProtocol() {
        return neutronNetProtocol;
    }

    public NetProtocol getNetProtocol() {
        return netProtocol;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronNetProtocol protocol = Arrays.stream(NeutronNetProtocol.values()).filter(o -> o.getNeutronNetProtocol().equals(neutronValue)).findFirst().orElse(null);
        return protocol == null ? null : protocol.getNetProtocol().value();
    }

    public static String getByGcValue(String gcValue){
        NeutronNetProtocol protocol = Arrays.stream(NeutronNetProtocol.values()).filter(o -> o.getNetProtocol().value().equals(gcValue)).findFirst().orElse(null);
        return protocol == null ? null : protocol.getNeutronNetProtocol();
    }
}