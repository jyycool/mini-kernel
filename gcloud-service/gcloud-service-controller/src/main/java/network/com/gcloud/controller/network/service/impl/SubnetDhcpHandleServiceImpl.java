package com.gcloud.controller.network.service.impl;

import com.gcloud.controller.network.dao.SubnetDhcpHandleDao;
import com.gcloud.controller.network.entity.SubnetDhcpHandle;
import com.gcloud.controller.network.service.ISubnetDhcpHandleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
public class SubnetDhcpHandleServiceImpl implements ISubnetDhcpHandleService {

    @Autowired
    private SubnetDhcpHandleDao dhcpHandleDao;

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void createOrUpdateHandle(String subnetId, String handleId) {
        SubnetDhcpHandle handle = dhcpHandleDao.getById(subnetId);
        if(handle == null){
            handle = new SubnetDhcpHandle();
            handle.setSubnetId(subnetId);
            handle.setHandleId(handleId);
            handle.setUpdateTime(new Date());
            handle.setRetry(0);
            dhcpHandleDao.save(handle);

            //已经删除，不接受其他操作
        }else if(handle.getDeleteSubnet() == null || !handle.getDeleteSubnet()){

            List<String> updateField = new ArrayList<>();
            updateField.add(handle.updateHandleId(handleId));
            updateField.add(handle.updateUpdateTime(new Date()));
            dhcpHandleDao.update(handle, updateField);
        }

    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void deleteHandle(String subnetId, String handleId) {
        SubnetDhcpHandle handle = new SubnetDhcpHandle();
        handle.setSubnetId(subnetId);
        handle.setHandleId(handleId);
        handle.setUpdateTime(new Date());
        handle.setRetry(0);
        dhcpHandleDao.saveOrUpdate(handle);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void rollbackHandle(String subnetId, String handleId) {
        SubnetDhcpHandle handle = dhcpHandleDao.getById(subnetId);
        if(handle == null){
            handle = new SubnetDhcpHandle();
            handle.setSubnetId(subnetId);
            handle.setHandleId(handleId);
            handle.setDeleteSubnet(null);
            handle.setUpdateTime(new Date());
            handle.setRetry(0);
            dhcpHandleDao.save(handle);

        }else if(handle.getDeleteSubnet() != null && handle.getDeleteSubnet()){

            List<String> updateField = new ArrayList<>();
            updateField.add(handle.updateHandleId(handleId));
            updateField.add(handle.updateDeleteSubnet(null));
            updateField.add(handle.updateUpdateTime(new Date()));
            dhcpHandleDao.update(handle, updateField);
        }
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void finishHandle(String subnetId, String handleId) {

        SubnetDhcpHandle handle = dhcpHandleDao.getById(subnetId);
        if(handle != null && handle.getHandleId() != null && handle.getHandleId().equals(handleId)){
            List<String> updateField = new ArrayList<>();
            updateField.add(handle.updateHandleId(null));
            updateField.add(handle.updateUpdateTime(new Date()));
            updateField.add(handle.updateRetry(0));
            dhcpHandleDao.update(handle, updateField);
        }

    }
}