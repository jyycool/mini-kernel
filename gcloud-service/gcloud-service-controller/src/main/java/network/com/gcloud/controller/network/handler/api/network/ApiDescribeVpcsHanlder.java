package com.gcloud.controller.network.handler.api.network;

import com.gcloud.controller.ResourceIsolationCheck;

//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.VpcsItemType;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.DescribeVpcsReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="DescribeVpcs", name = "虚拟私有云列�?")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcIds")
public class ApiDescribeVpcsHanlder extends MessageHandler<DescribeVpcsMsg, DescribeVpcsReplyMsg>{
	
	@Autowired
	IVpcService service;
	
	@Override
	public DescribeVpcsReplyMsg handle(DescribeVpcsMsg msg) throws GCloudException {
		// TODO Auto-generated method stub

		PageResult<VpcsItemType> response = service.describeVpcs(msg);
		DescribeVpcsReplyMsg reply = new DescribeVpcsReplyMsg();
        reply.init(response);
        
		return reply;
	}

}