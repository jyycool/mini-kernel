package com.gcloud.controller.network.handler.api.network.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyVpcAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyVpcAttributeReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="ModifyVpcAttribute", versions = {ApiVersion.Standard}, name = "修改虚拟私有云属�?")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcId")
public class StandardApiModifyVpcAttributeHandler extends MessageHandler<StandardApiModifyVpcAttributeMsg, StandardApiModifyVpcAttributeReplyMsg>{

	@Autowired
	IVpcService service;
	
	@Override
	public StandardApiModifyVpcAttributeReplyMsg handle(StandardApiModifyVpcAttributeMsg msg) throws GCloudException {
		service.updateVpc(toParams(msg));
		msg.setObjectId(msg.getVpcId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.ROUTER_NAME, msg.getVpcId()));
		return new StandardApiModifyVpcAttributeReplyMsg();
	}
	
	public ModifyVpcAttributeMsg toParams(StandardApiModifyVpcAttributeMsg msg) {
		ModifyVpcAttributeMsg params = BeanUtil.copyProperties(msg, ModifyVpcAttributeMsg.class);
		return params;
	}

}