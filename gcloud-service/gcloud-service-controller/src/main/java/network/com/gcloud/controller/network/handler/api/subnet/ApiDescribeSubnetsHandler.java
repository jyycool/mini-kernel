package com.gcloud.controller.network.handler.api.subnet;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DescribeSubnetParams;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DescribeSubnetModel;
import com.gcloud.header.network.msg.api.ApiDescribeSubnetsMsg;
import com.gcloud.header.network.msg.api.ApiDescribeSubnetsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.SUBNET,action="DescribeSubnets",name="子网列表")
public class ApiDescribeSubnetsHandler extends MessageHandler<ApiDescribeSubnetsMsg, ApiDescribeSubnetsReplyMsg>{
	
	@Autowired
	private ISubnetService subnetService;
	
	@Override
	public ApiDescribeSubnetsReplyMsg handle(ApiDescribeSubnetsMsg msg) throws GCloudException {
		DescribeSubnetParams params = BeanUtil.copyProperties(msg, DescribeSubnetParams.class);
		//如果没有传，copy会默认复制为0，不方便后面判断有没有传参数
		params.setNetworkType(msg.getNetworkType());
		PageResult<DescribeSubnetModel> response = subnetService.describeSubnets(params, msg.getCurrentUser());
		
		ApiDescribeSubnetsReplyMsg reply = new ApiDescribeSubnetsReplyMsg();
		reply.init(response);
		return reply;
	}

}