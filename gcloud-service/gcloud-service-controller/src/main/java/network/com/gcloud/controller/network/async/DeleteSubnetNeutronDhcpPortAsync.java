package com.gcloud.controller.network.async;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.SubnetDhcpHandle;
import com.gcloud.controller.network.provider.enums.neutron.NeutronDeviceOwner;
import com.gcloud.controller.network.util.NeutronSubnetDhcpUtil;
import com.gcloud.controller.network.util.SubnetDhcpHandleInfo;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.core.async.AsyncBase;
import com.gcloud.core.async.AsyncResult;
import com.gcloud.core.async.AsyncStatus;
import com.gcloud.core.async.NetworkThreadPool;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.openstack4j.model.network.IP;
import org.openstack4j.model.network.Port;
import org.openstack4j.model.network.Subnet;
import org.openstack4j.model.network.options.PortListOptions;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class DeleteSubnetNeutronDhcpPortAsync extends AsyncBase {


    private String gcloudSubnetId;
    private String neutronSubnetId;
    private String handleId;

    private SubnetDhcpHandle subnetDhcpHandle;
    private String neutronNetworkId;

    private Boolean canRun;

    public DeleteSubnetNeutronDhcpPortAsync() {
        init();
    }

    public DeleteSubnetNeutronDhcpPortAsync(String gcloudSubnetId, String neutronSubnetId, String handleId) {
        this.neutronSubnetId = neutronSubnetId;
        this.gcloudSubnetId = gcloudSubnetId;
        this.handleId = handleId;

        init();
    }

    public void init(){

        //用网络的pool，和async base的pool分开
        NetworkThreadPool pool = SpringUtil.getBean(NetworkThreadPool.class);
        super.setThreadPool(pool);

    }

    @Override
    public long timeout() {
        return 0;
    }

    @Override
    public long sleepTime() {
        return 1000L;
    }

    @Override
    public AsyncResult execute() {

        NeutronSubnetDhcpUtil.running(gcloudSubnetId);

        SubnetDhcpHandleInfo info = NeutronSubnetDhcpUtil.canRun(gcloudSubnetId, handleId);
        subnetDhcpHandle = info.getSubnetDhcpHandle();
        canRun = info.isCanRun();
        if(!info.isCanRun()){
            throw new GCloudException("::已经被其他处�?");
        }

        if(subnetDhcpHandle == null){
            return new AsyncResult(AsyncStatus.FAILED);
        }

        NeutronProviderProxy proxy = SpringUtil.getBean(NeutronProviderProxy.class);
        if(StringUtils.isBlank(neutronNetworkId)){
            try{
                Subnet subnet = proxy.getSubnet(neutronSubnetId);
                neutronNetworkId = subnet.getNetworkId();
            }catch (Exception ex){
                log.error("::获取子网失败", ex);
            }

        }

        boolean clear = true;
        if(!StringUtils.isBlank(neutronNetworkId)){
            PortListOptions options = PortListOptions.create().deviceOwner(NeutronDeviceOwner.DHCP.getNeutronDeviceOwner()).networkId(neutronNetworkId);

            List<? extends Port> ports = proxy.listPort(options);
            if(ports == null || ports.isEmpty()){
                for(Port port : ports){

                    for(IP fixIp : port.getFixedIps()){

                        if(!neutronSubnetId.equals(fixIp.getSubnetId())){
                            continue;
                        }

                        clear = false;
                        break;
                    }

                    if(!clear){
                        break;
                    }
                }
            }
        }


        if(clear){
            SubnetDhcpHandleInfo deleteInfo = NeutronSubnetDhcpUtil.deleteSubnet(gcloudSubnetId, handleId);
            canRun = deleteInfo.isCanRun();
            return new AsyncResult(AsyncStatus.SUCCEED);
        }

        return new AsyncResult(AsyncStatus.RUNNING);
    }

    @Override
    public String outputString() {
        return String.format("DeleteSubnetNeutronDhcpPortAsync dhcp sync subnet [%s], handle [%s]", gcloudSubnetId, handleId);
    }


    public String getNeutronNetworkId() {
        return neutronNetworkId;
    }

    public void setNeutronNetworkId(String neutronNetworkId) {
        this.neutronNetworkId = neutronNetworkId;
    }

    public String getNeutronSubnetId() {
        return neutronSubnetId;
    }

    public void setNeutronSubnetId(String neutronSubnetId) {
        this.neutronSubnetId = neutronSubnetId;
    }

}