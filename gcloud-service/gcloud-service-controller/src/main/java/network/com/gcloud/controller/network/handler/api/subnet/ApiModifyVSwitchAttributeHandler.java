package com.gcloud.controller.network.handler.api.subnet;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ModifySubnetAttributeMsg;
import com.gcloud.header.network.msg.api.ModifyVSwitchAttributeMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect="修改交换机属性成�?")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="ModifyVSwitchAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SUBNET, resourceIdField = "vSwitchId")
public class ApiModifyVSwitchAttributeHandler extends MessageHandler<ModifyVSwitchAttributeMsg, ApiReplyMessage> {
	@Autowired
	ISubnetService subnetService;
	
	@Override
	public ApiReplyMessage handle(ModifyVSwitchAttributeMsg msg) throws GCloudException {

		ModifySubnetAttributeMsg modifyMsg = new ModifySubnetAttributeMsg();
		modifyMsg.setSubnetId(msg.getvSwitchId());
		modifyMsg.setSubnetName(msg.getvSwitchName());
		modifyMsg.setCurrentUser(msg.getCurrentUser());
		modifyMsg.setDhcp(msg.getDhcp());

		subnetService.modifyAttribute(modifyMsg);
		
		msg.setObjectId(msg.getvSwitchId());
		msg.setObjectName(msg.getvSwitchName());
		return new ApiReplyMessage();
	}

}