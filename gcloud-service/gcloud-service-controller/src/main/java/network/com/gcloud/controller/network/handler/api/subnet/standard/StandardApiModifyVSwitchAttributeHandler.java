package com.gcloud.controller.network.handler.api.subnet.standard;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyVSwitchAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyVSwitchAttributeReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect="修改交换机属�?")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="ModifyVSwitchAttribute", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SUBNET, resourceIdField = "vSwitchId")
public class StandardApiModifyVSwitchAttributeHandler extends MessageHandler<StandardApiModifyVSwitchAttributeMsg, StandardApiModifyVSwitchAttributeReplyMsg>{

	@Autowired
	ISubnetService subnetService;
	
	@Override
	public StandardApiModifyVSwitchAttributeReplyMsg handle(StandardApiModifyVSwitchAttributeMsg msg)
			throws GCloudException {
		subnetService.modifyAttribute(msg.getvSwitchId(), msg.getvSwitchName(), msg.getCurrentUser());
		msg.setObjectId(msg.getvSwitchId());
		msg.setObjectName(msg.getvSwitchName());
		return new StandardApiModifyVSwitchAttributeReplyMsg();
	}

}