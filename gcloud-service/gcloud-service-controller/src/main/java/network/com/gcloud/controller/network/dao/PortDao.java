package com.gcloud.controller.network.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.model.DescribeNetworkInterfacesParams;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsMsg;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
public class PortDao extends JdbcBaseDaoImpl<Port, String> {

    public <E> PageResult<E> describePorts(DescribeNetworkInterfacesParams params, Class<E> clazz, CurrentUser currentUser){
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "p.", ResourceIsolationCheckType.PORT);
		
        List<Object> values = new ArrayList<>();
        StringBuffer sql = new StringBuffer();
        sql.append("select p.*, it.subnet_id, it.ip_address, it.router_id, psg.security_group_ids_str,f.floating_ip_address from gc_ports p")
                .append(" left join gc_floating_ips f on p.id=f.fixed_port_id ")
                .append(" left join")
                .append(" (select i.port_id, rs.router_id, group_concat(ifnull(i.subnet_id, '')) subnet_id, group_concat(ifnull(i.ip_address, '')) ip_address, group_concat(ifnull(i.network_id, '')) network_id")
                .append(" from gc_ipallocations i left join gc_subnets s on i.subnet_id = s.id")
                .append(" left join")
                .append(" (select  group_concat(rp.router_id) router_id, i.subnet_id from gc_router_ports rp left join gc_ports p")
                .append(" on rp.port_id = p.id left join gc_ipallocations i on p.id = i.port_id group by i.subnet_id")
                .append(" ) rs")
                .append(" on i.subnet_id = rs.subnet_id ")
                .append(" group by i.port_id) it")
                .append(" on p.id = it.port_id")
                .append(" left join")
                .append(" (select p2.id, group_concat(sb.security_group_id) security_group_ids_str  ")
                .append(" from gc_ports p2, gc_security_group_port_bindings sb")
                .append(" where p2.id = sb.port_id group by p2.id) psg")
                .append(" on p.id = psg.id")
                .append(" where 1 = 1");


        if(StringUtils.isNotBlank(params.getvSwitchId())){
            sql.append(" and it.subnet_id = ?");
            values.add(params.getvSwitchId());
        }

        if(StringUtils.isNotBlank(params.getPrimaryIpAddress())){
            sql.append(" and it.ip_address like concat('%', ?, '%')");
            values.add(params.getPrimaryIpAddress());
        }

        if(StringUtils.isNotBlank(params.getNetworkInterfaceName())){
            sql.append(" and p.name like concat('%', ?, '%')");
            values.add(params.getNetworkInterfaceName());
        }

        if(StringUtils.isNotBlank(params.getInstanceId())){
            sql.append(" and p.device_id = ?");
            values.add(params.getInstanceId());
        }

        if(StringUtils.isNotBlank(params.getSecurityGroupId())){
            sql.append(" and p.id in (select sgb.port_id from gc_security_group_port_bindings sgb where sgb.security_group_id = ?)");
            values.add(params.getSecurityGroupId());
        }

        if(params.getNetworkInterfaceIds() != null && params.getNetworkInterfaceIds().size() > 0){
            String inPreSql = SqlUtil.inPreStr(params.getNetworkInterfaceIds().size());
            sql.append(" and p.id in (").append(inPreSql).append(")");
            values.addAll(params.getNetworkInterfaceIds());
        }


        List<String> owners = ownerList(params.getEni(), params.getDeviceOwners());
        if(owners == null){
            return ApiUtil.emptyPage(params);
        }

        if(owners.size() > 0){
            String inPreSql = SqlUtil.inPreStr(owners.size());
            if(params.getIncludeOwnerless() != null && params.getIncludeOwnerless()){
                sql.append(" and (p.device_owner in (").append(inPreSql).append(") or p.device_owner is null or p.device_owner = '' )");
            }else{
                sql.append(" and p.device_owner in (").append(inPreSql).append(")");
            }

            values.addAll(owners);
        }
        
        if(params.getAvail() != null) {
        	if(params.getAvail()) {
        		sql.append(" and (device_id is NULL or device_id = '') ");
        	} else {
        		sql.append(" and (device_id is NOT NULL AND device_id <> '') ");
        	}
        }
        
        if(params.getIsFixed() != null) {
        	if(params.getIsFixed()) {
        		sql.append(" and (f.fixed_port_id is NOT NULL and f.fixed_port_id <> '') ");
        	} else {
        		sql.append(" and (f.fixed_port_id is NULL or f.fixed_port_id = '') ");
        	}
        }
        
        sql.append(sqlModel.getWhereSql());
        values.addAll(sqlModel.getParams());

        sql.append(" order by create_time desc");

        return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);

    }

    public int attachPort(String portId, String deviceId, String deviceOwner, String sufId, String preName, String aftName, String brName, String customOvsBr, Boolean noArpLimit){

        StringBuffer sql = new StringBuffer();
        sql.append("update gc_ports set device_id = ?, device_owner = ?,");
        sql.append(" suf_id = ?, pre_name = ?, aft_name = ?, br_name = ?, ovs_bridge_id = ?, no_arp_limit = ?");
        sql.append(" where id = ? and (device_id is null or device_id = '' )");

        Object[] values = {deviceId, deviceOwner, sufId, preName, aftName, brName, customOvsBr, noArpLimit, portId};

        return this.jdbcTemplate.update(sql.toString(), values);

    }
    
    public <E> List<E> getInstanceNetworkInterfaces(String instanceId, Class<E> clazz) {
        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        sql.append("select p.id as networkInterfaceId, p.mac_address as macAddress, i.ip_address as primaryIpAddress from gc_ports p left join gc_ipallocations i on p.id = i.port_id");
        sql.append(" where p.device_id = ?");
        values.add(instanceId);

        List<String> deviceOwnerValues = DeviceOwner.eniDeviceOwnerValues();
        sql.append(" and p.device_owner in (").append(SqlUtil.inPreStr(deviceOwnerValues.size())).append(")");
        values.addAll(deviceOwnerValues);

    	return findBySql(sql.toString(), values, clazz);
    }

    public <E> List<E> getInstancePortAndIp(String instanceId, Class<E> clazz){

        List<Object> values = new ArrayList<>();
        StringBuffer sql = new StringBuffer();
        sql.append("select p.*, i.ip_address, i.subnet_id from gc_ports p left join gc_ipallocations i on p.id = i.port_id where p.device_id = ?");
        values.add(instanceId);

        return findBySql(sql.toString(), values, clazz);

    }

    public List<Port> subnetPorts(String subnetId){

        return subnetPorts(subnetId, null);
    }

    public List<Port> subnetPorts(String subnetId, String deviceOwner){

        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        sql.append("select * from gc_ports t where t.id in");
        sql.append(" (select i.port_id from gc_ipallocations i where i.subnet_id = ?)");
        values.add(subnetId);

        if(!StringUtils.isBlank(deviceOwner)){
            sql.append(" and device_owner = ?");
            values.add(deviceOwner);
        }

        return findBySql(sql.toString(), values);
    }
    
    public int statisticPort(ApiNetworkInterfaceStatisticsMsg params) {
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(params.getCurrentUser(), "p.", ResourceIsolationCheckType.PORT);
		
    	StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();
        sql.append("select count(p.id) from gc_ports p where 1=1 ");

        List<String> owners = ownerList(params.getEni(), params.getDeviceOwners());
        if(owners == null){
            return 0;
        }

        if(owners.size() > 0){
            String inPreSql = SqlUtil.inPreStr(owners.size());
            if(params.getIncludeOwnerless() != null && params.getIncludeOwnerless()){
                sql.append(" and (p.device_owner in (").append(inPreSql).append(") or p.device_owner is null or p.device_owner = '' )");
            }else{
                sql.append(" and p.device_owner in (").append(inPreSql).append(")");
            }

            values.addAll(owners);
        }

		sql.append(sqlModel.getWhereSql());
        values.addAll(sqlModel.getParams());
		return this.countBySql(sql.toString(), values);
	}
    
    public <E> List<E> getAllInstanceNetworkInfaceItem(CurrentUser currentUser, Class<E> clazz){
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "p.", ResourceIsolationCheckType.PORT);
		
    	StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        sql.append("SELECT p.device_id as instance_id,GROUP_CONCAT(CONCAT_WS('#', i.port_id,i.ip_address,i.subnet_id)) as network_interface_item ");
        sql.append("from gc_ports p LEFT JOIN gc_ipallocations i on p.id=i.port_id ");

        List<String> deviceOwnerValues = DeviceOwner.eniDeviceOwnerValues();
        sql.append(" where p.device_owner in (").append(SqlUtil.inPreStr(deviceOwnerValues.size())).append(")");
        values.addAll(deviceOwnerValues);

        sql.append(sqlModel.getWhereSql());
        values.addAll(sqlModel.getParams());
        
        sql.append("group by p.device_id");
        
        return findBySql(sql.toString(), values,  clazz);
    }

    public List<Port> instancePorts(String instanceId){

        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        sql.append("select * from gc_ports t where t.device_id = ?");
        values.add(instanceId);

        List<String> eniDeviceOwners = DeviceOwner.eniDeviceOwnerValues();
        if(eniDeviceOwners != null && eniDeviceOwners.size() > 0){
            sql.append(" and t.device_owner in (").append(SqlUtil.inPreStr(eniDeviceOwners.size())).append(")");
            values.addAll(eniDeviceOwners);

        }else{
            return new ArrayList<>();
        }

        return findBySql(sql.toString(), values);
    }

    private List<String> ownerList(Boolean eni, List<String> deviceOwners){
        List<String> owners = new ArrayList<>();
        if(eni != null && eni){
            owners.addAll(DeviceOwner.eniDeviceOwnerValues());
            //没有eni的设�?
            if(owners.size() == 0){
                return null;
            }

            if(deviceOwners != null && deviceOwners.size() > 0){
                owners.retainAll(deviceOwners);
            }
        }else{
            if(deviceOwners != null && deviceOwners.size() > 0){
                owners.addAll(deviceOwners);
            }
        }

        return owners;
    }

}