package com.gcloud.controller.network.handler.api.externalnetwork;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DetailExternalNetworkParams;
import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailExternalNetwork;
import com.gcloud.header.network.msg.api.ApiDetailExternalNetworkMsg;
import com.gcloud.header.network.msg.api.ApiDetailExternalNetworkReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="DetailExternalNetwork",name="外网详情")
public class ApiDetailExternalNetworkHandler extends MessageHandler<ApiDetailExternalNetworkMsg, ApiDetailExternalNetworkReplyMsg>{

	@Autowired
	INetworkService networkService;
	
	@Override
	public ApiDetailExternalNetworkReplyMsg handle(ApiDetailExternalNetworkMsg msg) throws GCloudException {
		DetailExternalNetworkParams params = BeanUtil.copyProperties(msg, DetailExternalNetworkParams.class);
		DetailExternalNetwork response = networkService.detailExternalNetwork(params, msg.getCurrentUser());
		
		ApiDetailExternalNetworkReplyMsg reply = new ApiDetailExternalNetworkReplyMsg();
		reply.setDetailExternalNetwork(response);
		return reply;
	}

}