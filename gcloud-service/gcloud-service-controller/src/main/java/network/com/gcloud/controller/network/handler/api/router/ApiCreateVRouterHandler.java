package com.gcloud.controller.network.handler.api.router;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IRouterService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateVRouterMsg;
import com.gcloud.header.network.msg.api.CreateVRouterReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="创建路由成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.VROUTER,action="CreateVRouter",name="创建路由")
public class ApiCreateVRouterHandler extends MessageHandler<CreateVRouterMsg, CreateVRouterReplyMsg> {
	@Autowired
	IRouterService vRouterService;
	
	@Override
	public CreateVRouterReplyMsg handle(CreateVRouterMsg msg) throws GCloudException {
		CreateVRouterReplyMsg reply=new CreateVRouterReplyMsg();
		reply.setvRouterId(vRouterService.createVRouter(msg));
		msg.setObjectId(reply.getvRouterId());
		msg.setObjectName(msg.getvRouterName());
		return reply;
	}

}