package com.gcloud.controller.network.util;

import com.gcloud.controller.network.entity.SubnetDhcpHandle;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class SubnetDhcpHandleInfo {

    private boolean canRun;
    private SubnetDhcpHandle subnetDhcpHandle;
    private boolean operationSucc;

    public SubnetDhcpHandleInfo() {
    }

    public SubnetDhcpHandleInfo(boolean canRun, SubnetDhcpHandle subnetDhcpHandle, boolean operationSucc) {
        this.canRun = canRun;
        this.subnetDhcpHandle = subnetDhcpHandle;
        this.operationSucc = operationSucc;
    }

    public boolean isCanRun() {
        return canRun;
    }

    public void setCanRun(boolean canRun) {
        this.canRun = canRun;
    }

    public SubnetDhcpHandle getSubnetDhcpHandle() {
        return subnetDhcpHandle;
    }

    public void setSubnetDhcpHandle(SubnetDhcpHandle subnetDhcpHandle) {
        this.subnetDhcpHandle = subnetDhcpHandle;
    }

    public boolean isOperationSucc() {
        return operationSucc;
    }

    public void setOperationSucc(boolean operationSucc) {
        this.operationSucc = operationSucc;
    }
}