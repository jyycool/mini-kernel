package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.QosDirection;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum NeutronQosDirection {
    EGRESS(org.openstack4j.model.network.QosDirection.EGRESS, QosDirection.EGRESS),
    INGRESS(org.openstack4j.model.network.QosDirection.INGRESS, QosDirection.INGRESS);

    NeutronQosDirection(org.openstack4j.model.network.QosDirection neutronQosDirection, QosDirection qosDirection) {
        this.neutronQosDirection = neutronQosDirection;
        this.qosDirection = qosDirection;
    }

    private org.openstack4j.model.network.QosDirection neutronQosDirection;
    private QosDirection qosDirection;

    public QosDirection getQosDirection() {
        return qosDirection;
    }

    public org.openstack4j.model.network.QosDirection getNeutronQosDirection() {
        return neutronQosDirection;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronQosDirection direction = Arrays.stream(NeutronQosDirection.values()).filter(o -> o.getNeutronQosDirection().toJson().equals(neutronValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getQosDirection().value();
    }

    public static org.openstack4j.model.network.QosDirection getByGcValue(QosDirection gcValue){
        NeutronQosDirection direction = Arrays.stream(NeutronQosDirection.values()).filter(o -> o.getQosDirection().equals(gcValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getNeutronQosDirection();
    }
}