package com.gcloud.controller.image.distribute;

import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.enums.ImageDistributeTargetType;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.msg.node.DownloadImageMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class FileToNodeDistributeImageImpl implements IDistributeImage {
	@Autowired
	private MessageBus bus;
	
	@Autowired
	ImageProp prop;

	@Override
	public void distributeImage(String imageId, String target, String taskId, String resourceType, String distributeAction) {
		DownloadImageMsg downloadMsg = new DownloadImageMsg();
        downloadMsg.setServiceId(MessageUtil.imageServiceId(target));
        downloadMsg.setImageId(imageId);
        downloadMsg.setImagePath((resourceType.equals(ImageResourceType.IMAGE.value())?prop.getImageFilesystemStoreDir():prop.getIsoFilesystemStoreDir()) + imageId);
        downloadMsg.setProvider(ProviderType.GCLOUD.name().toLowerCase());
        downloadMsg.setImageStroageType(ImageDriverEnum.FILE.getStorageType());
        downloadMsg.setTarget(target);
        downloadMsg.setTargetType(ImageDistributeTargetType.NODE.value());
        downloadMsg.setTaskId(taskId);
        downloadMsg.setImageResourceType(resourceType);
        downloadMsg.setDistributeAction(distributeAction);
        
        bus.send(downloadMsg);
	}
	
	/*@Override
	public void deleteImageCache(String imageId, String target, String storeType, String taskId) {
		DeleteImageMsg deleteMsg = new DeleteImageMsg();
		deleteMsg.setServiceId(MessageUtil.imageServiceId(target));
		deleteMsg.setImageId(imageId);
		deleteMsg.setStoreTarget(target);
		deleteMsg.setStoreType(storeType);
		deleteMsg.setTaskId(taskId);
        
        bus.send(deleteMsg);
	}*/

}