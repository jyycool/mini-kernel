package com.gcloud.controller.image.handler.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.service.IImageStoreService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.image.msg.node.DownloadImageReplyMsg;
import com.gcloud.service.common.enums.ImageStoreStatus;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class DownloadImageReplyHandler extends AsyncMessageHandler<DownloadImageReplyMsg>{
	@Autowired
	IImageStoreService imageStoreService;
	
	@Override
	public void handle(DownloadImageReplyMsg msg) {
		log.info("DownloadImageReplyHandler start");
		// 修改image_store_info表对应记录状�?
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("imageId", msg.getImageId());
		props.put("storeTarget", msg.getStoreTarget());
		props.put("resourceType", msg.getImageResourceType());
		ImageStore store = imageStoreService.findUniqueByProperties(props);
		if(msg.getSuccess()) {
			List<String> updateFields = new ArrayList<String>();
			updateFields.add(store.updateStatus(ImageStoreStatus.ACTIVE.value()));
			
			imageStoreService.update(store, updateFields);
		} else {
			imageStoreService.delete(store);
		}
	}

}