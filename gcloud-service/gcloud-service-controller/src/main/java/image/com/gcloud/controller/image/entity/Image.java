package com.gcloud.controller.image.entity;

import com.gcloud.controller.ResourceProviderEntity;
import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_images", jdbc = "controllerJdbcTemplate")
public class Image extends ResourceProviderEntity {

    @ID
    private String id;
    private String name;
    private Long size;
    private String status;
    private Date createdAt;
    private Date updatedAt;
    private Long minDisk;
    private String owner;
    private String ownerType;
    private String tenantId;
    private Boolean disable;//true禁用false可用
    private String parentId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getMinDisk() {
        return minDisk;
    }

    public void setMinDisk(Long minDisk) {
        this.minDisk = minDisk;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String SIZE = "size";
    public static final String STATUS = "status";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    public static final String MIN_DISK = "minDisk";
    public static final String OWNER = "owner";
    public static final String OWNER_TYPE = "ownerType";
    public static final String TENANT_ID = "tenantId";
    public static final String DISABLE = "disable";
    public static final String PARENT_ID = "parentId";
    public String updateId (String id){
        this.setId(id);
        return ID;
    }
    public String updateName (String name){
        this.setName(name);
        return NAME;
    }
    public String updateSize (Long size){
        this.setSize(size);
        return SIZE;
    }
    public String updateStatus (String status){
        this.setStatus(status);
        return STATUS;
    }
    public String updateCreatedAt (Date createdAt){
        this.setCreatedAt(createdAt);
        return CREATED_AT;
    }
    public String updateUpdatedAt (Date updatedAt){
        this.setUpdatedAt(updatedAt);
        return UPDATED_AT;
    }
    public String updateMinDisk (Long minDisk){
        this.setMinDisk(minDisk);
        return MIN_DISK;
    }
    public String updateOwner (String owner){
        this.setOwner(owner);
        return OWNER;
    }
    public String updateOwnerType (String ownerType){
        this.setOwnerType(ownerType);
        return OWNER_TYPE;
    }
    public String updateTenantId (String tenantId){
        this.setTenantId(tenantId);
        return TENANT_ID;
    }
    public String updateDisable (Boolean disable){
        this.setDisable(disable);
        return DISABLE;
    }
    public String updateParentId (String parentId){
        this.setParentId(parentId);
        return PARENT_ID;
    }
}