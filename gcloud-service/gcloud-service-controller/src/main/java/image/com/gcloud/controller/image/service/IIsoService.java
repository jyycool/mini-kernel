package com.gcloud.controller.image.service;


import com.gcloud.controller.image.model.iso.CreateIsoParams;
import com.gcloud.controller.image.model.iso.DescribeIsoParams;
import com.gcloud.controller.image.model.iso.DetailIsoParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.image.model.iso.DetailIso;
import com.gcloud.header.image.model.iso.IsoType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IIsoService {
	String createIso(CreateIsoParams params, CurrentUser currentUser);
	
	void updateIso(String imageId, String imageName) throws GCloudException;

    void deleteIso(String imageId) throws GCloudException;

    PageResult<IsoType> describeIso(DescribeIsoParams params, CurrentUser currentUser);

    void disableIso(String imageId, Boolean disable);
    
    DetailIso detailIso(DetailIsoParams params, CurrentUser currentUser);
    
}