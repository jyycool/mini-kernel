package com.gcloud.controller.image.workflow.iso;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.image.dao.IsoMapInfoDao;
import com.gcloud.controller.image.entity.IsoMapInfo;
import com.gcloud.controller.image.workflow.model.iso.UnmapAndDelLnGcloudIsoFlowCommandReq;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.IsoMapStatus;
import com.gcloud.header.compute.msg.node.vm.iso.RbdmapAndLnIsoCacheMsg;
import com.gcloud.header.compute.msg.node.vm.iso.UnmapAndRmLnIsoCacheMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class UnmapAndDelLnGcloudIsoFlowCommand extends BaseWorkFlowCommand{
	@Autowired
    private MessageBus bus;
	
	@Autowired
    private IsoMapInfoDao isoMapInfoDao;
	
	@Autowired
    private ControllerComputeProp prop;
	
	@Override
	public boolean judgeExecute() {
		UnmapAndDelLnGcloudIsoFlowCommandReq req = (UnmapAndDelLnGcloudIsoFlowCommandReq)getReqParams();
		return req.getRepeatParams()!=null;
	}
	
	@Override
	protected Object process() throws Exception {
		UnmapAndDelLnGcloudIsoFlowCommandReq req = (UnmapAndDelLnGcloudIsoFlowCommandReq)getReqParams();
		IsoMapInfo isoMap = req.getRepeatParams();
		isoMap.setStatus(IsoMapStatus.UNMAPPING.name());
		isoMapInfoDao.update(isoMap);
		
		UnmapAndRmLnIsoCacheMsg msg = new UnmapAndRmLnIsoCacheMsg();
		msg.setIsoId(isoMap.getIsoId());
		msg.setLnPath(isoMap.getLnPath());
		msg.setPoolName(isoMap.getPoolName());
		msg.setHostname(isoMap.getHostname());
		msg.setTaskId(getTaskId());
		msg.setServiceId(MessageUtil.computeServiceId(isoMap.getHostname()));
		
		bus.send(msg);
		
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		/*UnmapAndDelLnGcloudIsoFlowCommandReq req = (UnmapAndDelLnGcloudIsoFlowCommandReq)getReqParams();
		IsoMapInfo isoMapDel = req.getRepeatParams();
		
		IsoMapInfo isoMap = new IsoMapInfo();
		isoMap.setHostname(isoMapDel.getHostname());
		isoMap.setIsoId(isoMapDel.getIsoId());
		isoMap.setLnPath(isoMapDel.getLnPath());
		isoMap.setStatus(IsoMapStatus.MAPPING.name());
		isoMap.setCreatedAt(isoMapDel.getCreatedAt());
		
		isoMapInfoDao.save(isoMap);
		
		RbdmapAndLnIsoCacheMsg msg = new RbdmapAndLnIsoCacheMsg();
		msg.setIsoId(isoMapDel.getIsoId());
		msg.setPoolName(prop.getDefaultCephIsoCachePoolId());
		msg.setTaskId(getTaskId());
		msg.setServiceId(MessageUtil.computeServiceId(isoMapDel.getHostname()));
		
		bus.send(msg);*/
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return UnmapAndDelLnGcloudIsoFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}