package com.gcloud.controller.image.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.model.iso.DescribeIsoParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Repository
public class IsoDao extends JdbcBaseDaoImpl<Iso, String>{
	public <E> PageResult<E> describeIsos(DescribeIsoParams params, Class<E> clazz, CurrentUser currentUser){
		StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();
        sql.append("select i.* from gc_isos i ");
        sql.append(" where 1 = 1");

        if(StringUtils.isNotBlank(params.getIsoId())){
            sql.append(" and i.id = ?");
            values.add(params.getIsoId());
        }

        if(StringUtils.isNotBlank(params.getIsoName())){
            sql.append(" and i.name like concat('%', ?, '%')");
            values.add(params.getIsoName());
        }

        if(StringUtils.isNotBlank(params.getStatus())){
            sql.append(" and i.status = ?");
            values.add(params.getStatus());
        }
        
        if(StringUtils.isNotBlank(params.getIsoType())){
            sql.append(" and i.iso_type = ?");
            values.add(params.getIsoType());
        }
        
        if(params.getDisable() != null){
            sql.append(" and i.disable = ?");
            values.add(params.getDisable());
        }
        
        sql.append(" order by i.created_at desc");

        return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);
	}
}