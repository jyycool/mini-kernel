package com.gcloud.controller.image.handler.api;

import com.gcloud.controller.image.workflow.DeleteGcloudImageWorkflow;
import com.gcloud.controller.image.workflow.model.DeleteGcloudImageFlowCommandRes;
import com.gcloud.controller.image.workflow.model.DeleteGcloudImageWorkflowReq;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.msg.api.ApiDeleteImageMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@LongTask
@GcLog(isMultiLog = true, taskExpect = "删除镜像")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DeleteImage")

public class ApiDeleteImageHandler extends BaseWorkFlowHandler<ApiDeleteImageMsg, ApiReplyMessage> {

    @Override
    public Object initParams(ApiDeleteImageMsg msg) {
    	DeleteGcloudImageWorkflowReq req = new DeleteGcloudImageWorkflowReq();
    	req.setImageId(msg.getImageId());
    	return req;
    }

	@Override
	public Object preProcess(ApiDeleteImageMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public ApiReplyMessage process(ApiDeleteImageMsg msg) throws GCloudException {
		ApiReplyMessage replyMessage = new ApiReplyMessage();
		DeleteGcloudImageFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DeleteGcloudImageFlowCommandRes.class);
		replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getImageId())
				.objectName(CacheContainer.getInstance().getString(CacheType.IMAGE_NAME, msg.getImageId())).expect("删除镜像").build());

        return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return DeleteGcloudImageWorkflow.class;
	}

    /*@Override
    public ApiReplyMessage handle(ApiDeleteImageMsg msg) throws GCloudException {
        imageService.deleteImage(msg.getImageId());
        msg.setObjectId(msg.getImageId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.IMAGE_NAME, msg.getImageId()));
        return new ApiReplyMessage();
    }*/
}