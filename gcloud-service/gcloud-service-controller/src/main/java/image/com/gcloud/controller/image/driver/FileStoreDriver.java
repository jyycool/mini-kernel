package com.gcloud.controller.image.driver;


import com.gcloud.common.util.FileUtil;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.controller.image.distribute.ImageDistributeEnum;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.image.enums.ImageResourceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
@Slf4j
public class FileStoreDriver implements IImageStoreDriver {
	@Autowired
	private ImageProp prop;
	
	@Override
	public void copyImage(String sourceFilePath, String imageId, String resourceType) {
		//cp image
		String[] cmd = null;
        cmd = new String[]{"cp", sourceFilePath, (resourceType.equals(ImageResourceType.IMAGE.value())?prop.getImageFilesystemStoreDir():prop.getIsoFilesystemStoreDir()) + imageId};
        int res = SystemUtil.runAndGetCode(cmd);
        
        if(res != 0) {//失败
        	throw new GCloudException("::上传镜像失败");
        }
	}

	@Override
	public void deleteImage(String imageId, String resourceType) {
		//remove image
		String[] cmd = null;
        cmd = new String[]{"rm", "-f", (resourceType.equals(ImageResourceType.IMAGE.value())?prop.getImageFilesystemStoreDir():prop.getIsoFilesystemStoreDir()) + imageId};
        int res = SystemUtil.runAndGetCode(cmd);
        if(res != 0) {//失败
        	throw new GCloudException("::删除镜像失败");
        }
	}

	/* 
	 * target �? vg-ID、hostname、image pool
	 * targetType : vg、node、pool
	 */
	@Override
	public void distributeImage(String imageId, String target, String targetType, String taskId, String resourceType, String distributeAction) {
		//file store in nfs 
		ImageDistributeEnum.getByType(ImageDriverEnum.FILE.name(), targetType).distributeImage(imageId, target, taskId, resourceType, distributeAction);
	}

	@Override
	public long getImageActualSize(String imageId) {
		long size = 0;
		try {
			size = FileUtil.getFileSize(prop.getImageFilesystemStoreDir() + imageId);
        } catch(Exception ex) {
        	throw new GCloudException("::获取镜像文件大小出错");
        }
		return size;
	}

	/*@Override
	public void deleteImageCache(String imageId) {
		List<ImageStore> stores = storeDao.findByProperty("imageId", imageId);
		for(ImageStore store:stores) {
			DeleteImageMsg deleteMsg = new DeleteImageMsg();
			String controllerService = MessageUtil.controllerServiceId();
			String controllerHost = controllerService.substring(controllerService.indexOf("-") + 1);
			deleteMsg.setServiceId(store.getStoreType().equals("node")?MessageUtil.imageServiceId(store.getStoreTarget()):MessageUtil.imageServiceId(controllerHost));
			deleteMsg.setImageId(imageId);
			deleteMsg.setStoreTarget(store.getStoreTarget());
			deleteMsg.setStoreType(store.getStoreType());
			deleteMsg.setTaskId(null);
	        
	        bus.send(deleteMsg);
//			ImageDistributeEnum.getByType(ImageDriverEnum.FILE.name(), store.getStoreType()).deleteImageCache(imageId, store.getStoreTarget(), null);
		}
	}*/


	@Override
	public Resource download(String imageId) {
		try {
			String filePath = FileUtil.fileString(prop.getImageFilesystemStoreDir(), imageId);
			Resource resource = new UrlResource((new File(filePath)).toURI());
			if(resource.exists()) {
				return resource;
			} else {
				throw new GCloudException("::镜像文件不存�?");
			}
		} catch (Exception ex) {
			throw new GCloudException("::镜像文件不存�?");
		}
	}

	@Override
	public void upload(String imageId, MultipartFile file) {

		try{
			String filePath = FileUtil.fileString(prop.getImageFilesystemStoreDir(), imageId);
			log.debug("上传镜像�?:" + filePath);
			Files.copy(file.getInputStream(), (new File(filePath)).toPath(), StandardCopyOption.REPLACE_EXISTING);
		}catch (Exception ex){
			log.error("::上传失败:" + ex, ex);
			throw new GCloudException("::上传失败");
		}

	}
}