package com.gcloud.controller.image.async;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.controller.async.LogAsync;
import com.gcloud.controller.image.dao.IsoDao;
import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.core.async.AsyncResult;
import com.gcloud.core.async.AsyncStatus;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.enums.ImageStatus;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class UploadGcloudIsoAsync extends LogAsync{
	private String isoId;
	private String filePath;
	
	@Override
	public long timeout() {
		return 1800 * 1000L;
	}

	@Override
	public AsyncResult execute() {
		AsyncStatus status = AsyncStatus.SUCCEED;
        AsyncResult result = new AsyncResult();
        ImageProp prop = SpringUtil.getBean(ImageProp.class);

        try {
	        ImageDriverEnum.getByType(prop.getStroageType()).copyImage(filePath, isoId, ImageResourceType.ISO.value());
		} catch (Exception ex){
	        status = AsyncStatus.FAILED;
	        result.setErrorMsg(ErrorCodeUtil.getErrorCode(ex, "::上传映像失败"));
	    }

        result.setAsyncStatus(status);
        
        return result;
	}
	
	@Override
    public void successHandler() {

        IsoDao isoDao = SpringUtil.getBean(IsoDao.class);
        List<String> updateField = new ArrayList<>();

        Iso iso = new Iso();
        iso.setId(isoId);

        updateField.add(iso.updateStatus(ImageStatus.ACTIVE.value()));

        isoDao.update(iso, updateField);
    }

    @Override
    public void failHandler() {
        delete();
    }

    @Override
    public void timeoutHandler() {
    	 delete();
    }

    @Override
    public void exceptionHandler() {
        delete();
    }

    public void delete(){
    	ImageProp prop = SpringUtil.getBean(ImageProp.class);
    	ImageDriverEnum.getByType(prop.getStroageType()).deleteImage(isoId, ImageResourceType.ISO.value());

        IsoDao isoDao = SpringUtil.getBean(IsoDao.class);

        isoDao.deleteById(isoId);

    }

    @Override
    public void defaultHandler() {

    	IsoDao isoDao = SpringUtil.getBean(IsoDao.class);
        List<String> updateField = new ArrayList<>();

        Iso iso = new Iso();
        iso.setId(isoId);
        updateField.add(iso.updateStatus(ImageStatus.ACTIVE.name()));

        isoDao.update(iso, updateField);

    }

	public String getIsoId() {
		return isoId;
	}

	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
    
    

}