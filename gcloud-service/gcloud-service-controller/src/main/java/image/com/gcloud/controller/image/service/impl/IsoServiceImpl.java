package com.gcloud.controller.image.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.image.dao.IsoDao;
import com.gcloud.controller.image.dao.VmIsoAttachmentDao;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.controller.image.entity.enums.IsoTypeEnum;
import com.gcloud.controller.image.model.iso.CreateIsoParams;
import com.gcloud.controller.image.model.iso.DescribeIsoParams;
import com.gcloud.controller.image.model.iso.DetailIsoParams;
import com.gcloud.controller.image.provider.IIsoProvider;
import com.gcloud.controller.image.service.IIsoService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.image.enums.ImageArchitecture;
import com.gcloud.header.image.enums.ImageStatus;
import com.gcloud.header.image.enums.OsType;
import com.gcloud.header.image.model.iso.DetailIso;
import com.gcloud.header.image.model.iso.IsoType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
public class IsoServiceImpl implements IIsoService {
	
	@Autowired
	IsoDao isoDao;
	
	@Autowired
	VmIsoAttachmentDao vmIsoAttachmentDao;

	@Override
	public String createIso(CreateIsoParams params, CurrentUser currentUser) {
		if(StringUtils.isNotBlank(params.getOsType())) {
			OsType osType = OsType.value(params.getOsType());
	        if (osType == null) {
	            throw new GCloudException("0090505::不支持此操作系统类型");
	        }
		}
		if(StringUtils.isNotBlank(params.getOsType())) {
	        ImageArchitecture architecture = ImageArchitecture.value(params.getArchitecture());
	        if (architecture == null) {
	            throw new GCloudException("0090506::不支持此架构");
	        }
		}
        return this.getProviderOrDefault().createIso(params, currentUser);
	}
	
	private IIsoProvider getProviderOrDefault() {
        IIsoProvider provider = ResourceProviders.getDefault(ResourceType.ISO);
        return provider;
    }

	@Override
	public void updateIso(String isoId, String isoName) throws GCloudException {
		Iso iso = isoDao.getById(isoId);
        if (iso == null) {
            throw new GCloudException("0090603::找不到对应的映像");
        }
        this.checkAndGetProvider(iso.getProvider()).updateIso(isoId, iso.getProviderRefId(), isoName);
        CacheContainer.getInstance().put(CacheType.ISO_NAME, isoId, isoName);
	}

	@Override
	public void deleteIso(String isoId) throws GCloudException {
		Iso iso = isoDao.getById(isoId);
        if (iso == null) {
            throw new GCloudException("::找不到对应的映像");
        }
        List<VmIsoAttachment> vmIsoAttachments = vmIsoAttachmentDao.findByProperty("isoId", isoId);
        if (vmIsoAttachments.size() > 0) {
            throw new GCloudException("::有云服务器正在使用此映像，不能删�?");
        }
        this.checkAndGetProvider(iso.getProvider()).deleteIso(isoId, iso.getProviderRefId());
	}

	@Override
	public PageResult<IsoType> describeIso(DescribeIsoParams params, CurrentUser currentUser) {
		PageResult<IsoType> isoPages = isoDao.describeIsos(params, IsoType.class, currentUser);
    	for (IsoType item : isoPages.getList()) {
			item.setCnStatus(ImageStatus.getCnName(item.getStatus()));
			item.setCnIsoType(IsoTypeEnum.getCnIsoType(item.getIsoType()));
		}
    	
        return isoPages;
	}

	@Override
	public void disableIso(String isoId, Boolean disable) {
		Iso iso = isoDao.getById(isoId);
        if (iso == null) {
            throw new GCloudException("0090803::找不到对应的映像");
        }
        List<String> updateFields = new ArrayList<String>();
        updateFields.add(iso.updateDisable(disable));
        
        isoDao.update(iso, updateFields);
	}

	@Override
	public DetailIso detailIso(DetailIsoParams params, CurrentUser currentUser) {
		DetailIso response = new DetailIso();
		Iso iso = isoDao.getById(params.getIsoId());
        if (iso == null) {
            throw new GCloudException("0090902::找不到对应的映像");
        }
		
		response.setCnStatus(ImageStatus.getCnName(iso.getStatus()));
		response.setCreationTime(iso.getCreatedAt());
		response.setIsoId(iso.getId());
		response.setIsoName(iso.getName());
		response.setOsType(iso.getOsType());
		response.setCnIsoType(IsoTypeEnum.getCnIsoType(iso.getIsoType()));
		response.setArchitecture(iso.getArchitecture());
		response.setDescription(iso.getDescription());
		response.setSize(iso.getSize());
		response.setStatus(iso.getStatus());
		response.setIsoType(iso.getIsoType());
		response.setOsVersion(iso.getOsVersion());
		response.setDisable(iso.getDisable());
		response.setOwner(iso.getOwner());
		response.setIsoOwnerAlias(CacheContainer.getInstance().getString(CacheType.USER_NAME, iso.getOwner()));
		return response;
	}
	
	private IIsoProvider checkAndGetProvider(Integer providerType) {
        IIsoProvider provider = ResourceProviders.checkAndGet(ResourceType.ISO, providerType);
        return provider;
    }

}