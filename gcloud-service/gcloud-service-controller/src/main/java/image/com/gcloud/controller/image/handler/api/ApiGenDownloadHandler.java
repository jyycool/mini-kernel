package com.gcloud.controller.image.handler.api;

import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.msg.api.ApiGenDownloadMsg;
import com.gcloud.header.image.msg.api.ApiGenDownloadReplyMsg;

import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@LongTask
@ApiHandler(module = Module.ECS, subModule = SubModule.IMAGE, action = "GenDownload",name="生成下载镜像信息（内部组件调用）")
public class ApiGenDownloadHandler extends MessageHandler<ApiGenDownloadMsg, ApiGenDownloadReplyMsg> {

    @Autowired
    private IImageService imageService;
    
    @Override
    public ApiGenDownloadReplyMsg handle(ApiGenDownloadMsg msg) throws GCloudException {
        ApiGenDownloadReplyMsg reply = new ApiGenDownloadReplyMsg();
        try {
            reply.setDownloadInfo(imageService.genDownload(msg.getImageId()));
            reply.setSuccess(true);
        }
        catch (Exception e) {
            reply.setSuccess(false);
            reply.setErrorMsg(e.getMessage());
        }
        
        return reply;
    }

}