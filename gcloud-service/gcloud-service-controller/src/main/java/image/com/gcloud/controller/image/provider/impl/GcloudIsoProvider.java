package com.gcloud.controller.image.provider.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.image.async.UploadGcloudIsoAsync;
import com.gcloud.controller.image.dao.ImageStoreDao;
import com.gcloud.controller.image.dao.IsoDao;
import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.image.entity.enums.ImagePropertyItem;
import com.gcloud.controller.image.model.iso.CreateIsoParams;
import com.gcloud.controller.image.model.iso.DistributeIsoParams;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.controller.image.provider.IIsoProvider;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.NoRollbackFlow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.ImageOwnerType;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.enums.IsoFormat;
import com.gcloud.header.image.enums.IsoOwnerType;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.enums.DistributeAction;
import com.gcloud.service.common.enums.ImageStoreStatus;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Component
public class GcloudIsoProvider implements IIsoProvider{
	@Autowired
    private IsoDao isoDao;
    
    @Autowired
    ImageProp prop;
    
	@Autowired
	private MessageBus bus;
	
	@Autowired
	private ImageStoreDao storeDao;
	
	@Override
	public ResourceType resourceType() {
		return ResourceType.ISO;
	}

	@Override
	public ProviderType providerType() {
		return ProviderType.GCLOUD;
	}

	@Override
	public String createIso(CreateIsoParams params, CurrentUser currentUser) throws GCloudException {
		File file = new File(params.getFilePath());
        if (!file.exists()) {
            throw new GCloudException("0090507::映像文件不存�?");
        }

        String isoId = UUID.randomUUID().toString();

        QemuInfo info = DiskQemuImgUtil.info(params.getFilePath());

        long minDisk = (long)Math.ceil(info.getVirtualSize() / 1024.0 / 1024.0 / 1024.0);

        SimpleFlowChain<Long, String> chain = new SimpleFlowChain<>("create iso");
        chain.then(new Flow<Long>() {
            @Override
            public void run(SimpleFlowChain chain, Long data) {

                chain.data(file.length());
                Iso iso = new Iso();
                iso.setId(isoId);
                iso.setSize(file.length());
                iso.setCreatedAt(new Date());
                iso.setName(params.getIsoName());
                iso.setStatus(com.gcloud.header.image.enums.ImageStatus.SAVING.value());
                iso.setOwner(currentUser.getId());
                iso.setTenantId(currentUser.getDefaultTenant());
                iso.setProvider(providerType().getValue());
                iso.setProviderRefId(isoId);
                //暂时默认都是公共
                iso.setOwnerType(IsoOwnerType.SYSTEM.value());
                iso.setDisable(false);
                iso.setDescription(params.getDescription());
                iso.setFormat(IsoFormat.ISO.value());
                iso.setIsoType(params.getIsoType());
                iso.setOsType(params.getOsType());
                iso.setOsVersion(params.getOsVersion());
                iso.setArchitecture(params.getArchitecture());
                isoDao.save(iso);

                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, Long data) {
                isoDao.deleteById(isoId);
                chain.rollback();
            }
        }).then(new NoRollbackFlow<Long>() {
            @Override
            public void run(SimpleFlowChain chain, Long data) {
            	//通过镜像文件大小来判断是否拷贝完整来判断可用状�??
                UploadGcloudIsoAsync async = new UploadGcloudIsoAsync();
                async.setIsoId(isoId);
                async.setFilePath(params.getFilePath());
                async.setTaskId(params.getTaskId());
                async.start();
                chain.next();
            }
        }).start();

        if (chain.getErrorCode() != null) {
            throw new GCloudException(chain.getErrorCode());
        }

        return isoId;
	}

	@Override
	public void updateIso(String isoId, String imageProviderRefId, String isoName) throws GCloudException {
		// 只需要更改数据库属�??
		List<String> updateField = new ArrayList<>();
        Iso iso = new Iso();
        iso.setId(isoId);
        updateField.add(iso.updateName(isoName));
        isoDao.update(iso, updateField);
	}

	@Override
	public void deleteIso(String isoId, String imageProviderRefId) throws GCloudException {
		ImageDriverEnum.getByType(prop.getStroageType()).deleteImage(isoId, ImageResourceType.ISO.value());
    	isoDao.deleteById(isoId);
	}

	@Override
	public List<Iso> listIso(Map<String, String> filters) throws GCloudException {
		throw new GCloudException("no need to implement.");
	}

	@Override
	public void distributeIso(DistributeIsoParams params) {
		//记录image_store_info
		String distributeAction = DistributeAction.DISTRIBUTE.value();
    	try {
			ImageStore store = new ImageStore();
			store.setId(String.format("%s_%s_%s_%s", params.getIsoId(), params.getTargetType(), params.getTarget(), ImageResourceType.ISO.value()));
			store.setImageId(params.getIsoId());
			store.setStoreTarget(params.getTarget());
			store.setStoreType(params.getTargetType());
			store.setStatus(ImageStoreStatus.DOWNLOADING.value());
			store.setResourceType(ImageResourceType.ISO.value());
			
			storeDao.save(store);
    	}catch(Exception ex) {
			distributeAction = DistributeAction.CHECK.value();
		}
    			
    	ImageDriverEnum.getByType(prop.getStroageType()).distributeImage(params.getIsoId(), params.getTarget(), params.getTargetType(), params.getTaskId(), ImageResourceType.ISO.value(), distributeAction);
	}

}