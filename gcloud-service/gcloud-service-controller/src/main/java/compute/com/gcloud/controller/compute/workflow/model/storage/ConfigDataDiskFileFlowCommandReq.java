package com.gcloud.controller.compute.workflow.model.storage;

import com.gcloud.header.storage.model.VmVolumeDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ConfigDataDiskFileFlowCommandReq {

	private String instanceId;
	private String vmHostName;
	private VmVolumeDetail volumeDetail;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public VmVolumeDetail getVolumeDetail() {
		return volumeDetail;
	}

	public void setVolumeDetail(VmVolumeDetail volumeDetail) {
		this.volumeDetail = volumeDetail;
	}

	public String getVmHostName() {
		return vmHostName;
	}

	public void setVmHostName(String vmHostName) {
		this.vmHostName = vmHostName;
	}

}