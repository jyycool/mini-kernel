package com.gcloud.controller.compute.handler.api.vm.base;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.vm.ModifyInstanceSpecInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.vm.base.ModifyInstanceSpecWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.ApiModifyInstanceSpecMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiModifyInstanceSpecReplyMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "ModifyInstanceSpec")
@LongTask
@GcLog(isMultiLog = true, taskExpect = "修改实例规格")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class ApiModifyInstanceSpecHandler extends BaseWorkFlowHandler<ApiModifyInstanceSpecMsg, ApiModifyInstanceSpecReplyMsg> {

	@Override
	public Class getWorkflowClass() {
		return ModifyInstanceSpecWorkflow.class;
	}

	@Override
	public Object preProcess(ApiModifyInstanceSpecMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public ApiModifyInstanceSpecReplyMsg process(ApiModifyInstanceSpecMsg msg) throws GCloudException {
		ApiModifyInstanceSpecReplyMsg replyMessage = new ApiModifyInstanceSpecReplyMsg();
		ModifyInstanceSpecInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), ModifyInstanceSpecInitFlowCommandRes.class);

		replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId())
				.objectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId())).expect("修改实例规格").build());

		return replyMessage;
	}
}