package com.gcloud.controller.compute.workflow.vm.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.GenIDUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.workflow.model.vm.ModifyVmHostNameFlowCommandReq;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.base.ModifyVmHostNameMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class RenameInstanceFlowCommand extends BaseWorkFlowCommand {
	@Value("${gcloud.controller.vm.mustRename: true}")
	private boolean mustRename;
	
	@Autowired
	private MessageBus bus;

	@Override
	protected Object process() throws Exception {
		ModifyVmHostNameFlowCommandReq req = (ModifyVmHostNameFlowCommandReq)getReqParams();
		
		ModifyVmHostNameMsg modifyMsg = new ModifyVmHostNameMsg();
    	modifyMsg.setInstanceId(req.getInstanceId());
    	modifyMsg.setServiceId(MessageUtil.computeServiceId(req.getCreateHost()));
    	modifyMsg.setHostName(StringUtils.isBlank(req.getHostName())?GenIDUtil.generateShortUuidStrengthen(req.getInstanceId()):req.getHostName());
    	modifyMsg.setTaskId(getTaskId());
        bus.send(modifyMsg);
		
		return null;
	}
	
	@Override
	public boolean judgeExecute() {
		ModifyVmHostNameFlowCommandReq req = (ModifyVmHostNameFlowCommandReq)getReqParams();
		if( req.isIsoCreate() || (StringUtils.isBlank(req.getHostName()) && !mustRename)) {
			return false;
		}
		return true;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return ModifyVmHostNameFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}
	
	@Override
	public int getTimeOut() {
		return 660;
	}

}