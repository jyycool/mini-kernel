package com.gcloud.controller.compute.handler.node.vm.base;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.msg.node.vm.base.JoinDomainReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class JoinDomainReplyHandler extends AsyncMessageHandler<JoinDomainReplyMsg>{
	
	@Autowired
	private InstanceDao vmInstanceDao;
	@Override
	public void handle(JoinDomainReplyMsg msg) {
		// TODO Auto-generated method stub
		log.debug("JoinDomainReplyHandler" + msg.getSuccess());
		if(msg.getSuccess()) {
			VmInstance instance= vmInstanceDao.getById(msg.getInstanceId());
			if(instance!=null) {
				List<String> fields = new ArrayList<>();
				fields.add(instance.updateDomain(msg.getDomain()));
				vmInstanceDao.update(instance, fields);
			}
		}
	}

}