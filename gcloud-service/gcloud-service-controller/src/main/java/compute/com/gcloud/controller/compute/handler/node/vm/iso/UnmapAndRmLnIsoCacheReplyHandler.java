package com.gcloud.controller.compute.handler.node.vm.iso;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.dao.IsoMapInfoDao;
import com.gcloud.controller.image.entity.IsoMapInfo;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.enums.IsoMapStatus;
import com.gcloud.header.compute.msg.node.vm.iso.UnmapAndRmLnIsoCacheReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Handler
public class UnmapAndRmLnIsoCacheReplyHandler extends AsyncMessageHandler<UnmapAndRmLnIsoCacheReplyMsg>{
	@Autowired
    private IsoMapInfoDao isoMapInfoDao;
	
	@Override
	public void handle(UnmapAndRmLnIsoCacheReplyMsg msg) {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("isoId", msg.getIsoId());
		props.put("hostname", msg.getHostname());
		IsoMapInfo isoMap = isoMapInfoDao.findUniqueByProperties(props);
		if(msg.getSuccess()) {
			isoMapInfoDao.delete(isoMap);
		} else {
			isoMap.setStatus(IsoMapStatus.MAPPED.name());
			isoMapInfoDao.update(isoMap);
		}
	}

}