package com.gcloud.controller.compute.handler.api.vm.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.model.vm.DetailInstanceTypeParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.DetailInstanceType;
import com.gcloud.header.compute.msg.api.vm.base.ApiDetailInstanceTypeMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiDetailInstanceTypeReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "DetailInstanceType", name = "实例类型详情")
public class ApiDetailInstanceTypeHandler extends MessageHandler<ApiDetailInstanceTypeMsg, ApiDetailInstanceTypeReplyMsg>{

	@Autowired
	private IVmBaseService vmBaseService;
	
	@Override
	public ApiDetailInstanceTypeReplyMsg handle(ApiDetailInstanceTypeMsg msg) throws GCloudException {
		DetailInstanceTypeParams params = BeanUtil.copyProperties(msg, DetailInstanceTypeParams.class);
		DetailInstanceType instanceType = vmBaseService.detailInstanceType(params, msg.getCurrentUser());
		
		ApiDetailInstanceTypeReplyMsg reply = new ApiDetailInstanceTypeReplyMsg();
		reply.setDetailInstanceType(instanceType);
		return reply;
	}

}