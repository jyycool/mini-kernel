package com.gcloud.controller.compute.workflow.model.trash;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DeleteInstanceWorkflowReq {

    private String instanceId;
    private Boolean inTask = true;  //默认是true
    private Boolean deleteNotExist = false; //如果不存在，是否执行�? true不执行，false执行。（会抛错找不到云服务器的错误�?�）

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public Boolean getInTask() {
        return inTask;
    }

    public void setInTask(Boolean inTask) {
        this.inTask = inTask;
    }

    public Boolean getDeleteNotExist() {
        return deleteNotExist;
    }

    public void setDeleteNotExist(Boolean deleteNotExist) {
        this.deleteNotExist = deleteNotExist;
    }
}