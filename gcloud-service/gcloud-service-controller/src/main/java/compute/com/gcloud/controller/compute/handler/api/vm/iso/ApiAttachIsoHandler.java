package com.gcloud.controller.compute.handler.api.vm.iso;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.iso.AttachIsoInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.iso.AttachIsoWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.iso.AttachIsoWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.compute.msg.api.vm.iso.ApiAttachIsoMsg;
import com.gcloud.header.compute.msg.api.vm.iso.ApiAttachIsoReplyMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@LongTask
@GcLog(isMultiLog = true, taskExpect = "挂载光盘")
@ApiHandler(module = Module.ECS, action = "AttachIso",name="挂载光盘")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class ApiAttachIsoHandler extends BaseWorkFlowHandler<ApiAttachIsoMsg, ApiAttachIsoReplyMsg>{

	@Override
	public Object preProcess(ApiAttachIsoMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public ApiAttachIsoReplyMsg process(ApiAttachIsoMsg msg) throws GCloudException {
		ApiAttachIsoReplyMsg replyMessage = new ApiAttachIsoReplyMsg();
		AttachIsoInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), AttachIsoInitFlowCommandRes.class);
		replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getIsoId())
				.objectName(CacheContainer.getInstance().getString(CacheType.ISO_NAME, msg.getIsoId())).expect("挂载光盘").build());

        return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return AttachIsoWorkflow.class;
	}
	@Override
	public Object initParams(ApiAttachIsoMsg msg) {
		AttachIsoWorkflowReq req = new AttachIsoWorkflowReq();
		req = BeanUtil.copyProperties(msg, AttachIsoWorkflowReq.class);
		req.setCreateVmTask(false);
		return req;
	}
}