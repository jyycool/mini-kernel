package com.gcloud.controller.compute.handler.node.vm.base;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.compute.msg.node.vm.base.StopInstanceReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class StopInstanceReplyHandler extends AsyncMessageHandler<StopInstanceReplyMsg>{

	@Autowired
	private InstanceDao vmInstanceDao;


	@Override
	public void handle(StopInstanceReplyMsg msg) {
		VmInstance vm = vmInstanceDao.getById(msg.getInstanceId());
		List<String> fields = new ArrayList<>();
		fields.add(vm.updateStepState(null));
		if(msg.getSuccess()){
			fields.add(vm.updateState(VmState.STOPPED.value()));
			vmInstanceDao.update(vm, fields);

			if(msg.getHandleResource() != null && msg.getHandleResource()){
                //关机成功，释放资�?
                Dispatcher.dispatcher().release(vm.getHostname(), vm.getCore(), vm.getMemory());
            }

		}else{
		    //已经不是�?机，也需要释放资�?
		    if(!VmState.RUNNING.value().equals(msg.getCurrentState()) && msg.getHandleResource() != null && msg.getHandleResource()){
                try{
                    Dispatcher.dispatcher().release(vm.getHostname(), vm.getCore(), vm.getMemory());
                }catch (Exception ex){
                    log.error("释放资源是吧", ex);
                }
            }

			fields.add(vm.updateState(msg.getCurrentState()));
			vmInstanceDao.update(vm, fields);
		}

	}
}