package com.gcloud.controller.compute.workflow.vm.base;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.core.workflow.core.BaseWorkFlows;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
public class ShutdownVmsFlow extends BaseWorkFlows {
	public String getBatchFiled() {
		return "instanceIds";
	}

	@Override
	public String getFlowTypeCode() {
		// TODO Auto-generated method stub
		return "ShutdownVmsFlow";
	}

	@Override
	protected Class<?> getReqParamClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object preProcess() {
		return null;
	}

	@Override
	public void process() {
		// TODO Auto-generated method stub
		
	}

}