package com.gcloud.controller.compute.workflow.model.iso;

import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CleanIsoFileFlowCommandReq {
	private String instanceId;
	private String hostname;
	private VmCdromDetail vmCdromDetail;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public VmCdromDetail getVmCdromDetail() {
		return vmCdromDetail;
	}
	public void setVmCdromDetail(VmCdromDetail vmCdromDetail) {
		this.vmCdromDetail = vmCdromDetail;
	}
}