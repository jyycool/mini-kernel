package com.gcloud.controller.compute.handler.api.vm.base;

import com.gcloud.controller.compute.workflow.model.vm.StopInstanceCommandRes;
import com.gcloud.controller.compute.workflow.vm.base.ShutdownVmsFlow;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.ApiStopVmsMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiStopVmsReplyMsg;
import com.gcloud.header.log.model.Task;

import java.util.List;
import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.VM, subModule = SubModule.VM, action = "shutdown", name = "关闭云服务器")
@LongTask
@GcLog(isMultiLog = true, taskExpect = "关闭云服务器")
public class ShudownVmsHandler extends BaseWorkFlowHandler<ApiStopVmsMsg, ApiStopVmsReplyMsg> {

    @Override
    public Object preProcess(ApiStopVmsMsg msg) throws GCloudException {
        return null;
    }

    @Override
    public ApiStopVmsReplyMsg process(ApiStopVmsMsg msg) throws GCloudException {
        //记录操作日志，任务流无论单操作还是批�? 都用这种方式记录操作日志
        List<StopInstanceCommandRes> firstRess = getFlowTaskFirstStepRes(msg.getTaskId(), StopInstanceCommandRes.class);

        for (StopInstanceCommandRes res : firstRess) {
            String except = String.format("关闭云服务器成功");
            msg.getTasks().add(Task.builder().taskId("").objectId("").objectName("").expect(except).build());
        }

        ApiStopVmsReplyMsg reply = new ApiStopVmsReplyMsg();
        reply.setRequestId(UUID.randomUUID().toString());

        return reply;
    }

    @Override
    public Class getWorkflowClass() {
        return ShutdownVmsFlow.class;
    }

}