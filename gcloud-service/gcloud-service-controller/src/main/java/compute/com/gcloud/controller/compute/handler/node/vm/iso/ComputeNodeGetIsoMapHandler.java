package com.gcloud.controller.compute.handler.node.vm.iso;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.dao.IsoMapInfoDao;
import com.gcloud.controller.image.entity.IsoMapInfo;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.iso.ComputeNodeGetIsoMapMsg;
import com.gcloud.header.compute.msg.node.vm.iso.ComputeNodeGetIsoMapReplyMsg;
import com.gcloud.header.compute.msg.node.vm.model.IsoMapItem;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class ComputeNodeGetIsoMapHandler extends AsyncMessageHandler<ComputeNodeGetIsoMapMsg>{
	@Autowired
    private IsoMapInfoDao isoMapInfoDao;
	
	@Autowired
    private MessageBus bus;
	
	@Override
	public void handle(ComputeNodeGetIsoMapMsg msg) {
		log.debug("ComputeNodeGetIsoMapHandler start");
		List<IsoMapInfo> isoMaps = isoMapInfoDao.findByProperty("hostname", msg.getHostname());
		
		if(isoMaps.size()==0) {
			log.debug("ComputeNodeGetIsoMapHandler iso map size=0");
			return;	
		}

		ComputeNodeGetIsoMapReplyMsg reply = new ComputeNodeGetIsoMapReplyMsg();
		reply.setServiceId(MessageUtil.computeServiceId(msg.getHostname()));
		
		List<IsoMapItem> isoMapItems = isoMaps.stream().map(item -> {
			IsoMapItem bean = new IsoMapItem();
		    bean.setIsoId(item.getIsoId());
		    bean.setLnPath(item.getLnPath());
		    bean.setPoolName(item.getPoolName());
		    return bean;
		}).collect(Collectors.toList());
		reply.setIsoMapItems(isoMapItems);
		bus.send(reply);
	}

}