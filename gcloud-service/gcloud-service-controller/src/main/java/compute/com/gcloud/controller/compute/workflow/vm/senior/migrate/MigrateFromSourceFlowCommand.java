package com.gcloud.controller.compute.workflow.vm.senior.migrate;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateFromSourceFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateFromSourceFlowCommandRes;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateFromSourceMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class MigrateFromSourceFlowCommand extends BaseWorkFlowCommand{

	@Autowired
    private MessageBus bus;
	
	@Override
	protected Object process() throws Exception {
		log.debug("MigrateFromSourceFlowCommand start");
		MigrateFromSourceFlowCommandReq req = (MigrateFromSourceFlowCommandReq) getReqParams();
		String instanceId = req.getInstanceId();
		String beginStatus = req.getBeginStatus();
		
		if(StringUtils.isBlank(instanceId)) {
			log.error("::实例ID不能为空");
			throw new GCloudException("::实例ID不能为空");
		}
		
		//目标节点信息
		Node targetNode = RedisNodesUtil.getComputeNodeByHostName(req.getTargetHostName());
		if(null == targetNode) {
			String message = String.format("找不到[%s]节点", req.getTargetHostName());
			log.error("::" + message);
			throw new GCloudException("::" + message);
		}
		String targetNodeIp = targetNode.getNodeIp();
		
		Node sourceNode = RedisNodesUtil.getComputeNodeByHostName(req.getSourceHostName());
		if(null == sourceNode) {
			String message = String.format("找不到[%s]节点", req.getSourceHostName());
			log.error("::" + message);
			throw new GCloudException("::" + message);
		}
		String sourceNodeIp = sourceNode.getNodeIp();
		
		//到源节点执行迁移命令
		MigrateFromSourceMsg msg = new MigrateFromSourceMsg();
		msg.setInstanceId(instanceId);
		msg.setTargetIp(targetNodeIp);
		msg.setSourceIp(sourceNodeIp);
		msg.setTargetHostName(req.getTargetHostName());
		msg.setBeginStatus(beginStatus);
		msg.setServiceId(MessageUtil.computeServiceId(req.getSourceHostName()));
		msg.setTaskId(getTaskId());
		bus.send(msg);
		
		MigrateFromSourceFlowCommandRes res = new MigrateFromSourceFlowCommandRes();
		res.setSourceIp(sourceNodeIp);
		res.setTargetIp(targetNodeIp);
		log.debug("MigrateFromSourceFlowCommand end");
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return MigrateFromSourceFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return MigrateFromSourceFlowCommandRes.class;
	}

}