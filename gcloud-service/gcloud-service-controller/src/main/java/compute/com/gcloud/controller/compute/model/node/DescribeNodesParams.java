package com.gcloud.controller.compute.model.node;

import com.gcloud.common.model.PageParams;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeNodesParams extends PageParams{
	private String zoneId;
	private Boolean zone;
	private String key;
	private Integer state;
	private Boolean resource;
	private Boolean includeNoZone;

	public Boolean getIncludeNoZone() {
		return includeNoZone;
	}

	public void setIncludeNoZone(Boolean includeNoZone) {
		this.includeNoZone = includeNoZone;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Boolean getZone() {
		return zone;
	}

	public void setZone(Boolean zone) {
		this.zone = zone;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Boolean getResource() {
		return resource;
	}

	public void setResource(Boolean resource) {
		this.resource = resource;
	}
}