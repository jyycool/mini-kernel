package com.gcloud.controller.compute.handler.api.vm.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.handler.api.model.DescribeInstanceTypesParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.InstanceTypeItemType;
import com.gcloud.header.compute.msg.api.vm.base.ApiDescribeInstanceTypesMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiDescribeInstanceTypesReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "DescribeInstanceTypes", name = "实例类型列表")
public class ApiDescribeInstanceTypesHandler extends MessageHandler<ApiDescribeInstanceTypesMsg, ApiDescribeInstanceTypesReplyMsg> {

	@Autowired
	private IVmBaseService vmBaseService;

	@Override
	public ApiDescribeInstanceTypesReplyMsg handle(ApiDescribeInstanceTypesMsg msg) throws GCloudException {

		DescribeInstanceTypesParams params = BeanUtil.copyProperties(msg, DescribeInstanceTypesParams.class);
		PageResult<InstanceTypeItemType> response = vmBaseService.describeInstanceTypes(params);
        for (InstanceTypeItemType type : response.getList()) {
            type.setMemorySize(Math.ceil(type.getMemorySize() / 1024));
        }
		ApiDescribeInstanceTypesReplyMsg replyMsg = new ApiDescribeInstanceTypesReplyMsg();
		replyMsg.init(response);
		return replyMsg;
	}
}