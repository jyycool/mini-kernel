package com.gcloud.controller.compute.workflow.model.vm;

import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateAttachDataVolumeWorkflowReq {
    private String instanceId;
    private DiskInfo repeatParams;
    private CurrentUser createUser;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public DiskInfo getRepeatParams() {
        return repeatParams;
    }

    public void setRepeatParams(DiskInfo repeatParams) {
        this.repeatParams = repeatParams;
    }

    public CurrentUser getCreateUser() {
        return createUser;
    }

    public void setCreateUser(CurrentUser createUser) {
        this.createUser = createUser;
    }
}