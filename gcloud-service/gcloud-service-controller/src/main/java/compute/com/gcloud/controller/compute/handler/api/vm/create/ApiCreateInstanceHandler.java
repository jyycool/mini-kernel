package com.gcloud.controller.compute.handler.api.vm.create;

import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceFlowInitCommandRes;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.create.CreateInstanceWorkflow;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.compute.msg.api.vm.create.ApiCreateInstanceMsg;
import com.gcloud.header.compute.msg.api.vm.create.ApiCreateInstanceReplyMsg;
import com.gcloud.header.log.model.Task;

import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@LongTask
@GcLog(isMultiLog = true, taskExpect = "创建云服务器")
@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "CreateInstance")
public class ApiCreateInstanceHandler extends BaseWorkFlowHandler<ApiCreateInstanceMsg, ApiCreateInstanceReplyMsg> {

	@Override
	public ApiCreateInstanceReplyMsg process(ApiCreateInstanceMsg msg) throws GCloudException {
		ApiCreateInstanceReplyMsg reply = new ApiCreateInstanceReplyMsg();
		// 记录操作日志，任务流无论单操作还是批�? 都用这种方式记录操作日志
		CreateInstanceFlowInitCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), CreateInstanceFlowInitCommandRes.class);
		
//		String except = String.format("创建云服务器[%s]成功", res.getInstanceId());
		String except = String.format("创建云服务器[%s]成功", msg.getInstanceName());
		reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getInstanceId()).objectName(res.getInstanceName()).expect(except).errorCode(res.getErrorMsg()).build());
		reply.setInstanceId(res.getInstanceId());

		String requestId = UUID.randomUUID().toString();
		reply.setRequestId(requestId);
		reply.setSuccess(true);
		return reply;
	}

	@Override
	public Class getWorkflowClass() {
		return CreateInstanceWorkflow.class;
	}

	@Override
	public Object preProcess(ApiCreateInstanceMsg msg) throws GCloudException {
		return null;
	}

    @Override
    public Object initParams(ApiCreateInstanceMsg msg) {
        CreateInstanceWorkflowReq req = BeanUtil.copyProperties(msg, CreateInstanceWorkflowReq.class);
        req.setSubnetId(msg.getvSwitchId());
        req.setIpAddress(msg.getPrivateIpAddress());
        req.setInTask(false);
        return req;
    }
}