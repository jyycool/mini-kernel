package com.gcloud.controller.compute.handler.api.vm.zone;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.zone.ApiGetZoneComputeNodesMsg;
import com.gcloud.header.compute.msg.api.vm.zone.ApiGetZoneComputeNodesReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "GetZoneComputeNodes", name = "可用区节点名列表")
public class ApiGetZoneComputeNodesHandler extends MessageHandler<ApiGetZoneComputeNodesMsg, ApiGetZoneComputeNodesReplyMsg>{
	@Autowired
    private IVmZoneService zoneService;
	
	@Override
	public ApiGetZoneComputeNodesReplyMsg handle(ApiGetZoneComputeNodesMsg msg) throws GCloudException {
		
		ApiGetZoneComputeNodesReplyMsg reply = new ApiGetZoneComputeNodesReplyMsg();
		reply.setRes(zoneService.getZoneHostNames(msg.getZoneId()));
		return reply;
	}
}