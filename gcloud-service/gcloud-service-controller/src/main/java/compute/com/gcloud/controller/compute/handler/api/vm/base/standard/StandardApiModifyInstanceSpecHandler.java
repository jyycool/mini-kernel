package com.gcloud.controller.compute.handler.api.vm.base.standard;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.vm.ModifyInstanceSpecInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.vm.base.ModifyInstanceSpecWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiModifyInstanceSpecMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiModifyInstanceSpecReplyMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "ModifyInstanceSpec", versions = {ApiVersion.Standard})
@LongTask
@GcLog(taskExpect = "修改实例规格")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class StandardApiModifyInstanceSpecHandler extends BaseWorkFlowHandler<StandardApiModifyInstanceSpecMsg, StandardApiModifyInstanceSpecReplyMsg>{

	@Override
	public Object preProcess(StandardApiModifyInstanceSpecMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public StandardApiModifyInstanceSpecReplyMsg process(StandardApiModifyInstanceSpecMsg msg) throws GCloudException {
		ModifyInstanceSpecInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), ModifyInstanceSpecInitFlowCommandRes.class);

		msg.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId()).expect("修改实例规格").build());

		StandardApiModifyInstanceSpecReplyMsg replyMessage = new StandardApiModifyInstanceSpecReplyMsg();
		replyMessage.setTaskId(res.getTaskId());
		return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return ModifyInstanceSpecWorkflow.class;
	}

}