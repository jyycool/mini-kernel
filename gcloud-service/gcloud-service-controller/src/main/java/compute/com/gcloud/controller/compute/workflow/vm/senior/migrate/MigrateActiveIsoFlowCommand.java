package com.gcloud.controller.compute.workflow.vm.senior.migrate;

import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateActiveIsoFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateActiveIsoFlowCommandRes;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateActiveIsoMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class MigrateActiveIsoFlowCommand extends BaseWorkFlowCommand{

	@Autowired
	private MessageBus bus;
	@Override
	protected Object process() throws Exception {

		MigrateActiveIsoFlowCommandReq req = (MigrateActiveIsoFlowCommandReq)getReqParams();
		MigrateActiveIsoMsg msg = new MigrateActiveIsoMsg();
		msg.setIsoId(req.getRepeatParams().getIsoId());
		msg.setServiceId(MessageUtil.computeServiceId(req.getTargetHostName()));
		msg.setTaskId(getTaskId());
		bus.send(msg);

		return null;
	}

	@Override
	public boolean judgeExecute() {
		MigrateActiveIsoFlowCommandReq req = (MigrateActiveIsoFlowCommandReq)getReqParams();
		return req.getRepeatParams() != null;
	}

	@Override
	protected Object rollback() throws Exception {
		

		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return MigrateActiveIsoFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return MigrateActiveIsoFlowCommandRes.class;
	}

}