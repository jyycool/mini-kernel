package com.gcloud.controller.compute.workflow.model.vm;

import com.gcloud.core.workflow.model.WorkflowFirstStepResException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ModifyInstanceSpecInitFlowCommandRes extends WorkflowFirstStepResException{

	private String taskId;
	private String instanceId;
	private Integer cpu;
	private Integer memory;
	private String hostName;
    private Integer orgCpu;
    private Integer orgMemory;

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getCpu() {
		return cpu;
	}

	public void setCpu(Integer cpu) {
		this.cpu = cpu;
	}

	public Integer getMemory() {
		return memory;
	}

	public void setMemory(Integer memory) {
		this.memory = memory;
	}

    public Integer getOrgCpu() {
        return orgCpu;
    }

    public void setOrgCpu(Integer orgCpu) {
        this.orgCpu = orgCpu;
    }

    public Integer getOrgMemory() {
        return orgMemory;
    }

    public void setOrgMemory(Integer orgMemory) {
        this.orgMemory = orgMemory;
    }
}