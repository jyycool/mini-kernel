package com.gcloud.controller.compute.model.vm;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class VmImageInfo {
    private String imageStorageType;
    private String imagePoolId;
    private String imagePath;
    private String imageId;

    public String getImageStorageType() {
        return imageStorageType;
    }

    public void setImageStorageType(String imageStorageType) {
        this.imageStorageType = imageStorageType;
    }

    public String getImagePoolId() {
        return imagePoolId;
    }

    public void setImagePoolId(String imagePoolId) {
        this.imagePoolId = imagePoolId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}