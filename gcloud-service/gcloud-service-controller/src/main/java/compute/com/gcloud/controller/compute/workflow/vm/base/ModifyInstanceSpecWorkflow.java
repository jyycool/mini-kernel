package com.gcloud.controller.compute.workflow.vm.base;

import com.gcloud.controller.compute.workflow.model.storage.ModifyInstanceSpecWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class ModifyInstanceSpecWorkflow extends BaseWorkFlows {

	@Override
	public String getFlowTypeCode() {
		return "ModifyInstanceSpecWorkflow";
	}

	@Override
	public void process() {

	}

	@Override
	protected Class<?> getReqParamClass() {
		return ModifyInstanceSpecWorkflowReq.class;
	}

	@Override
	public Object preProcess() {
		return null;
	}

}