package com.gcloud.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Configuration
public class ControllerConfig {
	@Value("${gcloud.service.controller}")
	public String queueName;
	
	@Value("${gcloud.service.controller}"+"_async")
	public String asyncQueue;
    @Bean
    public Queue controllerQueue() {
    	System.out.println("init contoller queue:"+queueName);
        return new Queue(queueName);
    }
    
    @Bean
    public Queue controller2Queue() {
    	System.out.println("init contoller async queue:"+asyncQueue);
        return new Queue(asyncQueue);
    }
    
}