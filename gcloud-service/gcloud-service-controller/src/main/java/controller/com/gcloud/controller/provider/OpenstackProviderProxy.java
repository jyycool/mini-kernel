package com.gcloud.controller.provider;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.identity.v3.Token;
import org.openstack4j.openstack.OSFactory;

import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public abstract class OpenstackProviderProxy {
	private Provider provider;
	public void setProvider(Provider provider){
		this.provider=provider;
	}
	private Token token;
	public OSClientV3 getClient(){
		if(token==null||token.getExpires().before(new Date())){
			OSClientV3 client=OSFactory.builderV3().endpoint(provider.getAuthUrl())
					.credentials(provider.getUsername(), provider.getPassword(), Identifier.byName("Default"))

					.scopeToProject(Identifier.byName( provider.getProject()), Identifier.byName("Default"))
					.authenticate();
			this.token=client.getToken();
			return client;
		}else{
			return OSFactory.clientFromToken(token);
		}
	}
	
	public String getUrl() {
	    return this.provider.getUrl();
	}
	
}