package com.gcloud.controller;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ResourceProviderEntity {

    private Integer provider;
    private String providerRefId;

    public static final String PROVIDER = "provider";
    public static final String PROVIDER_REF_ID = "providerRefId";

    public Integer getProvider() {
        return provider;
    }

    public void setProvider(Integer provider) {
        this.provider = provider;
    }

    public String getProviderRefId() {
        return providerRefId;
    }

    public void setProviderRefId(String providerRefId) {
        this.providerRefId = providerRefId;
    }

    public String updateProvider(Integer provider) {
        this.setProvider(provider);
        return PROVIDER;
    }

    public String updateProviderRefId(String providerRefId) {
        this.setProviderRefId(providerRefId);
        return PROVIDER_REF_ID;
    }

}