package com.gcloud.controller.provider;

import com.gcloud.common.util.HttpClientUtil;
import com.gcloud.controller.monitor.model.Statistics;
import com.gcloud.controller.monitor.model.StatisticsPoint;
import com.gcloud.controller.monitor.model.StatisticsResource;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.GcloudConstants;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Component
public class MonitorProviderProxy {
	
	@Autowired
	private MonitorProvider provider;
	
	public Statistics statistics(Map<String, Object> params){
		String result = null;

		if(params == null){
			params = new HashMap<>();
		}

		//传入平台参数 = 3，对应监控中的gcloud8平台
		params.put("platformType", GcloudConstants.MONITOR_GCLOUD8_PLATFORM_TYPE);
		params.put("regionId", provider.getRegionId());
		params.put("manageType", "gcloud8");
		
		try{
			result = HttpClientUtil.doGet(this.provider.getUrl() +  "/monitor/Statistics", params);
        }catch (Exception ex){
            log.error("获取监控信息失败" + ex, ex);
            throw new GCloudException("::获取监控信息失败");
        }

		JSONObject json = JSONObject.fromObject(result);
		
		Map<String, Class> classMap = new HashMap<String, Class>();
		classMap.put("list", StatisticsResource.class);
		classMap.put("points", StatisticsPoint.class);
		Statistics data = (Statistics) JSONObject.toBean(json.getJSONObject("data"), Statistics.class, classMap);
		return data;
	}
	
}