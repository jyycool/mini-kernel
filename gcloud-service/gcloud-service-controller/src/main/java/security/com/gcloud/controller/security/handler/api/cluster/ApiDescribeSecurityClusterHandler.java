package com.gcloud.controller.security.handler.api.cluster;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.security.model.DescribeSecurityClusterParams;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.security.model.SecurityClusterType;
import com.gcloud.header.security.msg.api.cluster.ApiDescribeSecurityClusterMsg;
import com.gcloud.header.security.msg.api.cluster.ApiDescribeSecurityClusterReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "DescribeSecurityCluster", name = "安全集群列表")
public class ApiDescribeSecurityClusterHandler extends MessageHandler<ApiDescribeSecurityClusterMsg, ApiDescribeSecurityClusterReplyMsg> {

	@Autowired
	private ISecurityClusterService securityClusterService;
	
    @Override
    public ApiDescribeSecurityClusterReplyMsg handle(ApiDescribeSecurityClusterMsg msg) throws GCloudException {
    	DescribeSecurityClusterParams params = BeanUtil.copyProperties(msg, DescribeSecurityClusterParams.class);
    	PageResult<SecurityClusterType> response = securityClusterService.describeSecurityCluster(params, msg.getCurrentUser());
    	
    	ApiDescribeSecurityClusterReplyMsg reply = new ApiDescribeSecurityClusterReplyMsg();
    	reply.init(response);
        return reply;
    }
}