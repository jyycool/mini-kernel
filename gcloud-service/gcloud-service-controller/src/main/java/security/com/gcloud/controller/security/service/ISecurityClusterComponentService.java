package com.gcloud.controller.security.service;

import com.gcloud.controller.security.entity.SecurityClusterComponent;
import com.gcloud.controller.security.enums.SecurityClusterComponentObjectType;
import com.gcloud.controller.security.enums.SecurityComponent;
import com.gcloud.controller.security.model.ComputeClusterCreateObjectInfo;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.security.msg.model.CreateClusterDcParams;
import com.gcloud.header.security.msg.model.CreateClusterInfoParams;
import com.gcloud.header.security.msg.model.CreateClusterObjectParams;
import com.gcloud.header.security.msg.model.CreateClusterVmParams;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface ISecurityClusterComponentService {

    ComputeClusterCreateObjectInfo createClusterComponentVm(String clusterId, String clusterName, CurrentUser loginUser, CreateClusterInfoParams clusterInfoParams, CreateClusterVmParams vmParams, SecurityComponent type, Boolean isHa);
    ComputeClusterCreateObjectInfo createClusterComponentDc(String clusterId, String clusterName, CurrentUser loginUser, CreateClusterInfoParams clusterInfoParams, CreateClusterDcParams dcParams, SecurityComponent type, Boolean isHa);
    Map<SecurityClusterComponentObjectType, Map<String, CreateClusterObjectParams>> getComponentConfig(List<SecurityClusterComponent> components, CreateClusterInfoParams createInfo);

}