package com.gcloud.controller.security.entity;


import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_security_cluster_ovsbridge")
public class SecurityClusterOvsBridge {

    @ID
    private String id;
    private String clusterId;
    private String ovsBridgeId;
    private Boolean ha;

    public static final String ID = "id";
    public static final String CLUSTER_ID = "clusterId";
    public static final String OVS_BRIDGE_ID = "ovsBridgeId";
    public static final String HA = "ha";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    public Boolean getHa() {
        return ha;
    }

    public void setHa(Boolean ha) {
        this.ha = ha;
    }

    public String getOvsBridgeId() {
        return ovsBridgeId;
    }

    public void setOvsBridgeId(String ovsBridgeId) {
        this.ovsBridgeId = ovsBridgeId;
    }

    public String updateId(String id) {
        this.setId(id);
        return ID;
    }

    public String updateClusterId(String clusterId) {
        this.setClusterId(clusterId);
        return CLUSTER_ID;
    }

    public String updateOvsBridgeId(String ovsBridgeId) {
        this.setOvsBridgeId(ovsBridgeId);
        return OVS_BRIDGE_ID;
    }

    public String updateHa(Boolean ha) {
        this.setHa(ha);
        return HA;
    }
}