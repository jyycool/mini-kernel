package com.gcloud.controller.security.handler.api.cluster;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.security.model.ModifySecurityClusterParams;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.security.msg.api.cluster.ApiModifySecurityClusterMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "ModifySecurityCluster", name = "修改安全集群")
public class ApiModifySecurityClusterHandler extends MessageHandler<ApiModifySecurityClusterMsg, ApiReplyMessage> {

	@Autowired
	private ISecurityClusterService securityClusterService;
	
    @Override
    public ApiReplyMessage handle(ApiModifySecurityClusterMsg msg) throws GCloudException {
    	ModifySecurityClusterParams params = BeanUtil.copyProperties(msg, ModifySecurityClusterParams.class);
    	securityClusterService.modifySecurityCluster(params, msg.getCurrentUser());
    	
    	ApiReplyMessage reply = new ApiReplyMessage();
        return reply;
    }
}