package com.gcloud.controller.log.quartz;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.log.dao.LogDao;
import com.gcloud.controller.log.prop.LogProp;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
@QuartzTimer(fixedRate=24* 60 * 60 * 1000L)
public class FileLogQuartz extends GcloudJobBean{
	@Autowired
    private LogDao logDao;
	
	@Autowired
	private LogProp prop;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if(prop.isEnableFileLog()) {
				log.debug("日志记录归档定时任务�?�?");
				logDao.fileLog(prop.getLogKeptDays(), prop.getFileLogKeptDays());
				log.debug("日志记录归档定时任务结束");
			}
		} catch(Exception ex) {
			log.error("日志记录归档任务失败", ex);
		}
	}

}