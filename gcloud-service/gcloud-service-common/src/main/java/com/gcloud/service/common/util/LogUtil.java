package com.gcloud.service.common.util;

import com.gcloud.core.exception.GCloudException;
import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class LogUtil {
	
	public static void handleLog(String[] cmd, int res, String errorCode) throws GCloudException {
		if (res != 0) {
			String commandString = "";
			for (String part : cmd) {
				commandString += part + " ";
			}
			
			Throwable ex = new Throwable();
			log.error(commandString, ex);
			throw new GCloudException(errorCode);
		}
	}
}