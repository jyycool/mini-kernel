package com.gcloud.service.common.util;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.common.util.StringUtils;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.msg.node.vm.model.IsoMapItem;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
public class RbdUtil {
	public static String rbdmap(String resourceId, String poolName, String errorCode) throws GCloudException {
		String[] cmd = new String[] { "rbd", "map", resourceId, "--pool", poolName };
		String[] res = SystemUtil.runAndGetCodeAndValue(cmd);
		LogUtil.handleLog(cmd, Integer.parseInt(res[0]), errorCode);
		return res[1].replace("\n", "");
	}
	
	public static void rbdUnmap(String mapPath, String errorCode) throws GCloudException {
		String[] cmd = new String[] { "rbd", "unmap", mapPath };
		int res = SystemUtil.runAndGetCode(cmd);
		LogUtil.handleLog(cmd, res, errorCode);
	}
	
	public static List<IsoMapItem> getRbdmaps(){
		List<IsoMapItem> result = new ArrayList<IsoMapItem>();
		String res = SystemUtil.run(new String[]{"rbd", "showmapped"});

        if(StringUtils.isBlank(res)){
            return result;
        }
        
        String[] records = res.split("\\n");
		for(String record:records) {
			String[] items =  record.trim().split("\\s+");
			IsoMapItem item = new IsoMapItem();
			item.setIsoId(items[2]);
			item.setPoolName(items[1]);
			item.setMapPath(items[4]);
			
			result.add(item);
		}
        return result;
	}
	
	public static boolean snapIsProtected(String operator, String poolName, String volumeName, String snapId) {
		String res = SystemUtil.run(new String[]{"rbd", "--user", operator, "info", String.format("%s/%s@%s", poolName, volumeName, snapId)});

        if(StringUtils.isBlank(res)){
            throw new GCloudException("::获取rbd info信息失败");
        }
        String[] records = res.split("\\n");
		for(String record:records) {
			if(record.contains("protected:")) {
				if(record.contains("protected: False")) {
					return false;
				}else {
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isVolumeExist(String operator, String poolName, String volumeName) {
		String result = SystemUtil.run(new String[]{"rbd", "--user", operator, "ls", "-p", poolName});

        if(StringUtils.isBlank(result)){
        	return false;
        }
        String[] records = result.split("\\n");
		for(String record:records) {
			if(record.equals(volumeName)) {
				return true;
			}
		}
        return false;
	}
	
}