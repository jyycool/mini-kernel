#!/bin/bash
configPath=/var/lib/kolla/config_files/application.yml
temConfigPath=/var/lib/gcloud/temApplication.cfg
parsePath=/usr/share/gcloud/network/parseYaml.sh
insConfigName=gcloud.computeNode.instanceConfigPath
nodeIpName=gcloud.computeNode.nodeIp


insConfigPath=
nodeIp=

nodeInfoName="nodeinfo"
scriptPre="start_net"

#init_params

insConfigName="$insConfigName="
nodeIpName="$nodeIpName="

sh $parsePath $configPath '' > $temConfigPath

while read LINE
do
  if [[ $LINE = "$nodeIpName"* ]];then
     nodeIp=${LINE#*$nodeIpName}
  fi
  if [[ $LINE = "$insConfigName"* ]];then
     insConfigPath="${LINE#*$insConfigName}"
  fi
done < $temConfigPath

if [ -z "$nodeIp" ];then
  echo "node ip is null"
  exit 1
fi

if [ -z "$insConfigPath" ];then
  echo "config path is null"
  exit 1
fi



for instanceId in `ls $insConfigPath/$nodeIp`
  do
    if [ -d $insConfigPath/$nodeIp/$instanceId ];then
       instancePath="$insConfigPath/$nodeIp/$instanceId"
       for cfgFile in `ls $instancePath`
       do
          if [[ $cfgFile = "$scriptPre"* ]];then
             sh $instancePath/$cfgFile
          fi
       done
    fi
  done

rm -f $temConfigPath