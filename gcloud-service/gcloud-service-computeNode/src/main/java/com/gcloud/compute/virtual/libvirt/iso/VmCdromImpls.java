package com.gcloud.compute.virtual.libvirt.iso;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.compute.enums.DiskProtocol;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class VmCdromImpls {
	private static Map<DiskProtocol, IVmCdrom> impls = new HashMap<>();

    @PostConstruct
    public void init(){
        for(IVmCdrom cdrom : SpringUtil.getBeans(IVmCdrom.class)){
            impls.put(cdrom.disProtocol(), cdrom);
        }
    }

    public static IVmCdrom vmCdromImpl(DiskProtocol diskProtocol){
        return impls.get(diskProtocol);
    }
}