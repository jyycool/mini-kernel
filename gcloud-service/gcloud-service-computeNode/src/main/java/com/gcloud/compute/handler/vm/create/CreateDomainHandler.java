package com.gcloud.compute.handler.vm.create;

import com.gcloud.compute.service.vm.create.IVmCreateNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.create.CreateDomainMsg;
import com.gcloud.header.compute.msg.node.vm.create.CreateDomainReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class CreateDomainHandler extends AsyncMessageHandler<CreateDomainMsg> {
	@Autowired
	private IVmCreateNodeService vmCreateNodeService;

	@Autowired
	private MessageBus bus;

	@Override
	public void handle(CreateDomainMsg msg) {
		CreateDomainReplyMsg reply = new CreateDomainReplyMsg();
		reply.setServiceId(MessageUtil.controllerServiceId());
		reply.setSuccess(true);
		reply.setTaskId(msg.getTaskId());

		try {
			vmCreateNodeService.createDomain(msg.getInstanceId(), msg.getUserId());
		} catch (Exception e) {
			reply.setSuccess(false);
			reply.setErrorCode(e.getMessage());
		}

		bus.send(reply);
	}

}