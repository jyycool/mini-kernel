package com.gcloud.compute.handler.vm.iso;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.compute.service.vm.iso.IVmIsoNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.iso.ConfigIsoFileMsg;
import com.gcloud.header.compute.msg.node.vm.iso.ConfigIsoFileReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class ConfigIsoFileHandler extends AsyncMessageHandler<ConfigIsoFileMsg>{
	@Autowired
	private MessageBus bus;
	
	@Autowired
	private IVmIsoNodeService vmIsoNodeService;
	
	@Override
	public void handle(ConfigIsoFileMsg msg) {
		ConfigIsoFileReplyMsg replyMsg = msg.deriveMsg(ConfigIsoFileReplyMsg.class);
		replyMsg.setInstanceId(msg.getInstanceId());
		replyMsg.setIsoId(msg.getVmCdromDetail().getIsoId());
		replyMsg.setSuccess(false);
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		try {
			vmIsoNodeService.configIsoFile(msg.getInstanceId(),msg.getVmCdromDetail(), msg.getPlatform());
			replyMsg.setSuccess(true);
		} catch (Exception ex) {
			log.error("配置光盘文件错误", ex);
			replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "1010901::配置光盘文件错误"));
		}
		bus.send(replyMsg);
		
	}

}