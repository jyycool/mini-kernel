package com.gcloud.compute.handler.vm.senior.migrate;

import com.gcloud.compute.service.vm.network.IVmNetworkNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateAttachNetworkMsg;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateAttachNetworkReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class MigrateAttachNetworkHandler extends AsyncMessageHandler<MigrateAttachNetworkMsg>{

	@Autowired
    private MessageBus bus;
	
	@Autowired
	private IVmNetworkNodeService vmNetworkNodeService;
	
	@Override
	public void handle(MigrateAttachNetworkMsg msg) {
		log.debug("MigrateAttachNetworkHandler start");
		MigrateAttachNetworkReplyMsg replyMsg = msg.deriveMsg(MigrateAttachNetworkReplyMsg.class);

		replyMsg.setSuccess(false);
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		replyMsg.setTaskId(msg.getTaskId());
		try{
			vmNetworkNodeService.configNetEnv(msg.getInstanceId(), msg.getVmNetworkDetail());
			replyMsg.setSuccess(true);
		}catch (Exception ex){
			log.error("迁移配置目标节点网络信息失败", ex);
			replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::迁移配置目标节点网络信息失败"));
		}

		bus.send(replyMsg);
		log.debug("MigrateAttachNetworkHandler end");
	}

}