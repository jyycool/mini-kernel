package com.gcloud.compute.handler.vm.base;

import com.gcloud.compute.util.VmNodeUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.compute.msg.node.vm.base.CheckInstanceDomainStateMsg;
import com.gcloud.header.compute.msg.node.vm.base.CheckInstanceDomainStateReplyMsg;
import com.gcloud.service.common.compute.model.DomainInfo;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Handler
@Slf4j
public class CheckInstanceDomainStateHandler extends MessageHandler<CheckInstanceDomainStateMsg, CheckInstanceDomainStateReplyMsg>{
	
	@Override
	public CheckInstanceDomainStateReplyMsg handle(CheckInstanceDomainStateMsg msg) throws GCloudException {
		CheckInstanceDomainStateReplyMsg reply = msg.deriveMsg(CheckInstanceDomainStateReplyMsg.class);
		DomainInfo domInfo = VmNodeUtil.checkVm(msg.getInstanceId());
		if (domInfo == null) {
			throw new GCloudException("::云服务器不存�?");
		}
		reply.setState(domInfo.getGcState());
		return reply;
	}

}