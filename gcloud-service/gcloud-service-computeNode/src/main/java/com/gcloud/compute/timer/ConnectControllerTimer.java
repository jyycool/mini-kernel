package com.gcloud.compute.timer;

import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.node.ComputeNodeConnectMsg;
import com.gcloud.header.compute.msg.node.node.model.ComputeNodeInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Slf4j
public class ConnectControllerTimer {

    @Autowired
    private MessageBus bus;

    @Autowired
    private ComputeNodeProp prop;

    @Autowired
    private ComputeNodeInfo nodeInfo;

    @Scheduled(fixedDelayString = "${gcloud.computeNode.reportFrequency:30}" + "000")
    public void connect(){

        log.debug("ConnectControllerTimer begin connect to :" + MessageUtil.controllerServiceId());

        try {
            ComputeNodeConnectMsg msg = new ComputeNodeConnectMsg();
            msg.setServiceId(MessageUtil.controllerServiceId());
            msg.setComputeNodeInfo(nodeInfo);
            msg.setNodeTimeout(prop.getReportFrequency() * 2);
            bus.send(msg);

        } catch (Exception e) {
            log.error("连接控制器失�?", e);
        }

        log.debug("ConnectControllerTimer end");

    }

}