package com.gcloud.compute.timer;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.compute.lock.VmStartLock;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Slf4j
public class AddStartMapTimer {

    @Scheduled(fixedDelay = 2000L)
    public void check(){

        log.debug("AddStartMapTimer begin");

        try {

            while(true){
                String instanceId = VmStartLock.getFirst();
                if(!StringUtils.isBlank(instanceId)){
                    log.debug(String.format("【检测开机序列�?? �?机队列到�?机池, 获取到�??%s�? ", instanceId));
                    if(VmStartLock.addConcurrentNum(instanceId)){
                        VmStartLock.removeStartList(instanceId);
                    }else{
                        break;
                    }
                }else{
                    break;
                }
            }

        } catch (Exception e) {
            log.error("添加到startMap失败",e);
        }

        log.debug("AddStartMapTimer end");

    }

}