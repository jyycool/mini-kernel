package com.gcloud.compute.handler.vm.senior.migrate;

import com.gcloud.compute.service.vm.senior.IVmSeniorNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateFromSourceMsg;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateFromSourceReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class MigrateFromSourceHandler extends AsyncMessageHandler<MigrateFromSourceMsg>{

	@Autowired
	private IVmSeniorNodeService vmSeniorNodeService;

	@Autowired
    private MessageBus bus;
	
	@Value("${spring.computeNode.migrateProtocol:TCP}")
	private String migrateProtocol;
	
	@Override
	public void handle(MigrateFromSourceMsg msg) {
		log.debug("MigrateFromSourceHandler start");
		MigrateFromSourceReplyMsg replyMsg = msg.deriveMsg(MigrateFromSourceReplyMsg.class);
		
		replyMsg.setSuccess(true);
		replyMsg.setTaskId(msg.getTaskId());
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		try{
			vmSeniorNodeService.migrateInstance("live", migrateProtocol, msg.getInstanceId(), msg.getSourceIp(), msg.getTargetIp(), msg.getTargetHostName(), false);
		}catch (Exception ex){
			log.error("迁移清理节点网络信息失败", ex);
			replyMsg.setSuccess(false);
			replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::迁移清理节点网络信息失败"));
		}


		bus.send(replyMsg);
		log.debug("MigrateFromSourceHandler end");
	}

}