package com.gcloud.compute.service.vm.trash.impl;

import com.gcloud.compute.cache.cache.VmInstancesCache;
import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.compute.service.vm.trash.IVmTrashNodeService;
import com.gcloud.compute.util.VmNodeUtil;
import com.gcloud.compute.virtual.IVmVirtual;
import com.gcloud.core.condition.ConditionalHypervisor;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.service.common.compute.model.DomainInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ConditionalHypervisor
@Slf4j
public class VmTrashNodeServiceKvmImpl implements IVmTrashNodeService {

    @Autowired
    private ComputeNodeProp prop;

    @Autowired
    private IVmVirtual vmVirtual;

    @Override
    public void cleanInstanceFile(String instanceId) {
        String configPath = VmNodeUtil.getInstanceConfigPath(prop.getNodeIp(), instanceId);
        File cfgPahFile = new File(configPath);
        if(cfgPahFile.exists()){
            try{
                FileUtils.deleteDirectory(cfgPahFile);
            }catch (Exception ex){
                log.error("删除文件失败", ex);
                throw new GCloudException("::删除文件失败");
            }

        }

    }

    @Override
    public void cleanInstanceInfo(String instanceId) {
        DomainInfo domInfo = VmNodeUtil.checkVm(instanceId);
        if(domInfo != null){
            vmVirtual.undefine(instanceId);
        }

        VmInstancesCache.remove(instanceId);
    }
}