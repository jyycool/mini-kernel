package com.gcloud.compute.handler.vm.base;

import com.gcloud.compute.service.vm.base.IVmBaseNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.base.DestroyInstanceIfExistMsg;
import com.gcloud.header.compute.msg.node.vm.base.DestroyInstanceReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
@Handler
public class DestroyInstanceIfExistHandler extends AsyncMessageHandler<DestroyInstanceIfExistMsg> {

    @Autowired
    private IVmBaseNodeService vmBaseNodeService;

    @Autowired
    private MessageBus bus;

    @Override
    public void handle(DestroyInstanceIfExistMsg msg) {
        DestroyInstanceReplyMsg replyMsg = msg.deriveMsg(DestroyInstanceReplyMsg.class);
        replyMsg.setServiceId(MessageUtil.controllerServiceId());
        replyMsg.setInstanceId(msg.getInstanceId());
        replyMsg.setSuccess(false);
        replyMsg.setHandleResource(msg.getHandleResource());
        try{
            vmBaseNodeService.destroyIfExist(msg.getInstanceId());
            replyMsg.setSuccess(true);
        }catch (Exception ex){
            log.error("云服务器destroy异常", ex);
            replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "1010801::云服务器destroy异常"));
            String vmState = vmBaseNodeService.vmGcloudState(msg.getInstanceId());
            replyMsg.setCurrentState(vmState);
        }
        bus.send(replyMsg);

    }
}