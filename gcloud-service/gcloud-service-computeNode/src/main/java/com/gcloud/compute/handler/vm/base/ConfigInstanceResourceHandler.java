package com.gcloud.compute.handler.vm.base;

import com.gcloud.compute.service.vm.base.IVmBaseNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.base.ConfigInstanceResourceMsg;
import com.gcloud.header.compute.msg.node.vm.base.ConfigInstanceResourceReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
@Handler
public class ConfigInstanceResourceHandler extends AsyncMessageHandler<ConfigInstanceResourceMsg> {

	@Autowired
	private IVmBaseNodeService vmBaseNodeService;

	@Autowired
	private MessageBus bus;

	@Override
	public void handle(ConfigInstanceResourceMsg msg) {
		msg.getServiceId();
		ConfigInstanceResourceReplyMsg replyMsg = msg.deriveMsg(ConfigInstanceResourceReplyMsg.class);
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		replyMsg.setSuccess(false);
		try {
			vmBaseNodeService.configInstanceResource(msg.getInstanceId(), msg.getCpu(), msg.getMemory(), msg.getOrgCpu(), msg.getOrgMemory());
			replyMsg.setSuccess(true);
		} catch (Exception ex) {
			log.error("修改云服务器配置异常", ex);
			replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "1011301::修改云服务器配置异常"));
		}
		bus.send(replyMsg);

	}
}