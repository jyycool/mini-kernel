package com.gcloud.compute.virtual.libvirt.iso;

import org.dom4j.Document;

import com.gcloud.header.compute.enums.DiskProtocol;
import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IVmCdrom {
	void addCdromDevice(Document doc, VmCdromDetail vmcdromDetail, boolean isCreateElement);
	DiskProtocol disProtocol();
}