package com.gcloud.image.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.FileFormat;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.image.prop.ImageNodeProp;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.util.RbdUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 镜像的后端存储为file，镜像分发到rbd
 *
 */
@Component
@Slf4j
public class FileStoreRbdDriver implements IImageStoreNodeDriver{
	
	@Autowired
	ImageNodeProp imageNodeProp;

	@Override
	public void downloadImage(String sourceFilePath, String imageId, String target, String imageResourceType) {
		if(RbdUtil.isVolumeExist(imageNodeProp.getCephOperator(), target, imageId)) {
			log.debug(String.format("%s池上已经存在�?%s", target, imageId));
			return;
		}
		if(imageResourceType.equals(ImageResourceType.IMAGE.value())) {
			QemuInfo info = DiskQemuImgUtil.info(sourceFilePath);
			if(!info.getFormat().equals(FileFormat.RAW.getValue())) {
				DiskQemuImgUtil.convert(sourceFilePath, String.format("rbd:%s/%s", target, imageId), info.getFormat(), FileFormat.RAW.getValue(), false);
				/*DiskQemuImgUtil.convert(sourceFilePath, sourceFilePath + ".raw", info.getFormat(), FileFormat.RAW.getValue(), false);
		        String[] copyCmd = new String[]{"rbd", "--user", imageNodeProp.getCephOperator(), "-p", target, "import", sourceFilePath + ".raw", "--image", imageId};
		        this.exec(copyCmd, String.format("::将raw格式镜像文件copy�?%s池上失败", target));
		        //删除临时文件
		        String[] deleteCmd = new String[]{"rm", "-f", sourceFilePath + ".raw"};
		        this.exec(deleteCmd, "::删除临时raw格式镜像文件失败");*/
			} else {
		        String[] copyCmd = new String[]{"rbd", "--user", imageNodeProp.getCephOperator(), "-p", target, "import", sourceFilePath, "--image", imageId};
		        this.exec(copyCmd, String.format("::将raw格式镜像文件copy�?%s池上失败", target));
			}
		} else {//iso
			// rbd --user gcloud -p images import /opt/gcloud/storage/isos/G-Cloud-7.0-auto-install.iso --image iso-003 --image-feature layering
			String[] copyCmd = new String[]{"rbd", "--user", imageNodeProp.getCephOperator(), "-p", target, "import", sourceFilePath , "--image", imageId, "--image-feature", "layering" };
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

	        this.exec(copyCmd, String.format("::将映像文件copy�?%s池上失败", target));
		}
	}
	
	private void exec(String[] cmd, String errorCode) {
		int res = SystemUtil.runAndGetCode(cmd);
		if(res != 0) {
			throw new GCloudException(errorCode);
		}
	}
}