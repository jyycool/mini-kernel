package com.gcloud.image.prop;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@ConfigurationProperties(prefix = "gcloud.image-node")
public class ImageNodeProp {
	@Value("${gcloud.image-node.controller:}")
	private String controller;
	
	@Value("${gcloud.image-node.imageCachedPath:/var/lib/gcloud/caches/images/}")
	private String imageCachedPath;
	
	@Value("${gcloud.image-node.isoCachedPath:/var/lib/gcloud/caches/isos/}")
	private String isoCachedPath;
	
	@Value("${gcloud.image-node.cephOperator:gcloud}")
	private String cephOperator;//ceph操作�?

	public String getCephOperator() {
		return cephOperator;
	}

	public void setCephOperator(String cephOperator) {
		this.cephOperator = cephOperator;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getImageCachedPath() {
		return imageCachedPath;
	}

	public void setImageCachedPath(String imageCachedPath) {
		this.imageCachedPath = imageCachedPath;
	}

	public String getIsoCachedPath() {
		return isoCachedPath;
	}

	public void setIsoCachedPath(String isoCachedPath) {
		this.isoCachedPath = isoCachedPath;
	}
	
}