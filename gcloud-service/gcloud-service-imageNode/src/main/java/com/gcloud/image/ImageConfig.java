package com.gcloud.image;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Configuration
@Slf4j
public class ImageConfig {
	@Value("${gcloud.service.imageNode}")
	public String syncQueueName;
	
	@Value("${gcloud.service.imageNode}"+"_async")
	public String asyncQueue;
	
	@Value("${gcloud.service.imageSchedule:image-schedule}")
	public String syncScheduleQueueName;
	
	@Value("${gcloud.service.imageSchedule:image-schedule}"+"_async")
	public String asyncScheduleQueue;
	
    @Bean
    public Queue imageSyncQueue() {
    	log.debug("init image sync queue:"+syncQueueName);
        return new Queue(syncQueueName);
    }
    
    @Bean
    public Queue imageAsyncQueue() {
    	log.debug("init image async queue:"+asyncQueue);
        return new Queue(asyncQueue);
    }
    
    @ConditionalOnExpression("${gcloud.image-node.schedule:false} == true")
    @Bean
    public Queue imageScheduleSyncQueue() {
    	log.debug("init image schedule sync queue:"+syncScheduleQueueName);
        return new Queue(syncScheduleQueueName);
    }
    
    @ConditionalOnExpression("${gcloud.image-node.schedule:false} == true")
    @Bean
    public Queue imageScheduleAsyncQueue() {
    	log.debug("init image schedule async queue:"+asyncScheduleQueue);
        return new Queue(asyncScheduleQueue);
    }
}