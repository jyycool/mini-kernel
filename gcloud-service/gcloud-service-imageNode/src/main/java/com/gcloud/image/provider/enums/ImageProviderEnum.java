package com.gcloud.image.provider.enums;

import com.gcloud.core.service.SpringUtil;
import com.gcloud.image.provider.IImageProvider;
import com.gcloud.image.provider.impl.*;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ImageProviderEnum {
	GLANCE("glance", GlanceImageNodeProviderImpl.class),
	GCLOUD("gcloud", GcloudImageNodeProviderImpl.class);

	private String providerType;
	private Class providerClazz;
	
	ImageProviderEnum(String providerType, Class clazz){
		this.providerType = providerType;
		this.providerClazz = clazz;
	}
	
	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Class getProviderClazz() {
		return providerClazz;
	}

	public void setProviderClazz(Class providerClazz) {
		this.providerClazz = providerClazz;
	}


	public static IImageProvider getByType(String type) {
		for (ImageProviderEnum providerE : ImageProviderEnum.values()) {
			if(providerE.getProviderType().equals(type)) {
				return (IImageProvider)SpringUtil.getBean(providerE.providerClazz);
			}
		}
		return null;
	}
}