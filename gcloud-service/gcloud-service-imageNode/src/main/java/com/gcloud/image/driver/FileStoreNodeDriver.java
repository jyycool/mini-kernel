package com.gcloud.image.driver;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.compute.enums.FileFormat;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.image.prop.ImageNodeProp;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Slf4j
@Component
public class FileStoreNodeDriver implements IImageStoreNodeDriver {
	@Autowired
	ImageNodeProp props;

	@Override
	public void downloadImage(String sourceFilePath, String imageId, String target, String imageResourceType) {
		String sourceFile = sourceFilePath;
		String targetFile = (imageResourceType.equals(ImageResourceType.IMAGE.value())?props.getImageCachedPath():props.getIsoCachedPath()) + imageId;
		File file = new File(targetFile);
        if (file.exists()) {
        	log.debug("镜像/映像文件已存在无�?再拷�?");
        	return;
        }
		
		QemuInfo info = DiskQemuImgUtil.info(sourceFilePath);
		if(info.getFormat().equals(FileFormat.RAW.getValue())) {
			DiskQemuImgUtil.convert(sourceFilePath, sourceFilePath + ".qcow2", info.getFormat(), FileFormat.QCOW2.getValue(), false);
			sourceFile = sourceFilePath + ".qcow2";
		}
		//cp image
		String[] cmd = null;
        cmd = new String[]{"cp", sourceFile, targetFile};
        int res = SystemUtil.runAndGetCode(cmd);
        if(res != 0) {//失败
        	throw new GCloudException("::下载镜像失败");
        }
        
        if(info.getFormat().equals(FileFormat.RAW.getValue())) {
        	//删除临时文件
	        String[] deleteCmd = new String[]{"rm", "-f", sourceFile};
	        int deleteRes = SystemUtil.runAndGetCode(deleteCmd);
	        if(deleteRes != 0) {//失败
	        	throw new GCloudException("::删除临时qcow2格式镜像文件失败");
	        }
        }
	}

}