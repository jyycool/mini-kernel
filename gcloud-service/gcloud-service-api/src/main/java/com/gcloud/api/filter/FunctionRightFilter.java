package com.gcloud.api.filter;

import com.gcloud.api.ApiIdentityConfig;
import com.gcloud.common.constants.HttpRequestConstant;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.currentUser.enums.RoleType;
import com.gcloud.core.identity.IApiIdentity;
import com.gcloud.header.identity.enums.UncheckApiEnum;
import com.gcloud.header.identity.role.DescribeUncheckApiReplyMsg;
import com.gcloud.header.identity.role.model.CheckRightReplyMsg;
import com.gcloud.header.identity.user.model.GetUserModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
public class FunctionRightFilter implements Filter{
	
	@Autowired
	ApiIdentityConfig identityConfig;
	@Autowired
	IApiIdentity apiIdentity;

	@Value("${gcloud.api.region:}")
	private String currentRegion;

	public static final String API_FUNCTION = "apiFunction";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		log.info("---------------------εΌ?ε§θΏε₯εθ½ζιζ¦ζ?----------------------------"); 
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String reqPath = httpRequest.getRequestURI();
		
		List<String> uncheckApis = (List<String>)CacheContainer.getInstance().get(CacheType.FUNCTION_RIGHT_UNCHECK_API.name());
		if(uncheckApis == null) {
			DescribeUncheckApiReplyMsg reply = apiIdentity.getUncheckApi(UncheckApiEnum.FUNCTION_RIGHT.name());
			uncheckApis = reply.getResponse().getPath();
			CacheContainer.getInstance().put(CacheType.FUNCTION_RIGHT_UNCHECK_API.name(), uncheckApis, identityConfig.getTokenTimeout());
		}

		if(uncheckApis.contains(reqPath)) {
			chain.doFilter(request, response);
			return ;
		}
		GetUserModel userInfo = (GetUserModel)httpRequest.getAttribute(HttpRequestConstant.ATTR_USER_INFO);
        if(userInfo != null) {
			if(userInfo.getRoleId().equals(RoleType.SUPER_ADMIN.getRoleId())){
				chain.doFilter(request, response);
				return ;
			}
			CheckRightReplyMsg rightReply = apiIdentity.checkRight(userInfo.getRoleId(), currentRegion, reqPath);
			
			//httpRequest.setAttribute(API_FUNCTION, rightReply.getFunctionRight());
			httpRequest.setAttribute(HttpRequestConstant.ATTR_RESOURCE_RIGHT, rightReply.getRoleResourceRightItem());
		}
		
		log.info("---------------------η»ζεθ½ζιζ¦ζͺ----------------------------"); 
		chain.doFilter(request, response);
	}

}