package com.gcloud.api.config;

import com.gcloud.api.filter.AuthFilter;
import com.gcloud.api.filter.ErrorFilter;
import com.gcloud.api.filter.FunctionRightFilter;
import com.gcloud.api.filter.HcpRouterFilter;
import com.gcloud.api.filter.HttpSignatureFilter;
import com.gcloud.api.filter.MonitorRouterFilter;
import com.gcloud.api.filter.RegionRouterFilter;
import com.gcloud.api.filter.RequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.servlet.Filter;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

public class RequestConfig {
	@Autowired
	RegionRouterFilter regionRouterFilter;
//	@Autowired
//	K8sRouterFilter k8sRouterFilter;
	@Autowired
	AuthFilter authFilter;
	@Autowired
	ErrorFilter errorFilter;
	@Autowired
	HttpSignatureFilter httpSignatureFilter;
	@Autowired
	FunctionRightFilter functionRightFilter;

	@Autowired
    HcpRouterFilter hcpRouterFilter;

	@Autowired
	MonitorRouterFilter monitorRouterFilter;

    @Bean
    public FilterRegistrationBean sessionOnlineFilter(Filter requestFilter) {
        FilterRegistrationBean filterRegistration = new FilterRegistrationBean();
        filterRegistration.setFilter(requestFilter);
        filterRegistration.addUrlPatterns("*.do");
        filterRegistration.setOrder(1);
        return filterRegistration;
    }

	/** 过滤器统�?异常处理
	 * 此过滤器�?要设置为第一个执�?
	 * @author dengyf
	 *
	 */
	@Bean
	public FilterRegistrationBean errorFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(errorFilter);
		registration.addUrlPatterns("*");
		registration.setOrder(2);
		return registration;
	}

	@Bean
	public FilterRegistrationBean authTokenFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(authFilter);
		registration.addUrlPatterns("*");
		registration.setOrder(3);
		return registration;
	}

	@Bean
	public FilterRegistrationBean httpSignatureFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(httpSignatureFilter);
		registration.addUrlPatterns("*");
		registration.setOrder(4);
		return registration;
	}
	
	@Bean
	public FilterRegistrationBean functionRightFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(functionRightFilter);
		registration.addUrlPatterns("*");
		registration.setOrder(5);
		return registration;
	}

    @Bean
    public FilterRegistrationBean authFilterRegistration() {
    	FilterRegistrationBean registration = new FilterRegistrationBean(regionRouterFilter);
    	registration.addUrlPatterns("*");
    	registration.setOrder(6);
    	return registration;
    }

	@Bean
	public FilterRegistrationBean hcpRouterFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(hcpRouterFilter);
		registration.addUrlPatterns("/hcp/*");
		registration.setOrder(7);
		return registration;
	}

//    @Bean
//    public FilterRegistrationBean k8sRouterFilterRegistration() {
//    	FilterRegistrationBean registration = new FilterRegistrationBean(k8sRouterFilter);
//    	registration.addUrlPatterns("/api/*");
//    	registration.addUrlPatterns("/apis/*");
//    	registration.setOrder(8);
//    	return registration;
//    }

	@Bean
	public FilterRegistrationBean monitorRouterFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean(monitorRouterFilter);
		registration.addUrlPatterns("/monitor/*");
		registration.setOrder(9);
		return registration;
	}
    
    @Bean
    public Filter requestFilter() {
        return new RequestFilter();
    }

}