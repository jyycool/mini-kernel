package com.gcloud.api.filter;

import com.gcloud.api.region.Region;
import com.gcloud.api.region.Regions;
import com.gcloud.common.util.HttpUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.Module;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class RegionRouterFilter implements Filter{
	@Autowired
	private Regions regions;
	@Autowired
	private RequestRouter router;

	private Set<String> regionLessUrl = new HashSet<>();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		regionLessUrl.add("/ecs/DescribeRegions.do");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest)request;
		String r = Regions.regionId(request);
		Region region = StringUtils.isBlank(r) ? null : regions.getRegions().get(r);

		boolean requireRegion = checkRegion(httpRequest, region);

		if(requireRegion && region != null && !region.isLocal()){
			HttpServletResponse httpResponse=(HttpServletResponse)response;
			String targetUrl = HttpUtil.url(region.getRemoteAddress(), httpRequest.getRequestURI());
			router.route(targetUrl, httpRequest, httpResponse);
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}

	public String module(String url){

		String[] urlArr = url.split("/");
		if(urlArr != null && urlArr.length > 1){
			return urlArr[1];
		}
		return null;
	}

	public boolean checkRegion(HttpServletRequest httpRequest, Region region){

	    boolean requireRegion = true;

		String urlModule = module(httpRequest.getRequestURI());

		if(!regionLessUrl.contains(httpRequest.getRequestURI())){
			Module module = Module.getByValueIgnoreCase(urlModule);
			//如果不存在，则是api的模块或者实际不存在，交给controller报错
			requireRegion = module != null && module.getRegion() != null && module.getRegion();
		}else{
			requireRegion = false;
		}

        if(requireRegion && region == null){
            throw new GCloudException("00000001::请�?�择区域");
        }

        return requireRegion;
	}
}