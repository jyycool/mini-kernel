package com.gcloud.api.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.gcloud.api.ApiRole;
import com.gcloud.api.condition.ApiRoleSelect;
import com.gcloud.api.region.Region;
import com.gcloud.api.region.Regions;
import com.gcloud.api.region.model.DescribeRegionInfosParams;
import com.gcloud.api.region.model.ModifyRegionParams;
import com.gcloud.api.service.RegionService;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.compute.msg.api.model.RegionInfoType;
import com.gcloud.header.compute.msg.api.model.RegionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@ApiRoleSelect(ApiRole.API)
public class RegionServiceImpl implements RegionService {

    @Autowired
    private Regions regions;

    @Override
    public void create(Region region) {

        if(regions.getRegions().containsKey(region.getId())){
           throw new GCloudException("::区域已经存在");
        }

        if(!region.isLocal()){
            if(StringUtils.isBlank(region.getRemoteAddress())){
                throw new GCloudException("::服务地址不能为空");
            }
        }else{
            region.setRemoteAddress(null);
        }

        //保存文件成功再修改内�?
        TreeMap<String, Region> regionMap = BeanUtil.copyBean(regions.getRegions(), new TypeReference<TreeMap<String, Region>>(){});
        regionMap.put(region.getId(), region);
        regions.save(regionMap);

        regions.getRegions().put(region.getId(), region);

    }

    @Override
    public void update(ModifyRegionParams region) {

        if(!regions.getRegions().containsKey(region.getId())){
            throw new GCloudException("::区域不存�?");
        }

        Region currentRegion = regions.getRegions().get(region.getId());

        if(region.getName() != null){
            currentRegion.setName(region.getName());
        }


        if(region.getLocal() != null){

            currentRegion.setLocal(region.getLocal());
            //改成true，清空remoteAddress
            if(region.getLocal()){
                currentRegion.setRemoteAddress(null);
            }else{ //改成false，需要判断remoteAddress
                if(StringUtils.isBlank(region.getRemoteAddress())){
                    throw new GCloudException("::服务地址不能为空");
                }
                currentRegion.setRemoteAddress(region.getRemoteAddress());
            }

        }else{
            //只改remoteAddress
            if(region.getRemoteAddress() != null && !currentRegion.isLocal()){
                if(StringUtils.isBlank(region.getRemoteAddress())){
                    throw new GCloudException("::服务地址不能为空");
                }
                currentRegion.setRemoteAddress(region.getRemoteAddress());
            }
        }


        TreeMap<String, Region> regionMap = BeanUtil.copyBean(regions.getRegions(), new TypeReference<TreeMap<String, Region>>(){});
        regionMap.put(region.getId(), currentRegion);
        regions.save(regionMap);

        regions.getRegions().put(region.getId(), currentRegion);
    }

    @Override
    public void delete(String regionId) {

        if(!regions.getRegions().containsKey(regionId)){
            throw new GCloudException("::区域不存�?");
        }

        TreeMap<String, Region> regionMap = BeanUtil.copyBean(regions.getRegions(), new TypeReference<TreeMap<String, Region>>(){});
        regionMap.remove(regionId);
        regions.save(regionMap);

        regions.getRegions().remove(regionId);

    }


    @Override
    public List<RegionType> list() {
        List<RegionType> regionTypes = new ArrayList<>();

        if(regions.getRegions() != null){
            for(Map.Entry<String, Region> regionInfo : regions.getRegions().entrySet()){
                Region region = regionInfo.getValue();
                RegionType type = new RegionType();
                type.setRegionId(region.getId());
                regionTypes.add(type);
            }
        }
        return regionTypes;
    }

    @Override
    public Region detail(String regionId) {
        return regions.getRegions().get(regionId);
    }

    @Override
    public PageResult<RegionInfoType> page(DescribeRegionInfosParams params) {

        List<RegionInfoType> matchRegion = new ArrayList<>();

        int skipNum = (params.getPageNumber() - 1) * params.getPageSize();
        int num = params.getPageSize();

        int totalSize = regions.getRegions().values().size();

        if(skipNum >= totalSize){
            return ApiUtil.toPage(params, matchRegion, totalSize);
        }

        for(Region region : regions.getRegions().values()){
            if(params.getLocal() != null && params.getLocal() != region.isLocal()){
                continue;
            }

            if(params.getName() != null && !region.getName().contains(params.getName())){
                continue;
            }

            if(params.getRemoteAddress() != null && (region.getRemoteAddress() == null || !region.getRemoteAddress().contains(params.getRemoteAddress()))){
                continue;
            }

            if(skipNum > 0){
                skipNum--;
                continue;
            }

            RegionInfoType regionInfoType = new RegionInfoType();
            regionInfoType.setRegionId(region.getId());
            regionInfoType.setName(region.getName());
            regionInfoType.setLocal(region.isLocal());
            regionInfoType.setRemoteAddress(region.getRemoteAddress());

            matchRegion.add(regionInfoType);
            num--;
            if(num <= 0){
                break;
            }
        }

        return ApiUtil.toPage(params, matchRegion, totalSize);
    }
}