package com.gcloud.api.handler.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.api.ApiRole;
import com.gcloud.api.condition.ApiRoleSelect;
import com.gcloud.api.service.RegionService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.region.msg.api.model.standard.StandardDescribeRegionsResponse;
import com.gcloud.header.region.msg.api.standard.StandardApiDescribeRegionsMsg;
import com.gcloud.header.region.msg.api.standard.StandardApiDescribeRegionsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, action = "DescribeRegions", name = "区域列表", versions = {ApiVersion.Standard})
@ApiRoleSelect(ApiRole.API)
public class StandardApiDescribeRegionsHandler extends MessageHandler<StandardApiDescribeRegionsMsg, StandardApiDescribeRegionsReplyMsg>{

	@Autowired
	private RegionService regionService;
	
	@Override
	public StandardApiDescribeRegionsReplyMsg handle(StandardApiDescribeRegionsMsg msg) throws GCloudException {
		StandardDescribeRegionsResponse response = new StandardDescribeRegionsResponse();
		response.setRegion(regionService.list());

		StandardApiDescribeRegionsReplyMsg reply = new StandardApiDescribeRegionsReplyMsg();
		reply.setRegions(response);

		return reply;
	}

}