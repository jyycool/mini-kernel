package com.gcloud.api.filter;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class FilterOrder {

    public static final int REQUEST_FILTER = 1;
    public static final int ERROR_FILTER = 2;
    public static final int AUTH_FILTER = 3;
    public static final int HTTP_SIGNATURE_FILTER = 4;
    public static final int REGION_ROUTER_FILTER = 5;
    public static final int FUNCTION_RIGHT_FILTER = 6;
    public static final int HCP_ROUTER_FILTER = 7;
    public static final int MONITOR_ROUTER_FILTER = 8;



}