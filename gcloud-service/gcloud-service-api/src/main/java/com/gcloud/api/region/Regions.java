package com.gcloud.api.region;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gcloud.api.ApiRole;
import com.gcloud.api.condition.ApiRoleSelect;
import com.gcloud.core.exception.GCloudException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.PostConstruct;
import javax.servlet.ServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@ApiRoleSelect(ApiRole.API)
@Slf4j
public class Regions {

	@Value("${gcloud.region.config-path:/etc/gcloud/region-config.yml}")
	private String regionConfigPath;

	@PostConstruct
	public void init(){
		Yaml yaml = new Yaml();
		File file = regionFile();
		try(FileInputStream fileInputStream = new FileInputStream(file)){
			Map<String, Object> objectMap = yaml.load(fileInputStream);
			Map<String, Object> regions = (Map<String, Object>)objectMap.get("regions");
			if(regions != null && regions.size() > 0){
				for(Map.Entry<String, Object> region : regions.entrySet()){
					String value = region.getKey();
					Region regionInfo = JSONObject.parseObject(JSONObject.toJSONString(region.getValue()), Region.class);
					this.regions.put(value, regionInfo);
				}
			}
		}catch (Exception ex){
			log.error("读取Region配置文件失败");
		}

	}

	private Map<String,Region> regions = new TreeMap<>();

	public Map<String, Region> getRegions() {
		return regions;
	}

	public void setRegions(Map<String, Region> regions) {
		this.regions = regions;
	}

	public static String regionId(ServletRequest request){
		String r = request.getParameter("regionId");
		if(r == null || "".equals(r)){
			r = request.getParameter("RegionId");
		}
		return r;
	}

	public static String regionId(WebRequest request){
		String r = request.getParameter("regionId");
		if(r == null || "".equals(r)){
			r = request.getParameter("RegionId");
		}
		return r;
	}

	public String getRegionConfigPath() {
		return regionConfigPath;
	}

	public void setRegionConfigPath(String regionConfigPath) {
		this.regionConfigPath = regionConfigPath;
	}

	public File regionFile(){
		File file = new File(getRegionConfigPath());
		if(!file.exists()){
			throw new GCloudException("::Region配置文件不存�?");
		}

		return file;
	}

	public void save(Map<String, Region> regionMap){
		Map<String, Object> regionYaml = new TreeMap<>();
		regionYaml.put("regions", regionMap);

		File file = regionFile();

		DumperOptions options=new DumperOptions();
		options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
		options.setPrettyFlow(true);
		Yaml yaml = new Yaml(options);
		try(FileWriter fileWriter = new FileWriter(file)){

			//先转出map，直接用yaml输出，会有类名�??

			//为了按属性顺序输�?
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setSerializationInclusion(Include.NON_NULL);
			String str = objectMapper.writeValueAsString(regionYaml);
			Map<String, Object> objectMap = objectMapper.readValue(str, LinkedHashMap.class);

			yaml.dump(objectMap, fileWriter);
		}catch (Exception ex){
			log.error("::保存文件失败, ex=" + ex, ex);
			throw new GCloudException("::保存文件失败");
		}
	}
}