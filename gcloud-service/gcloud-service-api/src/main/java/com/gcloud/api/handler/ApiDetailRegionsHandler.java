package com.gcloud.api.handler;

import com.gcloud.api.ApiRole;
import com.gcloud.api.condition.ApiRoleSelect;
import com.gcloud.api.region.Region;
import com.gcloud.api.service.RegionService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.region.msg.api.ApiDetailRegionMsg;
import com.gcloud.header.region.msg.api.ApiDetailRegionReplyMsg;
import com.gcloud.header.region.msg.api.model.RegionMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.REGION, action = "DetailRegion", name = "区域详情")
@ApiRoleSelect(ApiRole.API)
@GcLog(taskExpect = "区域详情")
public class ApiDetailRegionsHandler extends MessageHandler<ApiDetailRegionMsg, ApiDetailRegionReplyMsg>{

	@Autowired
	private RegionService regionService;

	@Override
	public ApiDetailRegionReplyMsg handle(ApiDetailRegionMsg msg) throws GCloudException {

		Region region = regionService.detail(msg.getId());
		ApiDetailRegionReplyMsg reply = new ApiDetailRegionReplyMsg();
		reply.setDetailRegion(BeanUtil.copyBean(region, RegionMsg.class));
		return reply;
	}
}