package com.gcloud.core.currentUser.policy.enums;

import com.gcloud.core.currentUser.policy.service.IResourceIsolationCheck;
import com.gcloud.core.currentUser.policy.service.ResourceIsolationCheckTypes;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ResourceIsolationCheckType {
	INSTANCE("instance"),
	PORT("port"),
	VOLUME("volume"),
	SNAPSHOT("snapshot"),
	IMAGE("image"),
	EIP("eip"),
	NETWORK("network"),
	ROUTER("router"),
	SECURITYGROUP("securityGroup"),
	SECURITYGROUPRULE("securityGroupRule"),
	SUBNET("subnet"),
	LOADBALANCER("loadBalancer"),
	LISTENER("listener");
	
	private String resourceType;
	
	ResourceIsolationCheckType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public IResourceIsolationCheck getCheck() {
		return ResourceIsolationCheckTypes.get(this.resourceType);
	}

}