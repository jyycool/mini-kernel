package com.gcloud.core.currentUser.policy.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ResourceIsolationType {
	ALL_TENANT,//�?有租户及用户共享，自建用户也可操�?
	ALL_TENANT_SELF,//�?有租户共享，自建资源用户非自建资源不可见
	IN_TENANT,//租户内可见，不管是否自建资源
	IN_TENANT_SELF;//租户内可见，自建资源用户非自建资源不可见
	
	public String getValue() {
        return name().toLowerCase();
    }
}