package com.gcloud.core.cache.redis.lock;

import com.gcloud.core.cache.DistributedLock;
import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import com.gcloud.core.exception.GCloudException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@ConditionalOnExpression("${gcloud.redis.enable:false} == true")
public class RedisSpinLock extends DistributedLock {
	@Autowired
	private GCloudRedisTemplate redisTemplate;
	
	@Override
	public String getLock(String lockName, long lockTimeout, long getLockTimeout) throws GCloudException {
		// TODO Auto-generated method stub
		boolean isSucc = false;
		boolean isGetTimerout = false;

		String value = UUID.randomUUID().toString(); // 这个值用于删�?
		long startTime = System.currentTimeMillis();
		do {
			isSucc = redisTemplate.opsForValue().setIfAbsent(lockName, value, Duration.ofMillis(lockTimeout));

			if (getLockTimeout >= 0) {
				isGetTimerout = System.currentTimeMillis() - startTime > getLockTimeout;
			}

		} while (!isSucc && !isGetTimerout);

		// 不成功直接抛�?
		if (!isSucc) {
			throw new GCloudException("::get lock fail");
		}
		return value;
	}

	@Override
	public boolean releaseLock(String lockName, String value) throws GCloudException {
		// TODO Auto-generated method stub
		boolean release = false;
		try{
			String v = String.valueOf(redisTemplate.opsForValue().get(lockName));
			if (v != null && v.equals(value)) {
				release = redisTemplate.delete(lockName);
			}
		}catch (Exception ex){
			throw new GCloudException(String.format("释放锁失�?%s", ex.getMessage()));
		}
		return release;
	}

}