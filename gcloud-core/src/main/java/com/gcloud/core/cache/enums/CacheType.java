package com.gcloud.core.cache.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum CacheType {
	ACTIVE_INFO,   // 服务�?活信�?
	NODE_INFO,     // 节点信息
	TIMER,         // 定时器相关数�?
	TOKEN_USER,
	SIGN_USER,
	FUNCTION_RIGHT_UNCHECK_API,
	AUTH_UNCHECK_API,
	//资源名称缓存
	SUBNET_NAME,
	NETWORK_NAME,
	PORT_NAME,
	EIP_NAME,
	ROUTER_NAME,
	SECURITYGROUP_NAME,
	LOADBALANCER_NAME,
	VSEVERGROUP_NAME,
	IMAGE_NAME,
	ISO_NAME,
	VOLUME_NAME,
	SNAPSHOT_NAME,
	STORAGEPOOL_NAME,
	INSTANCE_ALIAS, //虚拟机别名缓�?
	ROLE_NAME,
	USER_NAME;
}