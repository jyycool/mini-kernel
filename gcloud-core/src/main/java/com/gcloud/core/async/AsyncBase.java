package com.gcloud.core.async;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.UUID;
import java.util.concurrent.Future;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public abstract class AsyncBase {

    private String asyncId;
    private GcloudThreadPool threadPool;

    //重新计算超时时间
    private Long newTimeout;

    public abstract long timeout();
    public Long newTimeout(){
        return newTimeout;
    }

    public void setNewTimeout(Long newTimeout){
        this.newTimeout = newTimeout;
    }

    public String outputString(){
        return null;
    }

    public void successHandler(){
        defaultHandler();
    }
    public void failHandler(){
        defaultHandler();
    }
    public void timeoutHandler(){
        defaultHandler();
    }
    public void exceptionHandler(){
        defaultHandler();
    }
    public void defaultHandler(){

    }

    public long sleepTime(){
        return 100L;
    }
    //改为返回AsyncResult 为满足后续复杂场景�?�辑处理�?
    //只有返回 RUNNING才会继续 循环�?
    //EXCEPTION 是抛出异常后赋�?��??
    public abstract AsyncResult execute();

    protected AsyncResult run(){

        long timeout = timeout();
        boolean willTimeout = timeout > 0;
        long leftTime = timeout;
        AsyncResult asyncResult = null;
        AsyncStatus asyncStatus = AsyncStatus.RUNNING;
        do{
            long beginTime = System.currentTimeMillis();
            try{
                asyncResult = execute();
                asyncStatus = asyncResult.getAsyncStatus();
                if(outputString() != null){
                    log.debug(String.format("%s; loop; status = %s", outputString(), asyncStatus.toString()));
                }
            }catch (Exception ex){
                log.error("async run error, uuid:" + this.getAsyncId(), ex);
                asyncStatus = AsyncStatus.EXCEPTION;
            }

            if(asyncStatus != AsyncStatus.RUNNING){
                break;
            }

            if(newTimeout() != null){
                willTimeout = newTimeout() > 0;
                leftTime = newTimeout();
                newTimeout = null;
            }

            if(sleepTime() > 0){
                try{
                    Thread.sleep(sleepTime());
                }catch (Exception ex){
                    log.error("asycn sleep error uuid:" + this.getAsyncId(), ex);
                }
            }

            //防止�?直循环导致负数太�?
            if(willTimeout){
                leftTime = leftTime - (System.currentTimeMillis() - beginTime);
            }

        }while (!willTimeout || leftTime > 0);

        //完成，去掉map里面的数�?
        removeAsyncInfo(this.getAsyncId());

        if(outputString() != null){
            log.debug(String.format("%s; status = %s", outputString(), asyncStatus.toString()));
        }
        if(willTimeout && leftTime < 0){
            asyncStatus = AsyncStatus.TIMEOUT;
            log.debug("async is timeout, uuid:" + this.getAsyncId());
            this.timeoutHandler();
        }else if(asyncStatus == AsyncStatus.SUCCEED){
            log.debug(String.format("async execute is success, uuid:%s", this.asyncId));
            this.successHandler();
        }else if(asyncStatus == AsyncStatus.FAILED || asyncStatus == null){
            log.debug(String.format("async execute is fail, uuid:%s", this.asyncId));
            this.failHandler();
        }else if(asyncStatus == AsyncStatus.EXCEPTION){
            log.debug(String.format("async execute throws exception , uuid:%s", this.asyncId));
            this.exceptionHandler();
        }
        if(outputString() != null){
            log.debug(String.format("%s; final status = %s", outputString(), asyncStatus.toString()));
        }

        asyncResult.setAsyncStatus(asyncStatus);

        return asyncResult;
    }

    public void removeAsyncInfo(String id){
        try{
            AsyncThreadPool pool = SpringUtil.getBean(AsyncThreadPool.class);
            if(pool != null){
                pool.removeAsyncTask(id);
            }
        }catch (Exception ex){
            log.error("remove async info error", ex);
        }

    }

    public void start() throws GCloudException{

        GcloudThreadPool pool = threadPool();

        Future<?> future = pool.getExecutor().submit(this::run);
        AsyncInfo asyncInfo = new AsyncInfo(this.getAsyncId(), future, timeout(), System.currentTimeMillis());
        pool.putAsyncTask(this.getAsyncId(), asyncInfo);
    }

    public GcloudThreadPool threadPool(){

        if(threadPool == null){
            threadPool = SpringUtil.getBean(AsyncThreadPool.class);
            if(threadPool == null){
                throw new GCloudException("::not support");
            }
        }

        return threadPool;
    }

    public String getAsyncId(){
        if(StringUtils.isBlank(asyncId)){
            asyncId = UUID.randomUUID().toString();
        }
        return asyncId;
    }

    public GcloudThreadPool getThreadPool() {
        return threadPool;
    }

    public void setThreadPool(GcloudThreadPool threadPool) {
        this.threadPool = threadPool;
    }
}