package com.gcloud.core.async;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public abstract class GcloudThreadPool {

    private Map<String, AsyncInfo> asyncTasks = new HashMap<>();
    private ExecutorService executor;
    private boolean checkThread = false;

    public void putAsyncTask(String id, AsyncInfo asyncInfo){
        if(isCheckThread()){
            asyncTasks.put(id, asyncInfo);
        }
    }

    public void removeAsyncTask(String id){
        if(isCheckThread()){
            asyncTasks.remove(id);
        }
    }

    public Map<String, AsyncInfo> getAsyncTasks() {
        return asyncTasks;
    }

    public ExecutorService getExecutor(){
        return executor;
    }

    public void setExecutor(ExecutorService executor){
        this.executor = executor;
    }

    public boolean isCheckThread() {
        return checkThread;
    }

    public void setCheckThread(boolean checkThread) {
        this.checkThread = checkThread;
    }
}