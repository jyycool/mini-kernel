package com.gcloud.core.workflow.async;

import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.gcloud.core.workflow.engine.WorkFlowEngine;
import com.gcloud.core.workflow.enums.FeedbackState;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
public class WorkFlowAsyncTask {
	@Async("asyncExecutor")
    public void feedbackAsync(String taskId, String errorInfo) {
		log.info("feedbackAsync taskId:" + taskId + ",errorInfo:" + errorInfo + ",thread id:" + Thread.currentThread().getId());
		WorkFlowEngine.feedbackHandler(taskId, StringUtils.isBlank(errorInfo)?FeedbackState.SUCCESS.name():FeedbackState.FAILURE.name(), errorInfo);
	}
}