package com.gcloud.core.workflow.mng.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.core.workflow.dao.impl.WorkFlowTypeDao;
import com.gcloud.core.workflow.entity.WorkFlowType;
import com.gcloud.core.workflow.mng.IWorkFlowTypeMng;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
public class WorkFlowTypeMng implements IWorkFlowTypeMng {
	
	@Autowired
	WorkFlowTypeDao workFlowTypeDao;
	
	public WorkFlowType findUniqueByCode(String code) {
		return workFlowTypeDao.findUniqueByProperty("flowTypeCode", code);
	}
}