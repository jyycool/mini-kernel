package com.gcloud.core.workflow.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.core.workflow.dao.IBatchWorkFlowDao;
import com.gcloud.core.workflow.dao.IWorkFlowInstanceDao;
import com.gcloud.core.workflow.dao.IWorkFlowInstanceStepDao;
import com.gcloud.core.workflow.service.IWorkflowService;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class WorkflowServiceImpl implements IWorkflowService{
	@Autowired
	private IWorkFlowInstanceDao workFlowInstanceDao;
	
	@Autowired
	private IWorkFlowInstanceStepDao workFlowInstanceStepDao;
	
	@Autowired
	private IBatchWorkFlowDao batchWorkFlowDao;
}