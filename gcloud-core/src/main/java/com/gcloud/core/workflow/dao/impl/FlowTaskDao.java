package com.gcloud.core.workflow.dao.impl;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.gcloud.core.workflow.dao.IFlowTaskDao;
import com.gcloud.core.workflow.entity.FlowTask;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Repository
public class FlowTaskDao  extends JdbcBaseDaoImpl<FlowTask, Long> implements IFlowTaskDao{

	@Override
	public void deleteExpireData(String endTime) {
		String sql ="delete from gc_work_flow_task where start_time <= '" + endTime + "'";
		log.info("deleteExpireData sql:" + sql);
		this.jdbcTemplate.execute(sql);
	}

}