package com.gcloud.core.workflow.mng.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.core.workflow.dao.IWorkFlowTemplateDao;
import com.gcloud.core.workflow.entity.WorkFlowTemplate;
import com.gcloud.core.workflow.mng.IWorkFlowTemplateMng;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
@Transactional
public class WorkFlowTemplateMng implements IWorkFlowTemplateMng{
	@Autowired
	private IWorkFlowTemplateDao workFlowTemplateDao;

	@Override
	@Cacheable(value="flowtemplate", key="#id")
	public WorkFlowTemplate findById(Integer id) {
		return workFlowTemplateDao.getById(id);
	}

	@Override
	public WorkFlowTemplate findUnique(String field, String value) {
		return workFlowTemplateDao.findUniqueByProperty(field, value);
	}

	@Override
	public List<WorkFlowTemplate> findByProperty(String field, String value) {
		return workFlowTemplateDao.findByProperty(field, value);
	}

}