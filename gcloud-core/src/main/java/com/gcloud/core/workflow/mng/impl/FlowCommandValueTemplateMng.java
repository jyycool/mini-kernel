package com.gcloud.core.workflow.mng.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.core.workflow.dao.IFlowCommandValueTemplateDao;
import com.gcloud.core.workflow.entity.FlowCommandValueTemplate;
import com.gcloud.core.workflow.mng.IFlowCommandValueTemplateMng;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
@Transactional
public class FlowCommandValueTemplateMng implements IFlowCommandValueTemplateMng{
	@Autowired
	private IFlowCommandValueTemplateDao flowCommandValueTemplateDao;

	@Override
	public List<FlowCommandValueTemplate> findByProperty(String field, String value) {
		return flowCommandValueTemplateDao.findByProperty(field, value);
	}

	@Override
	public List<FlowCommandValueTemplate> getTemplatesByStepId(String flowTypeCode, Integer stepId) {
		Map<String, Object> search = new HashMap<String, Object>();
		search.put("flow_type_code", flowTypeCode);
		search.put("step_id", stepId);
		return flowCommandValueTemplateDao.findByProperties(search);
	}

}