package com.gcloud.core.simpleflow;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class SimpleFlowTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleFlowChain<String,String> chain=new SimpleFlowChain<String,String>();
		chain.name("test").then(new Flow<String>("1111") {

			@Override
			public void run(SimpleFlowChain chain, String data) {
				// TODO Auto-generated method stub
				data="step1";
				System.out.println(data);
				chain.next();
			}

			@Override
			public void rollback(SimpleFlowChain chain, String data) {
				// TODO Auto-generated method stub
				System.out.println("step1 rollback");
				chain.rollback();
			}
		}).then(new NoRollbackFlow<Object>("22222") {

			@Override
			public void run(SimpleFlowChain chain, Object data) {
				// TODO Auto-generated method stub
				data="step2";
				System.out.println(data);
				chain.next();
			}
		}).then(new Flow<String>("3333") {

			@Override
			public void run(SimpleFlowChain chain, String data) {
				// TODO Auto-generated method stub
				data="step3";
				System.out.println(data);
				chain.fail("dfdfdfd");
			}

			@Override
			public void rollback(SimpleFlowChain chain, String data) {
				// TODO Auto-generated method stub
				System.out.println("step 3 rollback");
				chain.rollback();
			}
		}).done(new FlowDoneHandler<String>() {

			@Override
			public void handle(String data) {
				// TODO Auto-generated method stub
				System.out.println("done");
			}
		}).flowFinally(new FlowFinallyHandler() {
			
			@Override
			public void handle() {
				// TODO Auto-generated method stub
				System.out.println("finally");
			}
		}).start();
	}

}