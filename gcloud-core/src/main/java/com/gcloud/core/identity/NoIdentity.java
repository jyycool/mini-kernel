package com.gcloud.core.identity;

import com.gcloud.header.identity.api.GetApiReplyMsg;
import com.gcloud.header.identity.ldap.GetLdapConfReplyMsg;
import com.gcloud.header.identity.role.ApiGetFunctionRightReplyMsg;
import com.gcloud.header.identity.role.DescribeUncheckApiReplyMsg;
import com.gcloud.header.identity.role.model.CheckRightReplyMsg;
import com.gcloud.header.identity.user.GetUserReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class NoIdentity implements IApiIdentity {

	@Override
	public TokenUser checkToken(String token) {
		// TODO Auto-generated method stub
		TokenUser tokenUser=new TokenUser();
		tokenUser.setExpressTime(1877156686000l);//2029�?
		//tokenUser.setUserId("cc81efe719bf4b05a3c21524ccf6c1ea");
		return tokenUser;
	}

	@Override
	public SignUser getUserByAccessKey(String accessKey) {
		// TODO Auto-generated method stub
		SignUser signUser=new SignUser();
		signUser.setSecretKey("rgXooMaQAns9ki6SFlHps3O8flgY4TcPpQWwit0G");
		signUser.setUserId("cc81efe719bf4b05a3c21524ccf6c1ea");
		return signUser;
	}

	@Override
	public GetUserReplyMsg getUserById(String userId) {
		// TODO Auto-generated method stub
		GetUserReplyMsg reply=new GetUserReplyMsg();
		reply.setId("cc81efe719bf4b05a3c21524ccf6c1ea");
		reply.setDisable(false);
		reply.setEmail("gcloudtest01@gdeii.com.cn");
		reply.setLoginName("admin");
		reply.setRealName("admin");
		reply.setRoleId("superadmin");
		reply.setMobile("189xxxxxxxx");
		reply.setSuccess(true);
		return reply;
	}

	/*@Override
	public ApiGetRoleRightReplyMsg getRoleRight(String roleId, String regionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiDetailRoleReplyMsg getRoleDetail(String roleId) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	/*@Override
	public ApiGetFunctionRightReplyMsg getFunctionRight(String path) {
		return null;
	}*/

	@Override
	public DescribeUncheckApiReplyMsg getUncheckApi(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetLdapConfReplyMsg getLdapConf(String domainId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CheckRightReplyMsg checkRight(String roleId, String regionId, String funcPath) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GetApiReplyMsg getApi(String path) {
		// TODO Auto-generated method stub
		return null;
	}

}