package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.TplExternalNetworkResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiTplExternalNetworksReplyMsg extends ApiReplyMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "外网信息")
	TplExternalNetworkResponse externalNetworks;
	public TplExternalNetworkResponse getExternalNetworks() {
		return externalNetworks;
	}
	public void setExternalNetworks(TplExternalNetworkResponse externalNetworks) {
		this.externalNetworks = externalNetworks;
	}
}