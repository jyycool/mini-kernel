package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.standard.StandardDescribeSecurityGroupsResponse;
import com.gcloud.header.network.model.standard.StandardSecurityGroupItemType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeSecurityGroupsReplyMsg extends PageReplyMessage<StandardSecurityGroupItemType>{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全组列�?")
	private StandardDescribeSecurityGroupsResponse securityGroups;

	@Override
	public void setList(List<StandardSecurityGroupItemType> list) {
		securityGroups = new StandardDescribeSecurityGroupsResponse();
		securityGroups.setSecurityGroup(list);
	}

	public StandardDescribeSecurityGroupsResponse getSecurityGroups() {
		return securityGroups;
	}

	public void setSecurityGroups(StandardDescribeSecurityGroupsResponse securityGroups) {
		this.securityGroups = securityGroups;
	}
}