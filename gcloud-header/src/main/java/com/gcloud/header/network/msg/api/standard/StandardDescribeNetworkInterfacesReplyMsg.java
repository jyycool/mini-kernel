package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.standard.StandardDescribeNetworkInterfacesResponse;
import com.gcloud.header.network.model.standard.StandardNetworkInterfaceSet;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardDescribeNetworkInterfacesReplyMsg extends PageReplyMessage<StandardNetworkInterfaceSet> {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "网卡列表")
	private StandardDescribeNetworkInterfacesResponse networkInterfaceSets;
	
	@Override
	public void setList(List<StandardNetworkInterfaceSet> list) {
		networkInterfaceSets = new StandardDescribeNetworkInterfacesResponse();
		networkInterfaceSets.setNetworkInterfaceSet(list);
		
	}
	public StandardDescribeNetworkInterfacesResponse getNetworkInterfaceSets() {
		return networkInterfaceSets;
	}
	public void setNetworkInterfaceSets(StandardDescribeNetworkInterfacesResponse networkInterfaceSets) {
		this.networkInterfaceSets = networkInterfaceSets;
	}
	
}