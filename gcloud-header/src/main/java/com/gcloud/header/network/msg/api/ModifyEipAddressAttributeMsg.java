package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

import javax.validation.constraints.NotNull;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ModifyEipAddressAttributeMsg extends ApiMessage {
	@ApiModel(description = "弹�?? IP地址申请Id", require = true)
	@NotNull(message = "0050501::弹�?? IP地址申请ID不能为空")
	private String allocationId;
	@ApiModel(description = "弹�?? IP地址带宽", require = true)
	@NotNull(message = "0050502::弹�?? IP地址带宽不能为空")
	private Integer bandwidth;//带宽�? Mbps 计算
	
	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
}