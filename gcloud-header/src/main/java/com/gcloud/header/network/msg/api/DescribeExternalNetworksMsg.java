package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeExternalNetworksMsg extends ApiPageMessage{

	@ApiModel(description = "�?要查询的Network�? Id列表")
	private List<String> networkIds;
	
	private Integer type;
	
	
	public List<String> getNetworkIds() {
		return networkIds;
	}


	public void setNetworkIds(List<String> networkIds) {
		this.networkIds = networkIds;
	}
	
	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return DescribeExternalNetworksReplyMsg.class;
	}

}