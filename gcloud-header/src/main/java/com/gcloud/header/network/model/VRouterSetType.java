package com.gcloud.header.network.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;

import java.io.Serializable;
import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class VRouterSetType implements Serializable {
    @ApiModel(description = "路由器Id")
    @TableField("id")
    private String vRouterId;
    @ApiModel(description = "路由器名�?")
    @TableField("name")
    private String vRouterName;
    @ApiModel(description = "区域Id")
    private String regionId = ControllerProperty.REGION_ID;
    @ApiModel(description = "状�?�，active:�?�?;down:失活;build:已创�?;error:错误;pending_create:创建�?;pending_update:删除�?;pending_delete:删除�?;unrecognized:未知;")
    private String status;
    @ApiModel(description = "中文状�??")
    private String cnStatus;
    private String subnets;
    
    @ApiModel(description = "外网网络ID")
    @TableField("external_gateway_network_id")
    private String networkId;
    @ApiModel(description = "外网网络名称")
    private String networkName;
    @ApiModel(description = "创建时间")
    @JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
    private Date createTime;
    @ApiModel(description = "用户ID")
    private String userId;
    @ApiModel(description = "用户�?")
    private String userName;

    public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getvRouterId() {
        return vRouterId;
    }

    public void setvRouterId(String vRouterId) {
        this.vRouterId = vRouterId;
    }

    public String getvRouterName() {
        return vRouterName;
    }

    public void setvRouterName(String vRouterName) {
        this.vRouterName = vRouterName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubnets() {
        return subnets;
    }

    public void setSubnets(String subnets) {
        this.subnets = subnets;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}