package com.gcloud.header.network.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiAuthorizeSecurityGroupMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0040501")
	@ApiModel(description = "规则方向入：ingress和出：egress", require = true)
	private String direction;
	@NotBlank(message = "0040502")
	@ApiModel(description = "安全组Id", require = true)
	private String securityGroupId;
	@ApiModel(description = "安全组协�?", require = true)
	private String ipProtocol;
	//@NotBlank(message = "0040504")
	@ApiModel(description = "端口范围")
	private String portRange;
	@ApiModel(description = "源安全组Id")
	private String sourceGroupId;
	@ApiModel(description = "源ip�?")
	private String sourceCidrIp;
	@ApiModel(description = "目标安全组Id")
	private String destGroupId;
	@ApiModel(description = "目标安全组Ip")
	private String destCidrIp;

	@Override
	public Class replyClazz() {
		return StandardApiAuthorizeSecurityGroupReplyMsg.class;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getIpProtocol() {
		return ipProtocol;
	}

	public void setIpProtocol(String ipProtocol) {
		this.ipProtocol = ipProtocol;
	}

	public String getPortRange() {
		return portRange;
	}

	public void setPortRange(String portRange) {
		this.portRange = portRange;
	}

	public String getSourceGroupId() {
		return sourceGroupId;
	}

	public void setSourceGroupId(String sourceGroupId) {
		this.sourceGroupId = sourceGroupId;
	}

	public String getSourceCidrIp() {
		return sourceCidrIp;
	}

	public void setSourceCidrIp(String sourceCidrIp) {
		this.sourceCidrIp = sourceCidrIp;
	}

	public String getDestGroupId() {
		return destGroupId;
	}

	public void setDestGroupId(String destGroupId) {
		this.destGroupId = destGroupId;
	}

	public String getDestCidrIp() {
		return destCidrIp;
	}

	public void setDestCidrIp(String destCidrIp) {
		this.destCidrIp = destCidrIp;
	}
}