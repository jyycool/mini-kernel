package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailEipAddressResponse  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "区域Id")
    private String regionId = ControllerProperty.REGION_ID;                        //弹�?�公网IP�?在的地域
    @ApiModel(description = "公网地址")
    private String ipAddress;                        //弹�?�公网IP
    @ApiModel(description = "弹�?�公网IP实例Id")
    private String allocationId;                    //弹�?�公网IP实例Id
    @ApiModel(description = "弹�?�公网IP当前的状态Available:可使用�?�InUse:已使用�?�Unassociating:解绑中�?�Associating:绑定中�?�Deleted:已删�?")
    private String status;  
    @ApiModel(description = "中文状�??")
    private String cnStatus;
    @ApiModel(description = "绑定的实例id")
    private String instanceId;                        //弹�?�公网IP当前绑定资源的Id；如果未绑定则�?�为空�??
    @ApiModel(description = "绑定的实例名�?")
    private String instanceName; 
    @ApiModel(description = "绑定的实例类�?")
    private String instanceType = "netcard";
    @ApiModel(description = "弹�?�公网IP的公网带宽限�?")
    private Integer bandwidth;                        //弹�?�公网IP的公网带宽限速，默认�?5Mbps
    @ApiModel(description = "分配时间")
    private String allocationTime;                    //分配时间。按照ISO8601标准表示，并�?要使用UTC时间。格式为：YYYY-MM-DDThh:mmZ
    @ApiModel(description = "外网网络Id")
    private String externalNetworkId;
    @ApiModel(description = "外网网络名称")
    private String externalNetworkName;
    @ApiModel(description = "虚拟机ID")
    private String vmInstanceId;            //虚拟机ID            
    @ApiModel(description = "虚拟机名�?")
    private String vmInstanceName;          //虚拟机名�?
    
	public String getVmInstanceId() {
		return vmInstanceId;
	}
	public void setVmInstanceId(String vmInstanceId) {
		this.vmInstanceId = vmInstanceId;
	}
	public String getVmInstanceName() {
		return vmInstanceName;
	}
	public void setVmInstanceName(String vmInstanceName) {
		this.vmInstanceName = vmInstanceName;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getInstanceType() {
		return instanceType;
	}
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
	public Integer getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}
	public String getAllocationTime() {
		return allocationTime;
	}
	public void setAllocationTime(String allocationTime) {
		this.allocationTime = allocationTime;
	}
	public String getExternalNetworkId() {
		return externalNetworkId;
	}
	public void setExternalNetworkId(String externalNetworkId) {
		this.externalNetworkId = externalNetworkId;
	}
	public String getExternalNetworkName() {
		return externalNetworkName;
	}
	public void setExternalNetworkName(String externalNetworkName) {
		this.externalNetworkName = externalNetworkName;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
    
}