package com.gcloud.header.network.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailNic implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "网卡ID")
	private String networkInterfaceId;
	
	@ApiModel(description = "网卡名称")
	private String networkInterfaceName;
	
	@ApiModel(description ="中文状�??")
	private String cnStatus;
	
	@ApiModel(description ="创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date creationTime;
	
	@ApiModel(description = "创建�?")
	private String creator;
	
	@ApiModel(description = "设备ID")
	private String deviceId;
	
	@ApiModel(description = "设备名称")
	private String deviceName;
	
	@ApiModel(description = "设备类型")
	private String deviceType;
	
	@ApiModel(description = "Mac地址")
	private String macAddress;
	
	@ApiModel(description = "网络ID")
	private String networkId;
	
	@ApiModel(description = "网络名称")
	private String networkName;
	
	@ApiModel(description = "IP地址")
	private String privateIpAddress;
	
	@ApiModel(description = "状�??, active:�?�?;down:失活;build:已创�?;error:错误;pending_create:创建�?;pending_update:删除�?;pending_delete:删除�?;unrecognized:未知;")
	private String status;

	@ApiModel(description = "vswitchId")
	private String vSwitchId;
	
	@ApiModel(description = "vswitch名称")
	private String vSwitchName;
	
	@ApiModel(description = "vpcId")
	private String vpcId;
	
	@ApiModel(description = "vpc名称")
	private String vpcName;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	
	@ApiModel(description = "可用区名�?")
	private String zoneName;
	
	@ApiModel(description = "安全组信�?")
	private List<NicSecurityGroup> securityGroups;

	@ApiModel(description = "提供者类�?")
	private Integer provider;

	@ApiModel(description = "提供者资源ID")
	private String providerRefId;
	
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}
	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}
	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getPrivateIpAddress() {
		return privateIpAddress;
	}
	public void setPrivateIpAddress(String privateIpAddress) {
		this.privateIpAddress = privateIpAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getvSwitchName() {
		return vSwitchName;
	}
	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public List<NicSecurityGroup> getSecurityGroups() {
		return securityGroups;
	}
	public void setSecurityGroups(List<NicSecurityGroup> securityGroups) {
		this.securityGroups = securityGroups;
	}
	public Integer getProvider() {
		return provider;
	}
	public void setProvider(Integer provider) {
		this.provider = provider;
	}

	public String getProviderRefId() {
		return providerRefId;
	}

	public void setProviderRefId(String providerRefId) {
		this.providerRefId = providerRefId;
	}
}