package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class PermissionType  implements Serializable{
	@ApiModel(description = "安全组规则ID")
	private String securityGroupRuleId;
	@ApiModel(description = "协议")
	private String ipProtocol;
	@ApiModel(description = "端口")
	private String portRange;
	@ApiModel(description = "源IP")
	private String sourceCidrIp;
	@ApiModel(description = "源安全组ID")
	private String sourceGroupId;
	@ApiModel(description = "源安全组名称")
	private String sourceGroupName;
	@ApiModel(description = "目标IP")
	private String destCidrIp;
	@ApiModel(description = "目标安全组ID")
	private String destGroupId;
	@ApiModel(description = "目标安全组名�?")
	private String destGroupName;
	@ApiModel(description = "方向")
	private String direction;
	@ApiModel(description = "描述")
	private String description;
	
	public String getIpProtocol() {
		return ipProtocol;
	}
	
	public void setIpProtocol(String ipProtocol) {
		this.ipProtocol = ipProtocol;
	}
	
	public String getPortRange() {
		return portRange;
	}
	
	public void setPortRange(String portRange) {
		this.portRange = portRange;
	}
	
	public String getSourceCidrIp() {
		return sourceCidrIp;
	}
	
	public void setSourceCidrIp(String sourceCidrIp) {
		this.sourceCidrIp = sourceCidrIp;
	}
	
	public String getSourceGroupId() {
		return sourceGroupId;
	}
	
	public void setSourceGroupId(String sourceGroupId) {
		this.sourceGroupId = sourceGroupId;
	}
	
	public String getDestCidrIp() {
		return destCidrIp;
	}
	
	public void setDestCidrIp(String destCidrIp) {
		this.destCidrIp = destCidrIp;
	}
	
	public String getDestGroupId() {
		return destGroupId;
	}
	
	public void setDestGroupId(String destGroupId) {
		this.destGroupId = destGroupId;
	}
	
	public String getDirection() {
		return direction;
	}
	
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSecurityGroupRuleId() {
		return securityGroupRuleId;
	}

	public void setSecurityGroupRuleId(String securityGroupRuleId) {
		this.securityGroupRuleId = securityGroupRuleId;
	}

	public String getSourceGroupName() {
		return sourceGroupName;
	}

	public void setSourceGroupName(String sourceGroupName) {
		this.sourceGroupName = sourceGroupName;
	}

	public String getDestGroupName() {
		return destGroupName;
	}

	public void setDestGroupName(String destGroupName) {
		this.destGroupName = destGroupName;
	}
}