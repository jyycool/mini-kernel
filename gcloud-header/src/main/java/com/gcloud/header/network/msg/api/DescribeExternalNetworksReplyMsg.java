package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.DescribeExternalNetworksResponse;
import com.gcloud.header.network.model.ExternalNetworkSetType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeExternalNetworksReplyMsg extends PageReplyMessage<ExternalNetworkSetType> {
	
	@ApiModel(description = "外部网络信息")
	private DescribeExternalNetworksResponse networks;
	
	@Override
	public void setList(List<ExternalNetworkSetType> list) {
		// TODO Auto-generated method stub
		networks = new DescribeExternalNetworksResponse();
		networks.setNetwork(list);
	}

	public DescribeExternalNetworksResponse getNetworks() {
		return networks;
	}

	public void setNetworks(DescribeExternalNetworksResponse networks) {
		this.networks = networks;
	}
	
	

}