package com.gcloud.header.identity.ldap;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiCreateLdapMsg extends ApiMessage{
	@ApiModel(description = "域名" ,require = true)
	@Length(min=4,max=30, message="identity_server_ldap_000005")
	private String domain;//                    域名           string     长度�?4-30的字符串（不能手动输入，输入基本DN自动生成�?                     
	@ApiModel(description = "域别�?" ,require = true)
	@Length(min=4,max=20,message="identity_server_ldap_000006")
	private String domainAlias;//               域别�?         String     长度�?4-20的字符串                                      域测�?
	@ApiModel(description = "基本DN",require = true)
	@Length(min=4,max=256,message="identity_server_ldap_000007")
	private String baseDn;//                    基本DN         String    格式 (OU=developer,DC=domainname,DC=com�?                OU=developer,DC=domainname,DC=coms
	@ApiModel(description = "域服务器url",require = true)
	@Length(min=7,max=100,message="identity_server_ldap_000008")
	private String url;//                       域服务器url    String    s
	@ApiModel(description = "管理账号",require = true)
	@Length(min=4,max=20,message="identity_server_ldap_000009")
	private String managerUser;//               管理账号       String     长度�?4-20的字符串                                      gcloudtesr                        
	@ApiModel(description = "管理密码",require = true)
	@Length(min=6,max=20,message="identity_server_ldap_000010")
	private String managerPsw;//                                                                                                                       
	@ApiModel(description = "是否自动同步,true:�?,false:�?",require = true)
	private boolean autoSync;//                  是否自动同步   boolean     true,false                                             true                                                                                                                                                                                                                      
	@ApiModel(description = "是否为默认域,true:�?,false:�?")
	private boolean defaultDomain;//             是否为默认阈   boolean     true,false                                             true                                                                                                                                            
	@Length(min=0,max=200,message="identity_server_ldap_000013")
    @ApiModel(description = "备注")
    private String description;//               备注           Stirng      长度�?0-200的字符串 （可空）                           这是入域测试s                                                                                                                       
    @NotBlank(message="identity_server_ldap_000014")
	@ApiModel(description = "域类�?", require = true)
	private String ldapType;// 
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiReplyMessage.class;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getDomainAlias() {
		return domainAlias;
	}
	public void setDomainAlias(String domainAlias) {
		this.domainAlias = domainAlias;
	}
	public String getBaseDn() {
		return baseDn;
	}
	public void setBaseDn(String baseDn) {
		this.baseDn = baseDn;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getManagerUser() {
		return managerUser;
	}
	public void setManagerUser(String managerUser) {
		this.managerUser = managerUser;
	}
	public String getManagerPsw() {
		return managerPsw;
	}
	public void setManagerPsw(String managerPsw) {
		this.managerPsw = managerPsw;
	}
	public boolean isAutoSync() {
		return autoSync;
	}
	public void setAutoSync(boolean autoSync) {
		this.autoSync = autoSync;
	}
	public boolean isDefaultDomain() {
		return defaultDomain;
	}
	public void setDefaultDomain(boolean defaultDomain) {
		this.defaultDomain = defaultDomain;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLdapType() {
		return ldapType;
	}
	public void setLdapType(String ldapType) {
		this.ldapType = ldapType;
	}
	
}