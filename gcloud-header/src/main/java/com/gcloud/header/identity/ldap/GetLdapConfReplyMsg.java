package com.gcloud.header.identity.ldap;

import com.gcloud.header.ReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class GetLdapConfReplyMsg extends ReplyMessage{
	@ApiModel(description ="域名",require = true)
	private String domain;
	@ApiModel(description ="用户�?",require = true)
	private String user;
	@ApiModel(description = "密码",require = true)
	private String password;
	public String getDomain() {
		return domain;
	}
	public String getUser() {
		return user;
	}
	public String getPassword() {
		return password;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}