package com.gcloud.header.identity.ldap;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiSyncUsersMsg extends ApiMessage {
	@NotBlank(message ="identity_server_ldap_700004")
	@ApiModel(description ="域ID",require = true)
	private String domainId;
    @NotBlank(message = "identity_server_ldap_700005")
    @ApiModel(description ="默认角色", require = true)
	private String defaultRole;
    @ApiModel(description ="用户DN",require = true)
    private List<String> userDns;
    @ApiModel(description ="角色id")
    private List<String> roleIds;
    @ApiModel(description ="用户�?",require = true)
    private List<String> userNames;
    @ApiModel(description ="用户别名")
    private List<String> displayNames;
    @ApiModel(description ="邮件")
    private List<String> emails;
    @ApiModel(description ="组织DN")
    private List<String> orgDns;
    
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiReplyMessage.class;
	}

	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public String getDefaultRole() {
		return defaultRole;
	}

	public void setDefaultRole(String defaultRole) {
		this.defaultRole = defaultRole;
	}

	public List<String> getUserDns() {
		return userDns;
	}

	public void setUserDns(List<String> userDns) {
		this.userDns = userDns;
	}

	public List<String> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}

	public List<String> getUserNames() {
		return userNames;
	}

	public void setUserNames(List<String> userNames) {
		this.userNames = userNames;
	}

	public List<String> getDisplayNames() {
		return displayNames;
	}

	public void setDisplayNames(List<String> displayNames) {
		this.displayNames = displayNames;
	}

	public List<String> getEmails() {
		return emails;
	}

	public void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public List<String> getOrgDns() {
		return orgDns;
	}

	public void setOrgDns(List<String> orgDns) {
		this.orgDns = orgDns;
	}

}