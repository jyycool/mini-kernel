package com.gcloud.header.identity.role.model;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CheckRightReplyMsg extends ApiReplyMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	@ApiModel(description="功能详情")
//	private FunctionRightDetail functionRight;
	@ApiModel(description="角色资源权限")
	private RoleResourceRightItem roleResourceRightItem;
//	public FunctionRightDetail getFunctionRight() {
//		return functionRight;
//	}
//	public void setFunctionRight(FunctionRightDetail functionRight) {
//		this.functionRight = functionRight;
//	}
	public RoleResourceRightItem getRoleResourceRightItem() {
		return roleResourceRightItem;
	}
	public void setRoleResourceRightItem(RoleResourceRightItem roleResourceRightItem) {
		this.roleResourceRightItem = roleResourceRightItem;
	}
	
}