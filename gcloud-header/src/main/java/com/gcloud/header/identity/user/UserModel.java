package com.gcloud.header.identity.user;

import java.io.Serializable;
import java.util.Date;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class UserModel implements Serializable{
	@ApiModel(description="用户ID")
	private String id;
	@ApiModel(description="登录�?")
	private String loginName;
	@ApiModel(description="性别")
	private Boolean gender;//false男true�?
	@ApiModel(description="邮箱")
	private String email;
	@ApiModel(description="手机�?")
	private String mobile;
	@ApiModel(description="角色ID")
	private String roleId;//超级管理员admin、租户管理员、普通用�?
	@ApiModel(description="角色名称")
	private String roleName;
	@ApiModel(description="是否禁用，true禁用；false可用�?")
	private Boolean disable;//true禁用false可用
	@ApiModel(description="真实姓名")
	private String realName;
	@ApiModel(description="创建时间")
	private String createTime;
	@ApiModel(description="创建�?")
	private String creater;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public Boolean getDisable() {
		return disable;
	}
	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
}