package com.gcloud.header.compute.msg.api.network.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiAttachNetworkInterfaceMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "云服务器ID", require = true)
    @NotBlank(message = "0010601::云服务器ID不能为空")
    private String instanceId;
    @ApiModel(description = "网卡ID", require = true)
    @NotBlank(message = "0010602::网卡ID不能为空")
    private String networkInterfaceId;
    
	@Override
	public Class replyClazz() {
		return StandardApiAttachNetworkInterfaceReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
}