package com.gcloud.header.compute.msg.api.vm.statistics;

import java.util.List;

import com.gcloud.header.ListReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.InstanceStatisticsItemType;
import com.gcloud.header.compute.msg.api.model.InstanceStatisticsResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiInstancesStatisticsReplyMsg extends ListReplyMessage<InstanceStatisticsItemType>{
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "统计�?")
	InstanceStatisticsResponse statisticsItems;
	@ApiModel(description = "总数�?")
	int allNum;
	
	@Override
	public void setList(List<InstanceStatisticsItemType> list) {
		statisticsItems = new InstanceStatisticsResponse();
		statisticsItems.setStatisticsItem(list);
	}

	public InstanceStatisticsResponse getStatisticsItems() {
		return statisticsItems;
	}

	public void setStatisticsItems(InstanceStatisticsResponse statisticsItems) {
		this.statisticsItems = statisticsItems;
	}

	public int getAllNum() {
		return allNum;
	}

	public void setAllNum(int allNum) {
		this.allNum = allNum;
	}

}