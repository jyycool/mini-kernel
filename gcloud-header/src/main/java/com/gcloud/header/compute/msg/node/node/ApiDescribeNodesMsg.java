package com.gcloud.header.compute.msg.node.node;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeNodesMsg extends ApiPageMessage{

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDescribeNodesReplyMsg.class;
	}

	@ApiModel(description = "可用�?")
	private String zoneId;
	@ApiModel(description = "是否已经关联可用�?")
	private Boolean zone;
	@ApiModel(description = "key")
	private String key;
	@ApiModel(description = "节点状�??")
	private Integer state;
	@ApiModel(description = "是否查询资源，默认查�?")
    private Boolean resource;
	@ApiModel(description = "是否包括没有关联可用区的")
	private Boolean includeNoZone;


	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public Boolean getZone() {
		return zone;
	}

	public void setZone(Boolean zone) {
		this.zone = zone;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

    public Boolean getResource() {
        return resource;
    }

    public void setResource(Boolean resource) {
        this.resource = resource;
    }

    @Override
	public String getKey() {
		return key;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	public Boolean getIncludeNoZone() {
		return includeNoZone;
	}

	public void setIncludeNoZone(Boolean includeNoZone) {
		this.includeNoZone = includeNoZone;
	}
}