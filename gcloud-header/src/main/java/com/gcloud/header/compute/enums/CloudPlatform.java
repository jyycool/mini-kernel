package com.gcloud.header.compute.enums;

import org.apache.commons.lang.StringUtils;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public enum CloudPlatform {
	DESKTOPGCLOUD("DesktopCloud"), IDCSEVER("IDCSever");
	private CloudPlatform(String value) {
		this.value = value;
	}
	
	public static CloudPlatform getPlatformByValue(String val){
		
		CloudPlatform result = null;
		if(!StringUtils.isBlank(val)){
			CloudPlatform[] platformArr = CloudPlatform.values();
			for(CloudPlatform platform : platformArr){
				if(val.equals(platform.getValue())){
					result = platform;
					break;
				}
			}
		}
		return result;
		
	}

	private String value;

	public String getValue() {
		return value;
	}
}