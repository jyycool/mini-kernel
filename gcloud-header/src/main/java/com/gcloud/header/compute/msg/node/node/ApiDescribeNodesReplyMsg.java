package com.gcloud.header.compute.msg.node.node;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.node.node.model.DescribeNodesResponse;
import com.gcloud.header.compute.msg.node.node.model.NodeBaseInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeNodesReplyMsg extends PageReplyMessage<NodeBaseInfo>{

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "节点列表")
	private DescribeNodesResponse nodes;

	@Override
	public void setList(List<NodeBaseInfo> list) {
		nodes = new DescribeNodesResponse();
		nodes.setNode(list);
	}

	public DescribeNodesResponse getNodes() {
		return nodes;
	}

	public void setNodes(DescribeNodesResponse nodes) {
		this.nodes = nodes;
	}
}