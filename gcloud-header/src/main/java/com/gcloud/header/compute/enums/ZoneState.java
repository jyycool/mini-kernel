package com.gcloud.header.compute.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ZoneState {
	ENABLE(true, "可用"),
	DISABLE(false, "禁用");
	
	private boolean value;
	private String cnName;
	
	private ZoneState(boolean value, String cnName) {
		this.value = value;
		this.cnName = cnName;
	}

	public boolean getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}
	
	public static String getCnName(boolean value) {
		ZoneState zoneState = Arrays.stream(ZoneState.values()).filter(state -> state.getValue() == value).findFirst().orElse(null);
		return zoneState != null ? zoneState.getCnName() : null;
	}
}