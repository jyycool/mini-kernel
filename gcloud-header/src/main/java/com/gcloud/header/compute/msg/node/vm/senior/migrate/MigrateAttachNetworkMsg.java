package com.gcloud.header.compute.msg.node.vm.senior.migrate;

import com.gcloud.header.NodeMessage;
import com.gcloud.header.compute.msg.node.vm.model.VmNetworkDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MigrateAttachNetworkMsg extends NodeMessage{
	private static final long serialVersionUID = 1L;

	private String instanceId;
	private VmNetworkDetail vmNetworkDetail;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public VmNetworkDetail getVmNetworkDetail() {
		return vmNetworkDetail;
	}

	public void setVmNetworkDetail(VmNetworkDetail vmNetworkDetail) {
		this.vmNetworkDetail = vmNetworkDetail;
	}
	
	
}