package com.gcloud.header.compute.enums;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum VmState {
	STOPPED("已关�?"),
	PAUSED("已停�?"),
	RUNNING("正在运行"),
	DISABLED("已故�?"),
	CRASHED("已异�?"),
	PENDING("正在创建");


	private String cnName;

	VmState(String cnName) {
		this.cnName = cnName;
	}

	public String value() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
	}

	public String getCnName() {
		return cnName;
	}
	
	public static String getCnName(String enName) {
		VmState vmState = Arrays.stream(VmState.values()).filter(state -> state.value().equals(enName)).findFirst().orElse(null);
		return vmState != null ? vmState.getCnName() : null;
	}

	public static Map<String, String> cnMap(){
		Map<String, String> cnMap = new HashMap<>();
		Arrays.stream(VmState.values()).forEach(s -> cnMap.put(s.value(), s.getCnName()));
		return  cnMap;
	}
}