package com.gcloud.header.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//import org.springframework.web.bind.annotation.RequestMethod;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiOperation {

	public String name();
	public String url();
	//public RequestMethod requestMethod() default RequestMethod.GET;
	public String permissionCode() default "";//若此值不是空字串，表示些操作是一个单独的权限
	public String[] permissions() default {};//对应�?属权限，某些操作可能对应多个权限,若为空，取用permissionCode
	public boolean admin() default false;//表示super admin的接�?
	public boolean common() default false;//表示�?个共公接口，不受权限拦截
	public Class request() default Object.class;  // 参数�?
	public Class response() default Object.class;  // 返回�?
	public String description() default "";
	
}