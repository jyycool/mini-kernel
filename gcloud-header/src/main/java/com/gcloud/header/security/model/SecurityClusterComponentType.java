package com.gcloud.header.security.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class SecurityClusterComponentType implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "组件ID")
    private String id;
    @ApiModel(description = "集群ID")
    private String clusterId;
    @ApiModel(description = "组件类型")
    private String type;
    @ApiModel(description = "对象ID")
    private String objectId;
    @ApiModel(description = "对象类型")
    private String objectType;
    @ApiModel(description = "状�??")
    private String state;
    @ApiModel(description = "中文状�??")
    private String stateCnName;
    @ApiModel(description = "")
    private String stateInfo;
    @ApiModel(description = "创建�?")
    private String createUser;
    @ApiModel(description = "配置")
    private String createConfig;
    @ApiModel(description = "创建时间")
    private String createTime;
    @ApiModel(description = "更新时间")
    private String updateTime;
    @ApiModel(description = "HA")
    private Boolean ha;
    @ApiModel(description = "虚拟机信�?")
    private SecurityClusterInstanceType vmInfo;
    @ApiModel(description = "")
    private SecurityClusterDcType dcInfo;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getClusterId() {
		return clusterId;
	}
	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateCnName() {
		return stateCnName;
	}
	public void setStateCnName(String stateCnName) {
		this.stateCnName = stateCnName;
	}
	public String getStateInfo() {
		return stateInfo;
	}
	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateConfig() {
		return createConfig;
	}
	public void setCreateConfig(String createConfig) {
		this.createConfig = createConfig;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Boolean getHa() {
		return ha;
	}
	public void setHa(Boolean ha) {
		this.ha = ha;
	}
	public SecurityClusterInstanceType getVmInfo() {
		return vmInfo;
	}
	public void setVmInfo(SecurityClusterInstanceType vmInfo) {
		this.vmInfo = vmInfo;
	}
	public SecurityClusterDcType getDcInfo() {
		return dcInfo;
	}
	public void setDcInfo(SecurityClusterDcType dcInfo) {
		this.dcInfo = dcInfo;
	}
}