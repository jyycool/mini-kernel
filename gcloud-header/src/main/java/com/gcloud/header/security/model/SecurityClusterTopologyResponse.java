package com.gcloud.header.security.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.security.model.SecurityClusterComponentType;
import com.gcloud.header.security.model.SecurityClusterInstanceType;
import com.gcloud.header.security.model.SecurityClusterSubnetType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class SecurityClusterTopologyResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "集群ID")
	private String clusterId;
	@ApiModel(description = "组件信息")
	private List<SecurityClusterComponentType> components;
	@ApiModel(description = "虚拟机信�?")
	private List<SecurityClusterInstanceType> instances;
	@ApiModel(description = "子网信息")
	private SecurityClusterSubnetType subnet;
	private boolean ha;
	
	public String getClusterId() {
		return clusterId;
	}
	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}
	public List<SecurityClusterComponentType> getComponents() {
		return components;
	}
	public void setComponents(List<SecurityClusterComponentType> components) {
		this.components = components;
	}
	public List<SecurityClusterInstanceType> getInstances() {
		return instances;
	}
	public void setInstances(List<SecurityClusterInstanceType> instances) {
		this.instances = instances;
	}
	public SecurityClusterSubnetType getSubnet() {
		return subnet;
	}
	public void setSubnet(SecurityClusterSubnetType subnet) {
		this.subnet = subnet;
	}
	public boolean isHa() {
		return ha;
	}
	public void setHa(boolean ha) {
		this.ha = ha;
	}
}