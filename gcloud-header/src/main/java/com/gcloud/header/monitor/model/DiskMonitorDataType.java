package com.gcloud.header.monitor.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DiskMonitorDataType implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModel(description = "磁盘ID")
	private String diskId;
	@ApiModel(description = "读�?�度")
	private Integer bpsRead;
	@ApiModel(description = "写�?�度")
	private Integer bpsWrite;
	@ApiModel(description = "总�?�度")
	private Integer bpsTotal;
	@ApiModel(description = "时间�?")
	private String timeStamp;
	
	public String getDiskId() {
		return diskId;
	}
	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}
	public Integer getBpsRead() {
		return bpsRead;
	}
	public void setBpsRead(Integer bpsRead) {
		this.bpsRead = bpsRead;
	}
	public Integer getBpsWrite() {
		return bpsWrite;
	}
	public void setBpsWrite(Integer bpsWrite) {
		this.bpsWrite = bpsWrite;
	}
	public Integer getBpsTotal() {
		return bpsTotal;
	}
	public void setBpsTotal(Integer bpsTotal) {
		this.bpsTotal = bpsTotal;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
}