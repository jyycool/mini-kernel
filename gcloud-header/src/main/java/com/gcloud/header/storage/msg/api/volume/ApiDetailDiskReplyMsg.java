package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.DetailDisk;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDetailDiskReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="磁盘详情")
	private DetailDisk detailDisk;

	public DetailDisk getDetailDisk() {
		return detailDisk;
	}

	public void setDetailDisk(DetailDisk detailDisk) {
		this.detailDisk = detailDisk;
	}
}