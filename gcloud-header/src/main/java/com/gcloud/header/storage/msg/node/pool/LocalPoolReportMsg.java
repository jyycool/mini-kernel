package com.gcloud.header.storage.msg.node.pool;

import com.gcloud.header.NodeMessage;
import com.gcloud.header.storage.msg.node.pool.model.LocalPoolReportResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LocalPoolReportMsg extends NodeMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LocalPoolReportResponse res;
	private String cron;

	public LocalPoolReportResponse getRes() {
		return res;
	}

	public void setRes(LocalPoolReportResponse res) {
		this.res = res;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

}