package com.gcloud.header.storage.model;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.standard.StandardDiskItemType;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DiskItemType extends StandardDiskItemType implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ApiModel(description = "中文状�??")
    private String cnStatus;
    
    @ApiModel(description = "中文磁盘属�??, 数据盘|系统�?")
    private String cnType;
    
    @ApiModel(description = "可用�?")
    private String zoneName;
    
    @ApiModel(description = "关联云服务器名称")
    private String instanceName;
    
    @ApiModel(description = "磁盘种类")
    private String cnCategory;
    
    @ApiModel(description = "存储节点")
    private String hostname;

	public String getCnCategory() {
		return cnCategory;
	}

	public void setCnCategory(String cnCategory) {
		this.cnCategory = cnCategory;
	}

	public String getCnType() {
		return cnType;
	}

	public void setCnType(String cnType) {
		this.cnType = cnType;
	}

	public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}