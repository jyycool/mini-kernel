package com.gcloud.header.storage.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DescribeSnapshotsResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModel(description="快照")
    private List<SnapshotType> snapshot;

    public List<SnapshotType> getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(List<SnapshotType> snapshot) {
        this.snapshot = snapshot;
    }
}