package com.gcloud.header.storage.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StoragePoolType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "存储ID")
    private String id;
	@ApiModel(description = "存储池名�?")
    private String displayName;
    private Integer provider;
    @ApiModel(description = "存储池类�?")
    private String storageType;
    @ApiModel(description = "存储�?")
    private String poolName;
    @ApiModel(description = "存储池所在节点名�?")
    private String hostname;
    private String driver;
    @ApiModel(description = "存储池创建时�?")
    @JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
    private Date createTime;
    @ApiModel(description = "存储池关联信�?")
    private List<StoragePoolZoneInfo> associateInfo;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Integer getProvider() {
		return provider;
	}
	public void setProvider(Integer provider) {
		this.provider = provider;
	}
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	public String getPoolName() {
		return poolName;
	}
	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public List<StoragePoolZoneInfo> getAssociateInfo() {
		return associateInfo;
	}
	public void setAssociateInfo(List<StoragePoolZoneInfo> associateInfo) {
		this.associateInfo = associateInfo;
	}
}