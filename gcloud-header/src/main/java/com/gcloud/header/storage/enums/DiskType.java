package com.gcloud.header.storage.enums;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum  DiskType {

    SYSTEM("system", "系统�?"), DATA("data", "数据�?");

    private String value;
    private String cnName;


    DiskType(String value, String cnName) {
        this.value = value;
        this.cnName = cnName;
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }
    
    public static String getCnName(String value) {
    	DiskType enu =  Arrays.stream(DiskType.values()).filter(type -> type.getValue().equals(value)).findFirst().orElse(null);
		return enu != null ? enu.getCnName() : null;
	}

}