package com.gcloud.header.storage.msg.api.pool;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class StoragePoolActions {

	public static final String CREATE_DISK_CATEGORY = "CreateDiskCategory";
	public static final String MODIFY_DISK_CATEGORY = "ModifyDiskCategory";
	public static final String DELETE_DISK_CATEGORY = "DeleteDiskCategory";
    public static final String DESCRIBE_DISK_CATEGORIES = "DescribeDiskCategories";
    public static final String DETAIL_DISK_CATEGORY = "DetailDiskCategory";
    public static final String ASSOCIATE_DISK_CATEGORY_ZONE = "AssociateDiskCategoryZone";
    public static final String ASSOCIATE_DISK_CATEGORY_POOL = "AssociateDiskCategoryPool";
    public static final String ASSOCIATE_DISK_CATEGORY_MORE_POOL = "AssociateDiskCategoryMorePool";
    public static final String ASSOCIATE_POOL_ZONE = "AssociatePoolZone";
    public static final String ENABLE_DISK_CATEGORY = "EnableDiskCategory";
    public static final String UNASSOCIATE_DISK_CATEGORY_POOL = "UnassociateDiskCategoryPool";

    public static final String DESCRIBE_STORAGE_POOLS = "DescribeStoragePools";
    public static final String CREATE_STORAGE_POOL = "CreateStoragePool";
    public static final String MODIFY_STORAGE_POOL = "ModifyStoragePool";
    public static final String DELETE_STORAGE_POOL = "DeleteStoragePool";
    public static final String DETAIL_STORAGE_POOL = "DetailStoragePool";

    public static final String DISK_CATEGORY_STORAGE_TYPE = "DiskCategoryStorageType";
    public static final String STORAGE_POOL_STORAGE_TYPE = "StoragePoolStorageType";

}