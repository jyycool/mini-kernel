package com.gcloud.header.storage.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum StorageNodeState {
	CONNECTED("正常", 1), 
	DISCONNECTED("失去连接", 0);

	private StorageNodeState(String cnName, int value) {
		this.cnName = cnName;
		this.value = value;
	}

	private int value;
	private String cnName;

	public int getValue() {
		return this.value;
	}

	public String getCnName() {
		return cnName;
	}

	/**
	 * 
	 * @Title: getCnName
	 * @Description: 根据value获取cnName
	 *
	 * @param value
	 * @return
	 */
	public static String getCnName(int value) {
		for (StorageNodeState nodeState : StorageNodeState.values()) {
			if (nodeState.getValue() == value) {
				return nodeState.cnName;
			}
		}
		return null;
	}
	
	public void setCnName(String cnName) {
		this.cnName = cnName;
	}
}