package com.gcloud.header.storage.msg.node.volume.lvm;

import java.util.List;

import com.gcloud.header.NodeMessage;
import com.gcloud.header.storage.msg.node.volume.lvm.model.LvInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LvReportMsg extends NodeMessage {
	private String hostname;
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	private List<LvInfo> lvs;

	public List<LvInfo> getLvs() {
		return lvs;
	}

	public void setLvs(List<LvInfo> lvs) {
		this.lvs = lvs;
	}
	
}