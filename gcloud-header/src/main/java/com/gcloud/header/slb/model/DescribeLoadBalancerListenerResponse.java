package com.gcloud.header.slb.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeLoadBalancerListenerResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "监听器信�?")
	private List<LoadBalancerListenerSetType> loadBalancerListener;

	public List<LoadBalancerListenerSetType> getLoadBalancerListener() {
		return loadBalancerListener;
	}

	public void setLoadBalancerListener(List<LoadBalancerListenerSetType> loadBalancerListener) {
		this.loadBalancerListener = loadBalancerListener;
	}
}