package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.slb.model.BackendServerResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeVServerGroupAttributeReplyMsg extends ApiReplyMessage{
	
	@ApiModel(description = "服务器组ID")
	private String VServerGroupId;
	@ApiModel(description = "服务器组名称")
	private String VServerGroupName;
	@ApiModel(description = "后端服务器列�?")
	private BackendServerResponse backendServers;
	
	public String getVServerGroupId() {
		return VServerGroupId;
	}
	public void setVServerGroupId(String vServerGroupId) {
		VServerGroupId = vServerGroupId;
	}
	public String getVServerGroupName() {
		return VServerGroupName;
	}
	public void setVServerGroupName(String vServerGroupName) {
		VServerGroupName = vServerGroupName;
	}
	public BackendServerResponse getBackendServers() {
		return backendServers;
	}
	public void setBackendServers(BackendServerResponse backendServers) {
		this.backendServers = backendServers;
	}
}