package com.gcloud.header.slb.msg.api;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiCreateLoadBalancerTCPListenerMsg extends ApiMessage {
	
	@ApiModel(description = "负载均衡ID", require = true)
	@NotBlank(message = "0120301::负载均衡ID不能为空")
	private String  loadBalancerId;
	@ApiModel(description = "端口�?", require = true)
	@NotNull(message = "0120302::端口号不能为�?")
	@Max(value = 65535, message = "0120303::端口号不能超�?65535")
	@Min(value = 1, message = "0120304::端口号不能低�?1")
	private int   listenerPort;
	@ApiModel(description = "虚拟服务器组ID", require = true)
	@NotBlank(message = "0120305::虚拟服务器组ID不能为空")
	private String   vServerGroupId;	
	
	public String getLoadBalancerId() {
		return loadBalancerId;
	}

	public void setLoadBalancerId(String loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}

	public int getListenerPort() {
		return listenerPort;
	}

	public void setListenerPort(int listenerPort) {
		this.listenerPort = listenerPort;
	}

	public String getvServerGroupId() {
		return vServerGroupId;
	}

	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}

	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiCreateLoadBalancerTCPListenerReplyMsg.class;
	}
}