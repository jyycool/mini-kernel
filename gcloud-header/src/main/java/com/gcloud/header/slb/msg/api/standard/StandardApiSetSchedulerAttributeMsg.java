package com.gcloud.header.slb.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiSetSchedulerAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "监听器ID或后端服务器组ID", require = true)
	@NotBlank(message = "0130101::ID不能为空")
	private String resourceId;
	@ApiModel(description = "协议", require = false)
	private String protocol;
	@ApiModel(description = "调度策略名称，取值有ROUND_ROBIN:轮询，LEAST_CONNECTIONS:�?小连接数，SOURCE_IP:源IP", require = true)
	@NotBlank(message = "0130102::调度策略名称不能为空")
	private String scheduler;

	@Override
	public Class replyClazz() {
		return StandardApiSetSchedulerAttributeReplyMsg.class;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getScheduler() {
		return scheduler;
	}

	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}
}