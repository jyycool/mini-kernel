package com.gcloud.header.slb.msg.api.standard;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeHealthCheckAttributeReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "健康�?查连续失败次�?")
	private Integer unhealthyThreshold;
	@ApiModel(description = "健康�?查等待后端服务器响应时间")
	private Integer healthCheckTimeout;
	@ApiModel(description = "健康�?查的时间间隔")
	private Integer healthCheckInterval;
	@ApiModel(description = "健康�?查开关：on、off")
	private String healthCheck;
	@ApiModel(description = "用于健康�?查的URI")
	private String healthCheckURI;
	@ApiModel(description = "健康�?查连续成功次�?")
	private Integer healthyThreshold;
	@ApiModel(description = "健康�?查类�?,取�?�：tcp | http")
	private String healthCheckType;
	
	public Integer getUnhealthyThreshold() {
		return unhealthyThreshold;
	}
	public void setUnhealthyThreshold(Integer unhealthyThreshold) {
		this.unhealthyThreshold = unhealthyThreshold;
	}
	public Integer getHealthCheckTimeout() {
		return healthCheckTimeout;
	}
	public void setHealthCheckTimeout(Integer healthCheckTimeout) {
		this.healthCheckTimeout = healthCheckTimeout;
	}
	public Integer getHealthCheckInterval() {
		return healthCheckInterval;
	}
	public void setHealthCheckInterval(Integer healthCheckInterval) {
		this.healthCheckInterval = healthCheckInterval;
	}
	public String getHealthCheck() {
		return healthCheck;
	}
	public void setHealthCheck(String healthCheck) {
		this.healthCheck = healthCheck;
	}
	public String getHealthCheckURI() {
		return healthCheckURI;
	}
	public void setHealthCheckURI(String healthCheckURI) {
		this.healthCheckURI = healthCheckURI;
	}
	public Integer getHealthyThreshold() {
		return healthyThreshold;
	}
	public void setHealthyThreshold(Integer healthyThreshold) {
		this.healthyThreshold = healthyThreshold;
	}
	public String getHealthCheckType() {
		return healthCheckType;
	}
	public void setHealthCheckType(String healthCheckType) {
		this.healthCheckType = healthCheckType;
	}
}