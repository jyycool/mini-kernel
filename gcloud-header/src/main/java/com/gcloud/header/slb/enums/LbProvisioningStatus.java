package com.gcloud.header.slb.enums;

import java.util.Arrays;

import com.gcloud.header.network.enums.NetworkStatus;
import com.google.common.base.CaseFormat;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum LbProvisioningStatus {

    ACTIVE("�?�?"),
    DOWN("失活"),
    CREATED("已创�?"),
    PENDING_CREATE("创建�?"),
    PENDING_UPDATE("升级�?"),
    PENDING_DELETE("删除�?"),
    INACTIVE("失活"),
    ERROR("错误");


    private String cnName;

    LbProvisioningStatus(String cnName) {
        this.cnName = cnName;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public String getCnName() {
        return cnName;
    }
    
    public static String getCnName(String enName) {
    	LbProvisioningStatus status = Arrays.stream(LbProvisioningStatus.values()).filter(state -> state.value().equals(enName)).findFirst().orElse(null);
		return status != null ? status.getCnName() : null;
	}

}