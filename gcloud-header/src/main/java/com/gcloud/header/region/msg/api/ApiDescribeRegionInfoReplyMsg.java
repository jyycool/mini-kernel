package com.gcloud.header.region.msg.api;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.DescribeRegionInfoResponse;
import com.gcloud.header.compute.msg.api.model.RegionInfoType;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeRegionInfoReplyMsg extends PageReplyMessage<RegionInfoType> {

	@ApiModel(description = "区域列表")
	private DescribeRegionInfoResponse regions;

	@Override
	public void setList(List<RegionInfoType> list) {
		regions = new DescribeRegionInfoResponse();
		regions.setRegion(list);
	}

	public DescribeRegionInfoResponse getRegions() {
		return regions;
	}

	public void setRegions(DescribeRegionInfoResponse regions) {
		this.regions = regions;
	}
}