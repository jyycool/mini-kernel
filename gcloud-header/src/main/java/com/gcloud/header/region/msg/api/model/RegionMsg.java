package com.gcloud.header.region.msg.api.model;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class RegionMsg {

	@ApiModel(description = "区域ID")
    private String id;
	@ApiModel(description = "区域名称")
    private String name;
	@ApiModel(description = "是否本地")
    private Boolean local;
	@ApiModel(description = "API远程地址")
    private String remoteAddress;
	@ApiModel(description = "hcp信息")
    private RegionHcpMsg hcp;
	@ApiModel(description = "区域监控信息")
    private RegionMonitorMsg monitor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getLocal() {
        return local;
    }

    public void setLocal(Boolean local) {
        this.local = local;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public RegionHcpMsg getHcp() {
        return hcp;
    }

    public void setHcp(RegionHcpMsg hcp) {
        this.hcp = hcp;
    }

    public RegionMonitorMsg getMonitor() {
        return monitor;
    }

    public void setMonitor(RegionMonitorMsg monitor) {
        this.monitor = monitor;
    }
}