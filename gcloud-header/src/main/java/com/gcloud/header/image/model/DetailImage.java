package com.gcloud.header.image.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailImage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "镜像ID")
	private String id;
	
	@ApiModel(description = "镜像名称")
	private String name;
	
	@ApiModel(description = "架构, x86_64、i386�? i686")
	private String architeture;
	
	@ApiModel(description = "中文状�??")
	private String cnStatus;
	
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	
	@ApiModel(description = "描述")
	private String description;
	
	@ApiModel(description = "")
	private Boolean isTask;
	
	@ApiModel(description = "系统类型，linux、windows")
	private String osType;
	
	@ApiModel(description = "镜像大小")
	private Long size;
	
	@ApiModel(description = "状�?�，unrecognized:未知;queued:队列�?;saving:保存�?;active:可用:deactivated:不可�?;killed:终止;delteted:已删�?;pending_delete:删除�?;")
	private String status;

	@ApiModel(description = "实际大小")
	private Long actualSize;

	@ApiModel(description = "母镜像ID")
	private String parentId;
	
	@ApiModel(description = "是否禁用，true:禁用, false:可用")
	private Boolean disable;
	
	@ApiModel(description = "上传�?")
	private String owner;
	
	@ApiModel(description="镜像像拥有�?�别�?")
    private String imageOwnerAlias;
	
	public String getImageOwnerAlias() {
		return imageOwnerAlias;
	}
	public void setImageOwnerAlias(String imageOwnerAlias) {
		this.imageOwnerAlias = imageOwnerAlias;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Boolean getDisable() {
		return disable;
	}
	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
	public String getArchiteture() {
		return architeture;
	}
	public void setArchiteture(String architeture) {
		this.architeture = architeture;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Boolean getIsTask() {
		return isTask;
	}
	public void setIsTask(Boolean isTask) {
		this.isTask = isTask;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getActualSize() {
		return actualSize;
	}
	public void setActualSize(Long actualSize) {
		this.actualSize = actualSize;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
}