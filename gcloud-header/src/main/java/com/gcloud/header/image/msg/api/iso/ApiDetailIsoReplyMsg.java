package com.gcloud.header.image.msg.api.iso;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.image.model.iso.DetailIso;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDetailIsoReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="映像详情")
	private DetailIso detailIso;

	public DetailIso getDetailIso() {
		return detailIso;
	}

	public void setDetailIso(DetailIso detailIso) {
		this.detailIso = detailIso;
	}
}