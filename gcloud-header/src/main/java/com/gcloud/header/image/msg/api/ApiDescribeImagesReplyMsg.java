package com.gcloud.header.image.msg.api;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
import com.gcloud.header.image.model.DescribeImagesResponse;
import com.gcloud.header.image.model.ImageType;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeImagesReplyMsg extends PageReplyMessage<ImageType> {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "镜像列表")
    private DescribeImagesResponse images;
    @ApiModel(description = "区域ID")
    private String regionId = ControllerProperty.REGION_ID;

    @Override
    public void setList(List<ImageType> list) {
        images = new DescribeImagesResponse();
        images.setImage(list);
    }

    public DescribeImagesResponse getImages() {
        return images;
    }

    public void setImages(DescribeImagesResponse images) {
        this.images = images;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }
}