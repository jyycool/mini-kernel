package com.gcloud.header.image.msg.node;

import com.gcloud.header.NeedReplyMessage;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CheckImageStoreMsg extends NeedReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String imageId;
	private String target;//节点名�?�rbd池名、vg�?
	private String targetType;//node\vg\rbd
	private String imageResourceType;//image\iso
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public String getImageResourceType() {
		return imageResourceType;
	}
	public void setImageResourceType(String imageResourceType) {
		this.imageResourceType = imageResourceType;
	}
	@Override
	public Class replyClazz() {
		return CheckImageStoreReplyMsg.class;
	}

}